function Prop = PropensityFunction(x, Parameters)
% Stoichiometry Matrix for I-Circuit with Network Perturbation
% 	 Species: 		 X = [X_1; X_2; A; Z_1; Z_2; Z_prime_1; Z_prime_2; X_prime_1]
% 	 Reactions: 	R1:         X_1                     -->         phi                     [gamma*X_1 + D * (k_3 * (X_1/kappa_3) + k_prime_3 * (X_1/kappa_prime_3)^2 ) / (1 + (X_1/kappa_3) + (X_1/kappa_prime_3)^2)]
% 				    R2:         X_1                     -->         X_2                     [c*X_1]
% 				    R3:         X_2                     -->         phi                     [gamma*X_2]
%                   R4:         A                       -->         phi                     [gamma*A]
%                   R5:         X2 + X2                 -->         A                       [a*X_2^2]
%                   R6:         A                       -->         X2 + X2                 [d*A]
% 				    R7:         X_2                     -->         X_2 +  Z_prime_2        [G_prime_2 * (k_prime_0 + k_prime_2 * (A/kappa_2)) / (1 + A/kappa_2)]
% 				    R8:         Z_prime_2				-->         phi                     [delta_prime*Z_prime_2]
% 				    R9:         Z_prime_2				-->         X_prime_1 +  Z_prime_2	[k_prime*Z_prime_2]
% 				    R10:        X_prime_1				-->         phi                     [gamma_prime*X_prime_1]
% 				    R11:        X_prime_1 +  Z_1		-->         Z_prime_1				[a_prime*X_prime_1*Z_1]
% 				    R12:        Z_prime_1				-->         X_prime_1 +  Z_1		[d_prime*Z_prime_1]
% 				    R13:		Z_prime_1				-->         phi                     [delta*Z_prime_1]
% 				    R14:		Z_prime_1 +  Z_2        -->         phi                     [eta*Z_prime_1*Z_2]
% 				    R15:		phi                     -->         Z_1                     [k_1*G_1]
% 				    R16:        A                       -->         A +  Z_2				[G_2 * (k_0 + k_2 * (A/kappa_2)) / (1 + A/kappa_2)]
% 				    R17:		Z_1 +  Z_2				-->         phi                     [eta*Z_1*Z_2]
% 				    R18:		Z_1                     -->         X_1 +  Z_1				[k*Z_1]
% 				    R19:		Z_1                     -->         phi                     [delta*Z_1]
% 				    R20:		Z_2                     -->         phi                     [delta*Z_2]

%% Extract Parameters
gamma = Parameters.gamma;                   % [1/hr]
delta = Parameters.delta;                   % [1/hr]
eta = Parameters.eta;                       % [1/nM/hr]
k = Parameters.k;                           % [1/hr]
c = Parameters.c;                           % [1/hr]
a = Parameters.a;                           % [1/nM/hr]
d = Parameters.d;                           % [1/hr]
k_0 = Parameters.k_0;                       % [(nM/pmol)/hr]
k_1 = Parameters.k_1;                       % [(nM/pmol)/hr]
k_2 = Parameters.k_2;                       % [(nM/pmol)/hr]
k_3 = Parameters.k_3;                       % [1/hr]
k_prime_3 = Parameters.k_prime_3;           % [1/hr]
kappa_2 = Parameters.kappa_2;               % [nM]
kappa_3 = Parameters.kappa_3;               % [nM]
kappa_prime_3 = Parameters.kappa_prime_3; 	% [nM]
gamma_prime = Parameters.gamma_prime;      	% [1/hr]
delta_prime = Parameters.delta_prime;      	% [1/hr]
k_prime = Parameters.k_prime;               % [1/hr]
a_prime = Parameters.a_prime;           	% [1/nM/hr]
d_prime = Parameters.d_prime;               % [1/hr]
k_prime_0 = Parameters.k_prime_0;           % [(nM/pmol)(1/hr)]
k_prime_2 = Parameters.k_prime_2;           % [(nM/pmol)(1/hr)]

%% Extract Input and Disturbance
D = Parameters.D;
G_1 = Parameters.G_1;
G_2 = Parameters.G_2;
G_prime_2 = Parameters.G_prime_2;

%% Extract State Variables
X_1 = x(1);
X_2 = x(2);
A = x(3);
Z_1 = x(4);
Z_2 = x(5);
Z_prime_1 = x(6);
Z_prime_2 = x(7);
X_prime_1 = x(8);
        
%% Propensities
Prop = [gamma*X_1 + D * (k_3 * (X_1/kappa_3) + k_prime_3 * (X_1/kappa_prime_3)^2 ) / (1 + (X_1/kappa_3) + (X_1/kappa_prime_3)^2); ...
        c*X_1; ...
        gamma*X_2; ...
        gamma*A; ...
        a*X_2^2; ...
        d*A; ...
        G_prime_2 * (k_prime_0 + k_prime_2 * (A/kappa_2)) / (1 + A/kappa_2); ...
        delta_prime*Z_prime_2; ...
        k_prime*Z_prime_2; ...
        gamma_prime*X_prime_1; ...
        a_prime*X_prime_1*Z_1; ...
        d_prime*Z_prime_1; ...
        delta*Z_prime_1; ...
        eta*Z_prime_1*Z_2; ...
        k_1*G_1; ...
        G_2 * (k_0 + k_2 * (A/kappa_2)) / (1 + A/kappa_2); ...
        eta*Z_1*Z_2; ...
        k*Z_1; ...
        delta*Z_1; ...
        delta*Z_2];
end