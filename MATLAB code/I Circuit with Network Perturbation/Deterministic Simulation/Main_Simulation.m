%% Clear Workspace
close all;
clear;
clc;
Save_Flag = 0;

%% Inputs & Disturbances
G_1 = 1.3;                                                  % [pmol]
G_2 = 1.5;                                                  % [pmol]
G_prime_2 = 1.2;                                            % [nM]
D = 0.3;                                                    % [nM]
Input = [G_1, G_2, G_prime_2, D];
Type = 3;                                                   % Type 0 : Open Loop Without Network Perturbation
                                                            % Type 1 : Closed Loop Without Network Perturbation
                                                            % Type 2 : Open Loop With Network Perturbation
                                                            % Type 3 : Closed Loop With Network Perturbation
        
%% Biological Parameters
BioParameters = Class_BioParameters();
BioParameters.gamma = 0.14;                                 % [1/hr]
BioParameters.delta = 0.1;                                  % [1/hr]
BioParameters.eta = 10.355;                                 % [1/nM/hr]
BioParameters.k = 100.32;                                	% [1/hr]
BioParameters.c = 1;                                        % [1/hr]
BioParameters.a  = 8.3;                                     % [1/nM/hr]
BioParameters.d = 9.3;                                      % [1/hr]
BioParameters.k_0 = 0.124;                                  % [(nM/pmol)/hr]
BioParameters.k_1 = 30.532;                                 % [(nM/pmol)/hr]
BioParameters.k_2 = 5.34;                                   % [(nM/pmol)/hr]
BioParameters.k_3 = 1.34;                                   % [1/hr]
BioParameters.k_prime_3 = 1.22;                             % [1/hr]
BioParameters.kappa_2 = 5.23;                               % [nM]
BioParameters.kappa_3 = 3.253;                              % [nM]
BioParameters.kappa_prime_3 = 4.33;                         % [nM]
BioParameters.c_x = 94;                                     % [a.u./nM]
BioParameters.gamma_prime = 0.1;                            % [1/hr]
BioParameters.delta_prime = 0.08;                           % [1/hr]
BioParameters.k_prime = 1.8;                                % [1/hr]
BioParameters.a_prime = 10.3;                               % [1/nM/hr]
BioParameters.d_prime = 12.4;                               % [1/hr]
BioParameters.k_prime_0 = 0.1;                              % [(nM/pmol)(1/hr)]
BioParameters.k_prime_2  = 4.3;                             % [(nM/pmol)(1/hr)]

%% Time Horizon and Initial Conditions
tf = 100;                                                   % [hr]
N = 1000;
IC = [0; 0; 0; 0; 0; 0; 0; 0];

%% Select Scenario
switch Type
    case 0      % Type 0 : Open Loop Without Network Perturbation
        G_prime_2 = 0;
        BioParameters.kappa_2 = 1e10;    
    case 1      % Type 1 : Closed Loop Without Network Perturbation
        G_prime_2 = 0;
    case 2      % Type 2 : Open Loop With Network Perturbation
        BioParameters.kappa_2 = 1e10;
    case 3      % Type 3 : Closed Loop With Network Perturbation
end
        

%% Simulation
Solver = 'ODE23s';
NetworkParameters = struct(BioParameters);
NetworkParameters.G_1 = G_1;
NetworkParameters.G_2 = G_2;
NetworkParameters.G_prime_2 = G_prime_2;
NetworkParameters.D = D;
[time_vector, Solution] = DSA(StoichiometryMatrix(), @PropensityFunction, NetworkParameters, IC, tf, N, Solver);
X_1 = Solution(1,:); X_2 = Solution(2,:); A = Solution(3,:);
Z_1 = Solution(4,:); Z_2 = Solution(5,:);
Z_prime_1 = Solution(6,:); Z_prime_2 = Solution(7,:); X_prime_1 = Solution(8,:);

%% Fixed Point
LumpedParameters = Bio2Lumped(BioParameters);
tic
[X_tilde_1, X_tilde_2, A_tilde, Z_bar_1, Z_bar_2, Z_bar_prime_1, Z_bar_prime_2, X_bar_prime_1] = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);
toc
X_bar_1 = X_tilde_1 * BioParameters.gamma / BioParameters.c / BioParameters.c_x;
X_bar_2 = X_tilde_2 / BioParameters.c_x;
A_bar = A_tilde / BioParameters.c_x;

%% Figure Settings
SS = 4; % Screen Scale
Colors = lines(10);
MyBlue = Colors(1,:); MyRed = Colors(2,:); MyGreen = Colors(5,:); MyPurple = Colors(3,:);
Figure_Width = 7 * SS;
Figure_Height = 8 * SS;
FontSize = 7 * SS;
FontSize_Small = 4 * SS;
LineWidth = 0.5 * SS;
LineWidth_Thick = 1 * SS;
MarkerSize = 5 * SS;
% Figure 1
Handle_Figure1 = figure();
    Handle_Figure1.Color = [1 1 1];
    Handle_Figure1.PaperUnits = 'centimeters';
    Handle_Figure1.Units = 'centimeters';
    Handle_Figure1.Position = [1, 20, Figure_Width, Figure_Height];
    Handle_Figure1.PaperPositionMode = 'auto';
    Handle_Figure1.PaperSize = [Handle_Figure1.PaperPosition(3), Handle_Figure1.PaperPosition(4)];
Handle_Axis1 = subplot(3,3,1);
    Handle_Axis1.Box = 'on';
    Handle_Axis1.FontSize = FontSize;
    hold(Handle_Axis1, 'on');
    grid(Handle_Axis1, 'on');
    Handle_Axis1.XMinorGrid = 'on';
    Handle_Axis1.YMinorGrid = 'on';
    Handle_Axis1.XLabel.String = '$t$';
    Handle_Axis1.YLabel.String = '$X_1$';
    Handle_Axis1.XLabel.Interpreter = 'latex';
    Handle_Axis1.YLabel.Interpreter = 'latex';
Handle_Axis2 = subplot(3,3,2);
    Handle_Axis2.Box = 'on';
    Handle_Axis2.FontSize = FontSize;
    hold(Handle_Axis2, 'on');
    grid(Handle_Axis2, 'on');
    Handle_Axis2.XMinorGrid = 'on';
    Handle_Axis2.YMinorGrid = 'on';
    Handle_Axis2.XLabel.String = '$t$';
    Handle_Axis2.YLabel.String = '$X_2$';
    Handle_Axis2.XLabel.Interpreter = 'latex';
    Handle_Axis2.YLabel.Interpreter = 'latex';
Handle_Axis3 = subplot(3,3,3);
    Handle_Axis3.Box = 'on';
    Handle_Axis3.FontSize = FontSize;
    hold(Handle_Axis3, 'on');
    grid(Handle_Axis3, 'on');
    Handle_Axis3.XMinorGrid = 'on';
    Handle_Axis3.YMinorGrid = 'on';
    Handle_Axis3.XLabel.String = '$t$';
    Handle_Axis3.YLabel.String = '$A$'; 
    Handle_Axis3.XLabel.Interpreter = 'latex';
    Handle_Axis3.YLabel.Interpreter = 'latex';
Handle_Axis4 = subplot(3,3,4);
    Handle_Axis4.Box = 'on';
    Handle_Axis4.FontSize = FontSize;
    hold(Handle_Axis4, 'on');
    grid(Handle_Axis4, 'on');
    Handle_Axis4.XMinorGrid = 'on';
    Handle_Axis4.YMinorGrid = 'on';
    Handle_Axis4.XLabel.String = '$t$';
    Handle_Axis4.YLabel.String = '$Z_1$'; 
    Handle_Axis4.XLabel.Interpreter = 'latex';
    Handle_Axis4.YLabel.Interpreter = 'latex';
Handle_Axis5 = subplot(3,3,5);
    Handle_Axis5.Box = 'on';
    Handle_Axis5.FontSize = FontSize;
    hold(Handle_Axis5, 'on');
    grid(Handle_Axis5, 'on');
    Handle_Axis5.XMinorGrid = 'on';
    Handle_Axis5.YMinorGrid = 'on';
    Handle_Axis5.XLabel.String = '$t$';
    Handle_Axis5.YLabel.String = '$Z_2$'; 
    Handle_Axis5.XLabel.Interpreter = 'latex';
    Handle_Axis5.YLabel.Interpreter = 'latex';
Handle_Axis6 = subplot(3,3,6);
    Handle_Axis6.Box = 'on';
    Handle_Axis6.FontSize = FontSize;
    hold(Handle_Axis6, 'on');
    grid(Handle_Axis6, 'on');
    Handle_Axis6.XMinorGrid = 'on';
    Handle_Axis6.YMinorGrid = 'on';
    Handle_Axis6.XLabel.String = '$t$';
    Handle_Axis6.YLabel.String = '$Z_1''$'; 
    Handle_Axis6.XLabel.Interpreter = 'latex';
    Handle_Axis6.YLabel.Interpreter = 'latex';
Handle_Axis7 = subplot(3,3,7);
    Handle_Axis7.Box = 'on';
    Handle_Axis7.FontSize = FontSize;
    hold(Handle_Axis7, 'on');
    grid(Handle_Axis7, 'on');
    Handle_Axis7.XMinorGrid = 'on';
    Handle_Axis7.YMinorGrid = 'on';
    Handle_Axis7.XLabel.String = '$t$';
    Handle_Axis7.YLabel.String = '$Z_2''$'; 
    Handle_Axis7.XLabel.Interpreter = 'latex';
    Handle_Axis7.YLabel.Interpreter = 'latex';
Handle_Axis8 = subplot(3,3,8);
    Handle_Axis8.Box = 'on';
    Handle_Axis8.FontSize = FontSize;
    hold(Handle_Axis8, 'on');
    grid(Handle_Axis8, 'on');
    Handle_Axis8.XMinorGrid = 'on';
    Handle_Axis8.YMinorGrid = 'on';
    Handle_Axis8.XLabel.String = '$t$';
    Handle_Axis8.YLabel.String = '$X_1''$'; 
    Handle_Axis8.XLabel.Interpreter = 'latex';
    Handle_Axis8.YLabel.Interpreter = 'latex';

%% Response
plot(Handle_Axis1, time_vector, X_1, 'Color', MyGreen, 'LineWidth', LineWidth);
plot(Handle_Axis2, time_vector, X_2, 'Color', MyGreen, 'LineWidth', LineWidth);
plot(Handle_Axis3, time_vector, A, 'Color', MyGreen, 'LineWidth', LineWidth);
plot(Handle_Axis4, time_vector, Z_1, 'Color', MyBlue, 'LineWidth', LineWidth);
plot(Handle_Axis5, time_vector, Z_2, 'Color', MyBlue, 'LineWidth', LineWidth);
plot(Handle_Axis6, time_vector, Z_prime_1, 'Color', MyPurple, 'LineWidth', LineWidth);
plot(Handle_Axis7, time_vector, Z_prime_2, 'Color', MyPurple, 'LineWidth', LineWidth);
plot(Handle_Axis8, time_vector, X_prime_1, 'Color', MyPurple, 'LineWidth', LineWidth);

%% Steady State
plot(Handle_Axis1, time_vector(end), X_bar_1, 'Color', MyGreen, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis2, time_vector(end), X_bar_2, 'Color', MyGreen, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis3, time_vector(end), A_bar, 'Color', MyGreen, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis4, time_vector(end), Z_bar_1, 'Color', MyBlue, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis5, time_vector(end), Z_bar_2, 'Color', MyBlue, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis6, time_vector(end), Z_bar_prime_1, 'Color', MyPurple, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis7, time_vector(end), Z_bar_prime_2, 'Color', MyPurple, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis8, time_vector(end), X_bar_prime_1, 'Color', MyPurple, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);

%% Saving
if Save_Flag == 1
    print(Handle_Figure1, 'Response', '-dpdf');
end


