classdef Class_LumpedParameters
    properties
        gamma_bar           % [1/a.u.]                  gamma_bar = (gamma/k) * ((c+gamma)/c) / c_x
        delta_bar           % [dimensionless]           delta_bar = delta / k_1
        eta_bar             % [1/nM]                    eta_bar = eta / k_1
        kappa_bar           % [a.u.]                    kappa_bar = c_x * kappa
        kappa_bar_2         % [a.u.]                    kappa_bar_2 = c_x * sqrt(kappa * kappa_2)
        k_bar_0             % [Dimensionless]           k_bar_0 = k_0 / k_1
        k_bar_2             % [Dimensionless]           k_bar_2 = k_2 / k_1
        Delta_1           	% [1/nM]                    Delta_1 = k_3 / (kappa_3*c_bar)
        Delta_2           	% [1/a.u./nM]            	Delta_2 = (k_prime_3*gamma) / (kappa_prime_3^2*c_bar*c*c_x)
        Delta_prime_1     	% [1/a.u.]               	Delta_prime_1 = gamma / (kappa_3*c*c_x)
        Delta_prime_2      	% [1/a.u.^2]               	Delta_prime_2 = gamma^2 / (kappa_prime_3^2*c^2*c_x^2)
        gamma_prime_bar     % [nM]                      gamma_prime_bar = gamma_prime/a_prime
        kappa_prime_bar     % [nM]                      kappa_prime_bar = (d_prime/a_prime) * (gamma_prime/delta)
        k_prime_bar         % [Dimensionless]           k_prime_bar = k_prime / delta_prime
        k_prime_bar_0   	% [Dimensionless]           k_prime_bar_0 = k_prime_0 / k_1
        k_prime_bar_2     	% [Dimensionless]           k_prime_bar_2 = k_prime_2 / k_1
    end
    
    methods
        function obj = Class_LumpedParameters(Vector)
            if nargin > 0
                obj.gamma_bar = Vector(1);
                obj.delta_bar = Vector(2);
                obj.eta_bar = Vector(3);
                obj.kappa_bar = Vector(4);
                obj.kappa_bar_2 = Vector(5);
                obj.k_bar_0 = Vector(6);
                obj.k_bar_2 = Vector(7);
                obj.Delta_1 = Vector(8);
                obj.Delta_2 = Vector(9);
                obj.Delta_prime_1 = Vector(10);
                obj.Delta_prime_2 = Vector(11);
                obj.gamma_prime_bar = Vector(12);
                obj.kappa_prime_bar = Vector(13);
                obj.k_prime_bar = Vector(14);
                obj.k_prime_bar_0 = Vector(15);
                obj.k_prime_bar_2 = Vector(16);
            end
        end
        
        function Vector = Vector(obj)
            Vector = [obj.gamma_bar; obj.delta_bar; obj.eta_bar; ...
                obj.kappa_bar; obj.kappa_bar_2; ...
                obj.k_bar_0; obj.k_bar_2; ...
                obj.Delta_1; obj.Delta_2; obj.Delta_prime_1; obj.Delta_prime_2; ...
                obj.gamma_prime_bar; obj.kappa_prime_bar; obj.k_prime_bar; ...
                obj.k_prime_bar_0; obj.k_prime_bar_2];
        end     
    end
end

