function [] = Visualize_Data_NetworkPerturbation(DataTable)
%% Extract Inputs and Measurements
[Input_Data, Measurement_Data, ~, Measurement1_Data, Measurement2_Data, Measurement3_Data] = GenerateData_I_NetworkPerturbation(DataTable);

%% General Figure Settings 
Colors = lines(10);
MyGreen = Colors(5,:); MyRed = Colors(2,:); MyYellow = Colors(3,:); MyPurple = Colors(4,:);
DrugColor = MyRed;
NetworkColor = MyPurple;

SS = 4; % Screen Scale
Figure_Width = 12 * SS;
Figure_Height = 4 * SS;
FontSize = 7 * SS;
FontSize_Small = 4 * SS;
LineWidth_Thin = 0.01 * SS;
LineWidth = 1 * SS;
LineWidth_Thick = 1 * SS;
MarkerSize = 2 * SS;
LineWidth_Marker = LineWidth/2;
BarWidth1 = 0.6;
BarWidth2 = 0.45;
BarWidth3 = 0.3;
BarWidth4 = 0.15;
Transparency = 0.5;
DotSpace1 = 0.1;
DotSpace2 = DotSpace1 * 0.7;
DotSpace3 = DotSpace1 * 0.5;
DotSpace4 = DotSpace1 * 0.2;

%% Counts
N_SetPoints = 2;

%% Compute Steady-State Error
% SSError_OLNNP = round(100 * abs(Measurement_Data(2) - Measurement_Data(1)) ./ Measurement_Data(1));
% SSError_OLNP = round(100 * abs(Measurement_Data(4) - Measurement_Data(3)) ./ Measurement_Data(3));
% SSError_CLNNP = round(100 * abs(Measurement_Data(6) - Measurement_Data(5)) ./ Measurement_Data(5));
% SSError_CLNP = round(100 * abs(Measurement_Data(8) - Measurement_Data(7)) ./ Measurement_Data(7));

%% Figure 1 Settings
Handle_Figure1 = figure();
    Handle_Figure1.Color = [1 1 1];
    Handle_Figure1.PaperUnits = 'centimeters';
    Handle_Figure1.Units = 'centimeters';
    Handle_Figure1.Position = [1, 20, Figure_Width, Figure_Height];
    Handle_Figure1.PaperPositionMode = 'auto';
    Handle_Figure1.PaperSize = [Handle_Figure1.PaperPosition(3), Handle_Figure1.PaperPosition(4)];
figure(Handle_Figure1);

Handle_Axis1 = subplot(1,2,1);
    Handle_Axis1.Position = [0.075, 0.17, 0.45, 0.66];
    hold(Handle_Axis1, 'on');
    Handle_Axis1.Box = 'on';
    Handle_Axis1.FontSize = FontSize;
    hold(Handle_Axis1, 'on');
    grid(Handle_Axis1, 'on');
    Handle_Axis1.XMinorGrid = 'off';
    Handle_Axis1.YMinorGrid = 'off';
    Handle_Axis1.XLabel.String = 'Plasmid Ratio G_1/G_2';
    Handle_Axis1.YLabel.String = {'Fluorescence [a.u.]'};
    Handle_Axis1.Title.String = 'Open Loop';
	Handle_Axis1.YLim = [0, 2.6e5];
%   	Handle_Axis1.YLim = [2e3, 1e6];
%     Handle_Axis1.YScale = 'log';
    
Handle_Axis2 = subplot(1,2,2);
    Handle_Axis2.Position = [0.54, 0.17, 0.45, 0.66];
    hold(Handle_Axis2, 'on');
    Handle_Axis2.Box = 'on';
    Handle_Axis2.FontSize = FontSize;
    hold(Handle_Axis2, 'on');
    grid(Handle_Axis2, 'on');
    Handle_Axis2.XMinorGrid = 'off';
    Handle_Axis2.YMinorGrid = 'off';
    Handle_Axis2.XLabel.String = 'Plasmid Ratio G_1/G_2';
    Handle_Axis2.Title.String = 'Closed Loop';
    Handle_Axis2.YLim = Handle_Axis1.YLim;
%     Handle_Axis2.YScale = 'log';
    
%% Plotting Figure 1
% ------------------------
% Open Loop No Perturbation No Disturbance
% ------------------------
% Bars
Handle_Bar11 = bar(Handle_Axis1, Input_Data([1,3],1) ./ Input_Data([1,3],2), Measurement_Data([1,3]), BarWidth1);
    Handle_Bar11.FaceColor = MyGreen;
    Handle_Bar11.FaceAlpha = Transparency;
    Handle_Bar11.LineWidth = LineWidth/2;
    Handle_Bar11.EdgeColor = MyGreen;

% ------------------------
% Open Loop No Perturbation With Disturbance
% ------------------------
% Bars
Handle_Bar11 = bar(Handle_Axis1, Input_Data([2,4],1) ./ Input_Data([2,4],2), Measurement_Data([2,4]), BarWidth2);
    Handle_Bar11.FaceColor = DrugColor;
    Handle_Bar11.FaceAlpha = Transparency;
    Handle_Bar11.LineWidth = LineWidth/2;
    Handle_Bar11.EdgeColor = MyGreen;

% ------------------------
% Open Loop With Perturbation No Disturbance
% ------------------------
% Bars
Handle_Bar11 = bar(Handle_Axis1, Input_Data([5,7],1) ./ Input_Data([5,7],2), Measurement_Data([5,7]), BarWidth3);
    Handle_Bar11.FaceColor = MyGreen;
    Handle_Bar11.LineWidth = LineWidth/2;
    Handle_Bar11.FaceAlpha = Transparency;
    Handle_Bar11.LineWidth = LineWidth/2;
    Handle_Bar11.EdgeColor = NetworkColor;
% ------------------------
% Open Loop With Perturbation With Disturbance
% ------------------------
% Bars
Handle_Bar11 = bar(Handle_Axis1, Input_Data([6,8],1) ./ Input_Data([6,8],2), Measurement_Data([6,8]), BarWidth4);
    Handle_Bar11.FaceColor = DrugColor;
    Handle_Bar11.LineWidth = LineWidth/2;
    Handle_Bar11.FaceAlpha = Transparency;
    Handle_Bar11.LineWidth = LineWidth/2;
    Handle_Bar11.EdgeColor = NetworkColor;
    
% Data Points Open Loop No Perturbation No Disturbance
plot(Handle_Axis1, Input_Data([1,3],1) ./ Input_Data([1,3],2) - DotSpace1, Measurement1_Data([1,3]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', MyGreen);
plot(Handle_Axis1, Input_Data([1,3],1) ./ Input_Data([1,3],2), Measurement2_Data([1,3]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', MyGreen);
plot(Handle_Axis1, Input_Data([1,3],1) ./ Input_Data([1,3],2) + DotSpace1, Measurement3_Data([1,3]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', MyGreen);
% Data Points Open Loop No Perturbation With Disturbance
plot(Handle_Axis1, Input_Data([2,4],1) ./ Input_Data([2,4],2) - DotSpace2, Measurement1_Data([2,4]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', MyGreen);
plot(Handle_Axis1, Input_Data([2,4],1) ./ Input_Data([2,4],2), Measurement2_Data([2,4]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', MyGreen);
plot(Handle_Axis1, Input_Data([2,4],1) ./ Input_Data([2,4],2) + DotSpace2, Measurement3_Data([2,4]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', MyGreen);
% Data Points Open Loop With Perturbation No Disturbance
plot(Handle_Axis1, Input_Data([5,7],1) ./ Input_Data([5,7],2) - DotSpace3, Measurement1_Data([5,7]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', NetworkColor);
plot(Handle_Axis1, Input_Data([5,7],1) ./ Input_Data([5,7],2), Measurement2_Data([5,7]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', NetworkColor);
plot(Handle_Axis1, Input_Data([5,7],1) ./ Input_Data([5,7],2) + DotSpace3, Measurement3_Data([5,7]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', NetworkColor);
% Data Points Open Loop With Perturbation With Disturbance
plot(Handle_Axis1, Input_Data([6,8],1) ./ Input_Data([6,8],2) - DotSpace4, Measurement1_Data([6,8]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', NetworkColor);
plot(Handle_Axis1, Input_Data([6,8],1) ./ Input_Data([6,8],2), Measurement2_Data([6,8]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', NetworkColor);
plot(Handle_Axis1, Input_Data([6,8],1) ./ Input_Data([6,8],2)+ DotSpace4, Measurement3_Data([6,8]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', NetworkColor);
 
% ------------------------
% Closed Loop No Perturbation No Disturbance
% ------------------------
% Bars
Handle_Bar21 = bar(Handle_Axis2, Input_Data([9,11],1) ./ Input_Data([9,11],2), Measurement_Data([9,11]), BarWidth1);
    Handle_Bar21.FaceColor = MyGreen;
    Handle_Bar21.FaceAlpha = Transparency;
    Handle_Bar21.LineWidth = LineWidth/2;
    Handle_Bar21.EdgeColor = MyGreen;

% ------------------------
% Closed Loop No Perturbation With Disturbance
% ------------------------
% Bars
Handle_Bar21 = bar(Handle_Axis2, Input_Data([10,12],1) ./ Input_Data([10,12],2), Measurement_Data([10,12]), BarWidth2);
    Handle_Bar21.FaceColor = DrugColor;
    Handle_Bar21.FaceAlpha = Transparency;
    Handle_Bar21.LineWidth = LineWidth/2;
    Handle_Bar21.EdgeColor = MyGreen;

% ------------------------
% Closed Loop With Perturbation No Disturbance
% ------------------------
% Bars
Handle_Bar21 = bar(Handle_Axis2, Input_Data([13,15],1) ./ Input_Data([13,15],2), Measurement_Data([13,15]), BarWidth3);
    Handle_Bar21.FaceColor = MyGreen;
    Handle_Bar21.LineWidth = LineWidth/2;
    Handle_Bar21.FaceAlpha = Transparency;
    Handle_Bar21.LineWidth = LineWidth/2;
    Handle_Bar21.EdgeColor = NetworkColor;
% ------------------------
% Closed Loop With Perturbation With Disturbance
% ------------------------
% Bars
Handle_Bar21 = bar(Handle_Axis2, Input_Data([14,16],1) ./ Input_Data([14,16],2), Measurement_Data([14,16]), BarWidth4);
    Handle_Bar21.FaceColor = DrugColor;
    Handle_Bar21.LineWidth = LineWidth/2;
    Handle_Bar21.FaceAlpha = Transparency;
    Handle_Bar21.LineWidth = LineWidth/2;
    Handle_Bar21.EdgeColor = NetworkColor;
    
% Data Points Closed Loop No Perturbation No Disturbance
plot(Handle_Axis2, Input_Data([9,11],1) ./ Input_Data([9,11],2) - DotSpace1, Measurement1_Data([9,11]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', MyGreen);
plot(Handle_Axis2, Input_Data([9,11],1) ./ Input_Data([9,11],2), Measurement2_Data([9,11]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', MyGreen);
plot(Handle_Axis2, Input_Data([9,11],1) ./ Input_Data([9,11],2) + DotSpace1, Measurement3_Data([9,11]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', MyGreen);
% Data Points Closed Loop No Perturbation With Disturbance
plot(Handle_Axis2, Input_Data([10,12],1) ./ Input_Data([10,12],2) - DotSpace2, Measurement1_Data([10,12]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', MyGreen);
plot(Handle_Axis2, Input_Data([10,12],1) ./ Input_Data([10,12],2), Measurement2_Data([10,12]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', MyGreen);
plot(Handle_Axis2, Input_Data([10,12],1) ./ Input_Data([10,12],2) + DotSpace2, Measurement3_Data([10,12]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', MyGreen);
% Data Points Closed Loop With Perturbation No Disturbance
plot(Handle_Axis2, Input_Data([13,15],1) ./ Input_Data([13,15],2) - DotSpace3, Measurement1_Data([13,15]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', NetworkColor);
plot(Handle_Axis2, Input_Data([13,15],1) ./ Input_Data([13,15],2), Measurement2_Data([13,15]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', NetworkColor);
plot(Handle_Axis2, Input_Data([13,15],1) ./ Input_Data([13,15],2) + DotSpace3, Measurement3_Data([13,15]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', NetworkColor);
% Data Points Closed Loop With Perturbation With Disturbance
plot(Handle_Axis2, Input_Data([14,16],1) ./ Input_Data([14,16],2) - DotSpace4, Measurement1_Data([14,16]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', NetworkColor);
plot(Handle_Axis2, Input_Data([14,16],1) ./ Input_Data([14,16],2), Measurement2_Data([14,16]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', NetworkColor);
plot(Handle_Axis2, Input_Data([14,16],1) ./ Input_Data([14,16],2)+ DotSpace4, Measurement3_Data([14,16]), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', NetworkColor);

% % -------
% % Legends
% % -------
% Handle_White = plot(Handle_Axis2, -1, -1, 'Color', [1, 1, 1]);
% Handle_DataPoint = plot(Handle_Axis2, -1, -1, 'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', [0.5, 0.5, 0.5], 'Color', 'k'); 
% Handle_Bar = bar(Handle_Axis2, -1, -1, 10);
%     Handle_Bar.FaceColor = [0.5, 0.5, 0.5];
%     Handle_Bar.FaceAlpha = Transparency;
% Handle_Legend1 = legend(Handle_Axis2, [Handle_DataPoint, Handle_White, Handle_Bar, Handle_White], 'Measurement', '', 'Average', '');
%     Handle_Legend1.Orientation = 'Horizontal';
%     Handle_Legend1.EdgeColor = 'none';
%     Handle_Legend1.Position = [0.065, 0.935, 0.45, 0.05];
%     Handle_Legend1.Color = 'none';
% Handle_Box = annotation('rectangle', [Handle_Legend1.Position(1) + Handle_Legend1.Position(3) + 0.01, Handle_Legend1.Position(2), Handle_Legend1.Position(3:4) + [0.02, 0]]);
% Handle_Color1 = annotation('rectangle');
%     Handle_Color1.Position = [Handle_Box.Position(1) - 0.1, Handle_Box.Position(2), 0.01*1.5, 2*0.015*1.5];
%     Handle_Color1.EdgeColor = 'none';
%     Handle_Color1.FaceColor = MyGreen;
% Handle_Text1 = annotation('textbox');
%     Handle_Text1.Position = Handle_Color1.Position + [0.015, 0, 0.2, 0];
%     Handle_Text1.EdgeColor = 'none';
%     Handle_Text1.String = 'Without Disturbance';
%     Handle_Text1.FontSize = Handle_Legend1.FontSize;
%     Handle_Text1.FontName = Handle_Legend1.FontName;
%     Handle_Text1.VerticalAlignment = 'Middle';
% Handle_Color3 = annotation('rectangle');
%     Handle_Color3.Position = [Handle_Text1.Position(1) + Handle_Text1.Position(3), Handle_Color1.Position(2:4)];
%     Handle_Color3.EdgeColor = 'none';
%     Handle_Color3.FaceColor = MyYellow;
% Handle_Text2 = annotation('textbox');
%     Handle_Text2.Position = Handle_Color3.Position + [0.015, 0, 0.2, 0];
%     Handle_Text2.EdgeColor = 'none';
%     Handle_Text2.String = 'With Disturbance';
%     Handle_Text2.FontSize = Handle_Legend1.FontSize;
%     Handle_Text2.FontName = Handle_Legend1.FontName;
%     Handle_Text2.VerticalAlignment = 'Middle';
% Handle_Text3 = annotation('textbox');
%     Handle_Text3.Position = Handle_Color3.Position + [0.2, 0, 0.2, 0];
%     Handle_Text3.EdgeColor = 'none';
%     Handle_Text3.Color = MyRed;
%     Handle_Text3.String = '% Steady-State Error';
%     Handle_Text3.FontSize = Handle_Legend1.FontSize/1.5;
%     Handle_Text3.FontName = Handle_Legend1.FontName;
%     Handle_Text3.VerticalAlignment = 'Middle';
% Handle_Box.Position = [0.075, 0.92, 0.915, 0.08];

end

