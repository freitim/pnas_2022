function [] = Compare_FullData_Simulation_Prediction_Normalized_Mini(DataTable, LumpedParameters, N_Sim, XScale)
%% Extract Inputs and Measurements
[Input_Data, Measurement_Data, ~, Measurement1_Data, Measurement2_Data, Measurement3_Data] = GenerateData_I_NetworkPerturbation(DataTable);

%% Construct Inputs
G_1_Min = 0;
G_1_Max = Input_Data(1,2)*1.2;
G_1 = linspace(G_1_Min, G_1_Max, N_Sim)';
G_2 = Input_Data(1,2) * ones(N_Sim,1);

%% Compute Fixed Points for the Open Loop Without Network Perturbation
Type = zeros(N_Sim,1);
G_prime_2 = zeros(N_Sim,1);
% Without Disturbance
D = zeros(N_Sim,1);
Input = [G_1, G_2, G_prime_2, D];
Measurement_OL_NP_ND = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);
% With Disturbance
D = Input_Data(end,4) * ones(N_Sim,1);
Input = [G_1, G_2, G_prime_2, D];
Measurement_OL_NP_D = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);

%% Compute Fixed Points for the Open Loop With Network Perturbation
Type = 2 * ones(N_Sim,1);
G_prime_2 = Input_Data(end,3) * ones(N_Sim,1);
% Without Disturbance
D = zeros(N_Sim,1);
Input = [G_1, G_2, G_prime_2, D];
Measurement_OL_P_ND = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);
% With Disturbance
D = Input_Data(end,4) * ones(N_Sim,1);
Input = [G_1, G_2, G_prime_2, D];
Measurement_OL_P_D = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);

%% Compute Fixed Points for the Closed Loop Without Network Perturbation
Type = ones(N_Sim,1);
G_prime_2 = zeros(N_Sim,1);
% Without Disturbance
D = zeros(N_Sim,1);
Input = [G_1, G_2, G_prime_2, D];
Measurement_CL_NP_ND = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);
% With Disturbance
D = Input_Data(end,4) * ones(N_Sim,1);
Input = [G_1, G_2, G_prime_2, D];
Measurement_CL_NP_D = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);

%% Compute Fixed Points for the Closed Loop With Network Perturbation
Type = 3 * ones(N_Sim,1);
G_prime_2 = Input_Data(end,3) * ones(N_Sim,1);
% Without Disturbance
D = zeros(N_Sim,1);
Input = [G_1, G_2, G_prime_2, D];
Measurement_CL_P_ND = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);
% With Disturbance
D = Input_Data(end,4) * ones(N_Sim,1);
Input = [G_1, G_2, G_prime_2, D];
Measurement_CL_P_D = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);

%% General Figure Settings 
MyRed = [228,26,28]/255;
MyGreen = [77,175,74]/255;
MyPurple = [152,78,163]/255;
Colors = lines(10);
MyYellow = Colors(3,:); 
% MyYellow = (MyRed + MyPurple)/2;
DrugColor = MyRed;
NetworkColor = MyPurple;
DrugNetworkColor = MyYellow;

SS = 4; % Screen Scale
Shrink = 0.9;
Figure_Width = 12 * SS;
Figure_Height = Shrink * 3 * SS;
FontSize = 7 * SS;
LineWidth = 1 * SS;
LineWidth_Marker = LineWidth/5;
MarkerSize = 1.5 * SS;
MarkerSize_Big = 3*MarkerSize;
MarkerSize_Small = 2*MarkerSize;
DotSpace = 0.001;

%% Figure 1 Settings
Handle_Figure1 = figure();
    Handle_Figure1.Color = 'none';
    Handle_Figure1.PaperUnits = 'centimeters';
    Handle_Figure1.Units = 'centimeters';
    Handle_Figure1.Position = [1, 20, Figure_Width, Figure_Height];
    Handle_Figure1.PaperPositionMode = 'auto';
    Handle_Figure1.PaperSize = [Handle_Figure1.PaperPosition(3), Handle_Figure1.PaperPosition(4)];
    set(Handle_Figure1, 'InvertHardCopy', 'off');
figure(Handle_Figure1);

Handle_Axis1 = subplot(2,2,1);
    Handle_Axis1.Position = [0.09, 0.12, 0.4, 0.66 / Shrink];
    hold(Handle_Axis1, 'on');
    Handle_Axis1.Box = 'on';
    Handle_Axis1.FontSize = FontSize;
    hold(Handle_Axis1, 'on');
    grid(Handle_Axis1, 'on');
    Handle_Axis1.XMinorGrid = 'off';
    Handle_Axis1.YMinorGrid = 'off';
    Handle_Axis1.XLabel.Interpreter = 'latex';
    Handle_Axis1.YLabel.String = {'Normalized'; 'Fluorescence'};
    Handle_Axis1.XTick = [0, 1/4, 1/2];
    Handle_Axis1.XTickLabel = {'0', '1/4', '1/2'};
    Handle_Axis1.XLim = [0, 0.52];
    Handle_Axis1.YLim = [0, 1.2];
    Handle_Axis1.XScale = XScale;
    
Handle_Axis2 = subplot(2,2,2);
    Handle_Axis2.Position = [0.53, 0.12, 0.4, 0.66 / Shrink];
    hold(Handle_Axis2, 'on');
    Handle_Axis2.Box = 'on';
    Handle_Axis2.FontSize = FontSize;
    hold(Handle_Axis2, 'on');
    grid(Handle_Axis2, 'on');
    Handle_Axis2.XMinorGrid = 'off';
    Handle_Axis2.YMinorGrid = 'off';
    Handle_Axis2.XLabel.Interpreter = 'latex';
    Handle_Axis2.XTick = [0, 1/4, 1/2];
    Handle_Axis2.XTickLabel = {'0', '1/4', '1/2'};
    Handle_Axis2.XLim = [0, 0.52];
    Handle_Axis2.YLim = [0, 1.2];
    Handle_Axis2.XScale = XScale;
    Handle_Axis2.YTickLabel = {};

%% Plotting Figure 1
Normalization_OL = Measurement_Data(3);
Normalization_CL = Measurement_Data(11);
% ------------------------
% Open Loop No Perturbation No Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis1, G_1 ./ G_2, Measurement_OL_NP_ND / Normalization_OL, 'LineWidth', LineWidth, 'Color', MyGreen);
% Diamonds
plot(Handle_Axis1, Input_Data([1,3],1) ./ Input_Data([1,3],2), Measurement_Data([1,3]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
% Data Points 
plot(Handle_Axis1, Input_Data([1,3],1) ./ Input_Data([1,3],2) - DotSpace, Measurement1_Data([1,3]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis1, Input_Data([1,3],1) ./ Input_Data([1,3],2), Measurement2_Data([1,3]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis1, Input_Data([1,3],1) ./ Input_Data([1,3],2) + DotSpace, Measurement3_Data([1,3]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
 
% ------------------------
% Open Loop No Perturbation With Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis1, G_1 ./ G_2, Measurement_OL_NP_D / Normalization_OL, 'LineWidth', LineWidth, 'Color', DrugColor);
% Diamonds
plot(Handle_Axis1, Input_Data([2,4],1) ./ Input_Data([2,4],2), Measurement_Data([2,4]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
% Data Points 
plot(Handle_Axis1, Input_Data([2,4],1) ./ Input_Data([2,4],2) - DotSpace, Measurement1_Data([2,4]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis1, Input_Data([2,4],1) ./ Input_Data([2,4],2), Measurement2_Data([2,4]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis1, Input_Data([2,4],1) ./ Input_Data([2,4],2) + DotSpace, Measurement3_Data([2,4]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
 
% ------------------------
% Open Loop With Perturbation No Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis1, G_1 ./ G_2, Measurement_OL_P_ND / Normalization_OL, 'LineWidth', LineWidth, 'Color', NetworkColor);
% Diamonds
plot(Handle_Axis1, Input_Data([5,7],1) ./ Input_Data([5,7],2), Measurement_Data([5,7]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Small, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
% Data Points 
plot(Handle_Axis1, Input_Data([5,7],1) ./ Input_Data([5,7],2) - DotSpace, Measurement1_Data([5,7]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
plot(Handle_Axis1, Input_Data([5,7],1) ./ Input_Data([5,7],2), Measurement2_Data([5,7]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
plot(Handle_Axis1, Input_Data([5,7],1) ./ Input_Data([5,7],2) + DotSpace, Measurement3_Data([5,7]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
 
% ------------------------
% Open Loop With Perturbation With Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis1, G_1 ./ G_2, Measurement_OL_P_D / Normalization_OL, 'LineWidth', LineWidth/1.5, 'Color', DrugNetworkColor, 'LineStyle', '--');
% Diamonds
plot(Handle_Axis1, Input_Data([6,8],1) ./ Input_Data([6,8],2), Measurement_Data([6,8]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');
% Data Points 
plot(Handle_Axis1, Input_Data([6,8],1) ./ Input_Data([6,8],2) - DotSpace, Measurement1_Data([6,8]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');
plot(Handle_Axis1, Input_Data([6,8],1) ./ Input_Data([6,8],2), Measurement2_Data([6,8]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');
plot(Handle_Axis1, Input_Data([6,8],1) ./ Input_Data([6,8],2)+ DotSpace, Measurement3_Data([6,8]) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');

yyaxis(Handle_Axis1, 'right');
Handle_Axis1.YLim = [0, 1.2*Normalization_OL];
Handle_Axis1.YAxis(2).Color = MyGreen;

% ------------------------
% Closed Loop No Perturbation No Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis2, G_1 ./ G_2, Measurement_CL_NP_ND / Normalization_CL, 'LineWidth', LineWidth, 'Color', MyGreen);
% Diamonds
plot(Handle_Axis2, Input_Data([9,11],1) ./ Input_Data([9,11],2), Measurement_Data([9,11]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
% Data Points 
plot(Handle_Axis2, Input_Data([9,11],1) ./ Input_Data([9,11],2) - DotSpace, Measurement1_Data([9,11]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis2, Input_Data([9,11],1) ./ Input_Data([9,11],2), Measurement2_Data([9,11]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis2, Input_Data([9,11],1) ./ Input_Data([9,11],2) + DotSpace, Measurement3_Data([9,11]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
 
% ------------------------
% Closed Loop No Perturbation With Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis2, G_1 ./ G_2, Measurement_CL_NP_D / Normalization_CL, 'LineWidth', LineWidth, 'Color', DrugColor);
% Diamonds
plot(Handle_Axis2, Input_Data([10,12],1) ./ Input_Data([10,12],2), Measurement_Data([10,12]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
% Data Points 
plot(Handle_Axis2, Input_Data([10,12],1) ./ Input_Data([10,12],2) - DotSpace, Measurement1_Data([10,12]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis2, Input_Data([10,12],1) ./ Input_Data([10,12],2), Measurement2_Data([10,12]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis2, Input_Data([10,12],1) ./ Input_Data([10,12],2) + DotSpace, Measurement3_Data([10,12]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
 
% ------------------------
% Closed Loop With Perturbation No Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis2, G_1 ./ G_2, Measurement_CL_P_ND / Normalization_CL, 'LineWidth', LineWidth, 'Color', NetworkColor);
% Diamonds
plot(Handle_Axis2, Input_Data([13,15],1) ./ Input_Data([13,15],2), Measurement_Data([13,15]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Small, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
% Data Points 
plot(Handle_Axis2, Input_Data([13,15],1) ./ Input_Data([13,15],2) - DotSpace, Measurement1_Data([13,15]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
plot(Handle_Axis2, Input_Data([13,15],1) ./ Input_Data([13,15],2), Measurement2_Data([13,15]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
plot(Handle_Axis2, Input_Data([13,15],1) ./ Input_Data([13,15],2) + DotSpace, Measurement3_Data([13,15]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
 
% ------------------------
% Closed Loop With Perturbation With Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis2, G_1 ./ G_2, Measurement_CL_P_D / Normalization_CL, 'LineWidth', LineWidth/1.5, 'Color', DrugNetworkColor, 'LineStyle', '--');
% Diamonds
plot(Handle_Axis2, Input_Data([14,16],1) ./ Input_Data([14,16],2), Measurement_Data([14,16]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');
% Data Points 
plot(Handle_Axis2, Input_Data([14,16],1) ./ Input_Data([14,16],2) - DotSpace, Measurement1_Data([14,16]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');
plot(Handle_Axis2, Input_Data([14,16],1) ./ Input_Data([14,16],2), Measurement2_Data([14,16]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');
plot(Handle_Axis2, Input_Data([14,16],1) ./ Input_Data([14,16],2)+ DotSpace, Measurement3_Data([14,16]) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');
 
yyaxis(Handle_Axis2, 'right');
Handle_Axis2.YLim = [0, 1.2*Normalization_CL];
Handle_Axis2.YLabel.String = 'Fluorescence [a.u.]';
Handle_Axis2.YAxis(2).Color = MyGreen;
Handle_Axis2.YLabel.Position(1) = Handle_Axis2.YLabel.Position(1)*1.05;


% -------
% Legends
% -------
yyaxis(Handle_Axis2, 'left');
Handle_NNP_ND = bar(Handle_Axis2, -1, -1, 'FaceColor', MyGreen, 'EdgeColor', MyGreen);
Handle_NNP_D = bar(Handle_Axis2, -1, -1, 'FaceColor', DrugColor, 'EdgeColor', DrugColor);
Handle_NP_ND = bar(Handle_Axis2, -1, -1, 'FaceColor', NetworkColor, 'EdgeColor', NetworkColor);
Handle_NP_D = bar(Handle_Axis2, -1, -1, 'FaceColor', DrugNetworkColor, 'EdgeColor', DrugNetworkColor);
Handle_Legend2 = legend(Handle_Axis2, [Handle_NNP_ND, Handle_NNP_D, Handle_NP_ND, Handle_NP_D],  {'Without Disturbance, Without Network Perturbation', 'With Disturbance, Without Network Perturbation', 'Without Disturbance, With Network Perturbation', 'With Disturbance, With Network Perturbation'});
    Handle_Legend2.FontSize = FontSize / 1.8;
    Handle_Legend2.Location = 'SouthEast';
Handle_Box.Position = [0.15, 0.92, 0.7, 0.08];
end

