%% Clear Workspace
close all;
clear;
clc;
Save_Flag = 1;

%% Select Parameter Fit
% load Results_NP_ND;
% load Results_NP_D;
load Results_P_ND;
    
%% Generate Data
Data = load('FullData_I_Perturbed1');
[Input_Data, Measurement_Data, ~, ~, ~, ~] = GenerateData_I_NetworkPerturbation(Data.DataTable);

%% Override Manual Tuning
% LumpedParameters = LumpedParameters_Manual;
% LumpedParameters.gamma_bar = (12.8/9.5) * LumpedParameters.gamma_bar;                            % [1/a.u.]                  gamma_bar = (gamma/k) * ((c+gamma)/c) / c_x
% LumpedParameters.delta_bar = (9.5/12.8) * LumpedParameters.delta_bar;                            % [dimensionless]           delta_bar = delta / k_1
% LumpedParameters.eta_bar = 25.9*LumpedParameters.eta_bar;                                % [1/nM]                    eta_bar = eta / k_1
% LumpedParameters.kappa_bar = 0.8*LumpedParameters.kappa_bar;                            % [a.u.]                    kappa_bar = c_x * kappa
% LumpedParameters.kappa_bar_2 = 0.2*LumpedParameters.kappa_bar_2;                        % [a.u.]                    kappa_bar_2 = c_x * sqrt(kappa * kappa_2)
% LumpedParameters.k_bar_0 =  1*LumpedParameters.k_bar_0;                               % [Dimensionless]           k_bar_0 = k_0 / k_1
% LumpedParameters.k_bar_2 = 0.05*LumpedParameters.k_bar_2;                                % [Dimensionless]           k_bar_2 = k_2 / k_1
% LumpedParameters.Delta_1 = 1*LumpedParameters.Delta_1;                                % [1/nM]                    Delta_1 = k_3 / (kappa_3*c_bar)
LumpedParameters.Delta_2 = 0.9*LumpedParameters.Delta_2;                                % [1/a.u./nM]            	Delta_2 = (k_prime_3*gamma) / (kappa_prime_3^2*c_bar*c*c_x)
% LumpedParameters.Delta_prime_1 = 1*LumpedParameters.Delta_prime_1;                    % [1/a.u.]               	Delta_prime_1 = gamma / (kappa_3*c*c_x)
% LumpedParameters.Delta_prime_2 = 1*LumpedParameters.Delta_prime_2;                    % [1/a.u.^2]               	Delta_prime_2 = gamma^2 / (kappa_prime_3^2*c^2*c_x^2)
% LumpedParameters.gamma_prime_bar = 1*Factor1*LumpedParameters.gamma_prime_bar;     	% [nM]                      gamma_prime_bar = gamma_prime/a_prime
% LumpedParameters.kappa_prime_bar = 1*Factor1*LumpedParameters.kappa_prime_bar;     	% [nM]                      kappa_prime_bar = (d_prime/a_prime) * (gamma_prime/delta)
% LumpedParameters.k_prime_bar = 1*LumpedParameters.k_prime_bar;                        % [Dimensionless]           k_prime_bar = k_prime / delta_prime
% LumpedParameters.k_prime_bar_0 = 1*LumpedParameters.k_prime_bar_0;                    % [Dimensionless]           k_prime_bar_0 = k_prime_0 / k_1
% LumpedParameters.k_prime_bar_2 = 1*LumpedParameters.k_prime_bar_2;                    % [Dimensionless]           k_prime_bar_2 = k_prime_2 / k_1
LumpedParameters

%% Select Compare Function
N = 100;
% Compare_FullData_Simulation_Prediction_Normalized(Data.DataTable, LumpedParameters, N, 'linear');
Compare_FullData_Simulation_Prediction_Normalized_Mini(Data.DataTable, LumpedParameters, N, 'linear');

if Save_Flag == 1
%     print(gcf, 'Model_Fit_Network_Perturbation_Normalized','-dpdf');
    print(gcf, 'Model_Fit_Network_Perturbation_Normalized_Mini','-dpdf');
end
