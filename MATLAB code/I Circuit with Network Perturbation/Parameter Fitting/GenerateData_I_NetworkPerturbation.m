function [Input_Data, Measurement_Data, Measurement_SEM, ...
          Measurement1_Data, Measurement2_Data, Measurement3_Data] = GenerateData_I_NetworkPerturbation(DataTable)
% Input_Data = [G_1, G_2, G_3, D, Type] in [pmol, pmol, pmol nM, {0,1,2,3}]
%           Type:
%           0 : Open Loop Without Network Perturbation
%           1 : Closed Loop Without Network Perturbation
%           2 : Open Loop With Network Perturbation
%           3 : Closed Loop With Network Perturbation
% Measurement_Data = Green in [a.u.]
% The data generated are sorted as Type 0 then 1 then 2 then 3.
% Withing each type the data generated are sorted in increasing G_1/G_2 ratio and the Without Disturbance-With Disturbance

%% Extract Inputs and Measurements
N = size(DataTable,1) / 3;
Input_Data = zeros(N,5);
Measurement_Data = zeros(N,1);
Measurement_SEM = zeros(N,1);
Measurement1_Data = zeros(N,1);
Measurement2_Data = zeros(N,1);
Measurement3_Data = zeros(N,1);
for i = 1 : N
    Input_Data(i,1) = DataTable{3*(i-1)+1,5};                                       % G_1 [pmol]
    Input_Data(i,2) = DataTable{3*(i-1)+1,6};                                       % G_2 [pmol]
    Input_Data(i,3) = DataTable{3*(i-1)+1,7};                                       % G_3 [pmol]
    Input_Data(i,4) = DataTable{3*(i-1)+1,2} * 1e3;                                 % D [nM]
    if (DataTable{3*(i-1)+1,8} == 'Open' && (DataTable{3*(i-1)+1,9} == 'No' || DataTable{3*(i-1)+1,9} == 'no'))
        Input_Data(i,5) = 0;
    elseif (DataTable{3*(i-1)+1,8} == 'Open' && (DataTable{3*(i-1)+1,9} == 'Yes' || DataTable{3*(i-1)+1,9} == 'yes'))
        Input_Data(i,5) = 2;
    elseif (DataTable{3*(i-1)+1,8} == 'Closed' && (DataTable{3*(i-1)+1,9} == 'No' || DataTable{3*(i-1)+1,9} == 'no'))
        Input_Data(i,5) = 1;
    elseif (DataTable{3*(i-1)+1,8} == 'Closed' && (DataTable{3*(i-1)+1,9} == 'Yes' || DataTable{3*(i-1)+1,9} == 'yes'))
        Input_Data(i,5) = 3;
    else
        error('Only Open and Closed Loops with and without Network Perturbation are allowed.');
    end
    Measurement_Data(i) = mean(DataTable{3*(i-1)+1:3*i,4});                         % Green [a.u.]
    Measurement_SEM(i) = std(DataTable{3*(i-1)+1:3*i,4})/sqrt(3);                   % Green [a.u.]
    Measurement1_Data(i,1) = DataTable{3*(i-1)+1,4};                                % Green [a.u.]
    Measurement2_Data(i,1) = DataTable{3*(i-1)+2,4};                                % Green [a.u.]
    Measurement3_Data(i,1) = DataTable{3*(i-1)+3,4};                                % Green [a.u.]
end

%% Sort with and without Disturbance
% Open Loop No Network Perturbation
Indeces_OLNNP = find(Input_Data(:,5) == 0);
Input_Data_OLNNP = Input_Data(Indeces_OLNNP,:);
Measurement_Data_OLNNP = Measurement_Data(Indeces_OLNNP);
Measurement_SEM_OLNNP = Measurement_SEM(Indeces_OLNNP);
Measurement1_Data_OLNNP = Measurement1_Data(Indeces_OLNNP);
Measurement2_Data_OLNNP = Measurement2_Data(Indeces_OLNNP);
Measurement3_Data_OLNNP = Measurement3_Data(Indeces_OLNNP);
[Input_Data_OLNNP, Indeces_OLNNP] = sortrows(Input_Data_OLNNP, [1, 4]);
Measurement_Data_OLNNP = Measurement_Data_OLNNP(Indeces_OLNNP);
Measurement_SEM_OLNNP = Measurement_SEM_OLNNP(Indeces_OLNNP);
Measurement1_Data_OLNNP = Measurement1_Data_OLNNP(Indeces_OLNNP);
Measurement2_Data_OLNNP = Measurement2_Data_OLNNP(Indeces_OLNNP);
Measurement3_Data_OLNNP = Measurement3_Data_OLNNP(Indeces_OLNNP);

% Open Loop Network Perturbation
Indeces_OLNP = find(Input_Data(:,5) == 2);
Input_Data_OLNP = Input_Data(Indeces_OLNP,:);
Measurement_Data_OLNP = Measurement_Data(Indeces_OLNP);
Measurement_SEM_OLNP = Measurement_SEM(Indeces_OLNP);
Measurement1_Data_OLNP = Measurement1_Data(Indeces_OLNP);
Measurement2_Data_OLNP = Measurement2_Data(Indeces_OLNP);
Measurement3_Data_OLNP = Measurement3_Data(Indeces_OLNP);
[Input_Data_OLNP, Indeces_OLNP] = sortrows(Input_Data_OLNP, [1, 4]);
Measurement_Data_OLNP = Measurement_Data_OLNP(Indeces_OLNP);
Measurement_SEM_OLNP = Measurement_SEM_OLNP(Indeces_OLNP);
Measurement1_Data_OLNP = Measurement1_Data_OLNP(Indeces_OLNP);
Measurement2_Data_OLNP = Measurement2_Data_OLNP(Indeces_OLNP);
Measurement3_Data_OLNP = Measurement3_Data_OLNP(Indeces_OLNP);

% Closed Loop No Network Perturbation
Indeces_CLNNP = find(Input_Data(:,5) == 1);
Input_Data_CLNNP = Input_Data(Indeces_CLNNP,:);
Measurement_Data_CLNNP = Measurement_Data(Indeces_CLNNP);
Measurement_SEM_CLNNP = Measurement_SEM(Indeces_CLNNP);
Measurement1_Data_CLNNP = Measurement1_Data(Indeces_CLNNP);
Measurement2_Data_CLNNP = Measurement2_Data(Indeces_CLNNP);
Measurement3_Data_CLNNP = Measurement3_Data(Indeces_CLNNP);
[Input_Data_CLNNP, Indeces_CLNNP] = sortrows(Input_Data_CLNNP, [1, 4]);
Measurement_Data_CLNNP = Measurement_Data_CLNNP(Indeces_CLNNP);
Measurement_SEM_CLNNP = Measurement_SEM_CLNNP(Indeces_CLNNP);
Measurement1_Data_CLNNP = Measurement1_Data_CLNNP(Indeces_CLNNP);
Measurement2_Data_CLNNP = Measurement2_Data_CLNNP(Indeces_CLNNP);
Measurement3_Data_CLNNP = Measurement3_Data_CLNNP(Indeces_CLNNP);

% CLose Loop Network Perturbation
Indeces_CLNP = find(Input_Data(:,5) == 3);
Input_Data_CLNP = Input_Data(Indeces_CLNP,:);
Measurement_Data_CLNP = Measurement_Data(Indeces_CLNP);
Measurement_SEM_CLNP = Measurement_SEM(Indeces_CLNP);
Measurement1_Data_CLNP = Measurement1_Data(Indeces_CLNP);
Measurement2_Data_CLNP = Measurement2_Data(Indeces_CLNP);
Measurement3_Data_CLNP = Measurement3_Data(Indeces_CLNP);
[Input_Data_CLNP, Indeces_CLNP] = sortrows(Input_Data_CLNP, [1, 4]);
Measurement_Data_CLNP = Measurement_Data_CLNP(Indeces_CLNP);
Measurement_SEM_CLNP = Measurement_SEM_CLNP(Indeces_CLNP);
Measurement1_Data_CLNP = Measurement1_Data_CLNP(Indeces_CLNP);
Measurement2_Data_CLNP = Measurement2_Data_CLNP(Indeces_CLNP);
Measurement3_Data_CLNP = Measurement3_Data_CLNP(Indeces_CLNP);

% Concatenate
Input_Data = [Input_Data_OLNNP; Input_Data_OLNP; Input_Data_CLNNP; Input_Data_CLNP];
Measurement_Data = [Measurement_Data_OLNNP; Measurement_Data_OLNP; Measurement_Data_CLNNP; Measurement_Data_CLNP];
Measurement_SEM = [Measurement_SEM_OLNNP; Measurement_SEM_OLNP; Measurement_SEM_CLNNP; Measurement_SEM_CLNP];
Measurement1_Data = [Measurement1_Data_OLNNP; Measurement1_Data_OLNP; Measurement1_Data_CLNNP; Measurement1_Data_CLNP];
Measurement2_Data = [Measurement2_Data_OLNNP; Measurement2_Data_OLNP; Measurement2_Data_CLNNP; Measurement2_Data_CLNP];
Measurement3_Data = [Measurement3_Data_OLNNP; Measurement3_Data_OLNP; Measurement3_Data_CLNNP; Measurement3_Data_CLNP];

end