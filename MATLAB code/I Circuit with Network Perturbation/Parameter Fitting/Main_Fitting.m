%% Clear Workspace
close all;
clear;
clc;
Save_Flag = 0;

%% Select Optimization Settings and Cost Function and Specify FileName
DataFile = 'FullData_I_Perturbed1';
% FileName = 'Results_NP_ND';
% CostFunction = @CostFunction_NP_ND;
% [Input_Data, Measurement_Data, Measurement_SEM, ~,  LumpedParameters_LB,  LumpedParameters_UB, A, B, Aeq, Beq, Options] = OptimizationSettings_NP_ND(DataFile);

% FileName = 'Results_NP_D';
% CostFunction = @CostFunction_NP_D;
% [Input_Data, Measurement_Data, Measurement_SEM, ~,  LumpedParameters_LB,  LumpedParameters_UB, A, B, Aeq, Beq, Options] = OptimizationSettings_NP_D(DataFile);

FileName = 'Results_P_ND';
CostFunction = @CostFunction_P_ND;
[Input_Data, Measurement_Data, Measurement_SEM, ~,  LumpedParameters_LB,  LumpedParameters_UB, A, B, Aeq, Beq, Options] = OptimizationSettings_P_ND(DataFile);

%% Generate Random Initial Guesses
N_IG = 10;
LumpedParameters_IG = Generate_RandomIG(LumpedParameters_LB, LumpedParameters_UB, N_IG);

%% Optimizing 
tic;
J = zeros(N_IG,1);
LumpedParameters_Optimal(1:N_IG) = Class_LumpedParameters();
for i = 1 : N_IG
    i
    [LumpedParameters_Optimal(i), J(i)] = Optimization(Input_Data, Measurement_Data, ...
                                                       CostFunction, ...
                                                       LumpedParameters_IG(i), LumpedParameters_LB, LumpedParameters_UB, ...
                                                  	   A, B, Aeq, Beq, Options);
end
SimTime = toc;

%% Compare Data and Model
[J_Sorted, Indeces] = sort(J, 'descend');
figure();
plot(J_Sorted);
LumpedParameters = LumpedParameters_Optimal(Indeces(end));

%% Save Parameters
clearvars -except LumpedParameters_Optimal J ...
                  LumpedParameters_LB LumpedParameters_UB ...
                  LumpedParameters LumpedParameters_IG ...
                  FileName;
save(FileName);


