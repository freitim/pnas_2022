function [LumpedParameters_LB, LumpedParameters_UB] = Generate_UBLB_NP(LumpedParameters_IG, Fraction_LB, Fraction_UB)
%% Lower Bounds for Steady State Parameters
LumpedParameters_LB = Class_LumpedParameters();
LumpedParameters_LB.gamma_bar = LumpedParameters_IG.gamma_bar * (1 - Fraction_LB);       	% [1/a.u.]                  gamma_bar = (gamma/k) * ((c+gamma)/c) / c_x
LumpedParameters_LB.delta_bar  = LumpedParameters_IG.delta_bar * (1 - Fraction_LB);      	% [dimensionless]           delta_bar = delta / k_1
LumpedParameters_LB.eta_bar = LumpedParameters_IG.eta_bar * (1 - Fraction_LB);            	% [1/nM]                    eta_bar = eta / k_1
LumpedParameters_LB.kappa_bar = LumpedParameters_IG.kappa_bar * (1 - Fraction_LB);       	% [a.u.]                    kappa_bar = c_x * kappa
LumpedParameters_LB.kappa_bar_2 = LumpedParameters_IG.kappa_bar_2 * (1 - Fraction_LB);   	% [a.u.]                    kappa_bar_2 = c_x * sqrt(kappa * kappa_2)
LumpedParameters_LB.k_bar_0 = LumpedParameters_IG.k_bar_0;                                  % [Dimensionless]           k_bar_0 = k_0 / k_1
LumpedParameters_LB.k_bar_2 = LumpedParameters_IG.k_bar_2 * (1 - Fraction_LB);              % [Dimensionless]           k_bar_2 = k_2 / k_1
LumpedParameters_LB.Delta_1 = LumpedParameters_IG.Delta_1;                                  % [1/nM]                    Delta_1 = k_3 / (kappa_3*c_bar)
LumpedParameters_LB.Delta_2 = LumpedParameters_IG.Delta_2 * (1 - Fraction_LB);             	% [1/a.u./nM]            	Delta_2 = (k_prime_3*gamma) / (kappa_prime_3^2*c_bar*c*c_x)
LumpedParameters_LB.Delta_prime_1 = LumpedParameters_IG.Delta_prime_1;                      % [1/a.u.]               	Delta_prime_1 = gamma / (kappa_3*c*c_x)
LumpedParameters_LB.Delta_prime_2 = LumpedParameters_IG.Delta_prime_2;                      % [1/a.u.^2]               	Delta_prime_2 = gamma^2 / (kappa_prime_3^2*c^2*c_x^2)
LumpedParameters_LB.gamma_prime_bar = LumpedParameters_IG.gamma_prime_bar;                  % [nM]                      gamma_prime_bar = gamma_prime/a_prime
LumpedParameters_LB.kappa_prime_bar = LumpedParameters_IG.kappa_prime_bar;                  % [nM]                      kappa_prime_bar = (d_prime/a_prime) * (gamma_prime/delta)
LumpedParameters_LB.k_prime_bar = LumpedParameters_IG.k_prime_bar;                          % [Dimensionless]           k_prime_bar = k_prime / delta_prime
LumpedParameters_LB.k_prime_bar_0 = LumpedParameters_IG.k_prime_bar_0;                      % [Dimensionless]           k_prime_bar_0 = k_prime_0 / k_1
LumpedParameters_LB.k_prime_bar_2 = LumpedParameters_IG.k_prime_bar_2;                      % [Dimensionless]           k_prime_bar_2 = k_prime_2 / k_1

%% Upper Bounds for Steady State Parameters
LumpedParameters_UB = Class_LumpedParameters();
LumpedParameters_UB.gamma_bar = LumpedParameters_IG.gamma_bar * (1 + Fraction_UB);       	% [1/a.u.]                  gamma_bar = (gamma/k) * ((c+gamma)/c) / c_x
LumpedParameters_UB.delta_bar  = LumpedParameters_IG.delta_bar * (1 + Fraction_UB);       	% [dimensionless]           delta_bar = delta / k_1
LumpedParameters_UB.eta_bar = LumpedParameters_IG.eta_bar * (1 + Fraction_UB);            	% [1/nM]                    eta_bar = eta / k_1
LumpedParameters_UB.kappa_bar = LumpedParameters_IG.kappa_bar * (1 + Fraction_UB);        	% [a.u.]                    kappa_bar = c_x * kappa
LumpedParameters_UB.kappa_bar_2 = LumpedParameters_IG.kappa_bar_2 * (1 + Fraction_UB);   	% [a.u.]                    kappa_bar_2 = c_x * sqrt(kappa * kappa_2)
LumpedParameters_UB.k_bar_0 = LumpedParameters_IG.k_bar_0;                                  % [Dimensionless]           k_bar_0 = k_0 / k_1
LumpedParameters_UB.k_bar_2 = LumpedParameters_IG.k_bar_2 * (1 + Fraction_UB);          	% [Dimensionless]           k_bar_2 = k_2 / k_1
LumpedParameters_UB.Delta_1 = LumpedParameters_IG.Delta_1;                                  % [1/nM]                    Delta_1 = k_3 / (kappa_3*c_bar)
LumpedParameters_UB.Delta_2 = LumpedParameters_IG.Delta_2 * (1 + Fraction_UB);             	% [1/a.u./nM]            	Delta_2 = (k_prime_3*gamma) / (kappa_prime_3^2*c_bar*c*c_x)
LumpedParameters_UB.Delta_prime_1 = LumpedParameters_IG.Delta_prime_1;                      % [1/a.u.]               	Delta_prime_1 = gamma / (kappa_3*c*c_x)
LumpedParameters_UB.Delta_prime_2 = LumpedParameters_IG.Delta_prime_2;                      % [1/a.u.^2]               	Delta_prime_2 = gamma^2 / (kappa_prime_3^2*c^2*c_x^2)
LumpedParameters_UB.gamma_prime_bar = LumpedParameters_IG.gamma_prime_bar;                  % [nM]                      gamma_prime_bar = gamma_prime/a_prime
LumpedParameters_UB.kappa_prime_bar = LumpedParameters_IG.kappa_prime_bar;                  % [nM]                      kappa_prime_bar = (d_prime/a_prime) * (gamma_prime/delta)
LumpedParameters_UB.k_prime_bar = LumpedParameters_IG.k_prime_bar;                          % [Dimensionless]           k_prime_bar = k_prime / delta_prime
LumpedParameters_UB.k_prime_bar_0 = LumpedParameters_IG.k_prime_bar_0;                      % [Dimensionless]           k_prime_bar_0 = k_prime_0 / k_1
LumpedParameters_UB.k_prime_bar_2 = LumpedParameters_IG.k_prime_bar_2;                      % [Dimensionless]           k_prime_bar_2 = k_prime_2 / k_1

end

