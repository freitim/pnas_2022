function Boundary_Status = LumpedParameters_Boundary(LumpedParameters, LumpedParameters_LB, LumpedParameters_UB)
LB = Vector(LumpedParameters_LB);
UB = Vector(LumpedParameters_UB);
Parameters = Vector(LumpedParameters);
Status = (Parameters == UB) - (Parameters == LB);
Boundary_Status = Class_LumpedParameters(Status);
end

