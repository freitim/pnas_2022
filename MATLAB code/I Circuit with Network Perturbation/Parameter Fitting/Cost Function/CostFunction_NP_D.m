function J = CostFunction_NP_D(LumpedParameters, Input_Data, Measurement_Data)
%% Compute Fixed Points
[Measurement_Green, ~, ~, ~, ~, ~, ~, ~] = LumpedFixedPoint_FiniteEta(Input_Data(:,1:4), LumpedParameters, Input_Data(:,5));

%% Compute Error
Error = abs(Measurement_Green - Measurement_Data(:,1));

%% Optimization Weights
Weights = ones(size(Input_Data,1), 1);
Weights(Input_Data(:,5) == 0 & Input_Data(:,4) == 0) = 0;
Weights(Input_Data(:,5) == 0 & Input_Data(:,4) > 0) = 0.5;

Weights(Input_Data(:,5) == 1 & Input_Data(:,4) == 0) = 0;
Weights(Input_Data(:,5) == 1 & Input_Data(:,4) > 0) = 0.5;

Weights(Input_Data(:,5) == 2 & Input_Data(:,4) == 0) = 0;
Weights(Input_Data(:,5) == 2 & Input_Data(:,4) > 0) = 0;

Weights(Input_Data(:,5) == 3 & Input_Data(:,4) == 0) = 0;
Weights(Input_Data(:,5) == 3 & Input_Data(:,4) > 0) = 0;

%% Compute Cost Function
J = norm(Weights.*Error, 2);
end

