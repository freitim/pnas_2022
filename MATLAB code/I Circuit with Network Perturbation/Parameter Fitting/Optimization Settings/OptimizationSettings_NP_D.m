function [Input_Data, Measurement_Data, Measurement_SEM, ...
          LumpedParameters_IG,  LumpedParameters_LB,  LumpedParameters_UB,  ...
          A, B, Aeq, Beq, Options] = OptimizationSettings_NP_D(DataFile)
      
%% Generate Data
Data = load(DataFile);
[Input_Data, Measurement_Data, Measurement_SEM, ~, ~, ~] = GenerateData_I_NetworkPerturbation(Data.DataTable);
clear Data;

%% Initial Guess for Lumped Parameters
Results_NP_ND = load('Results_NP_ND');
LumpedParameters_IG = Results_NP_ND.LumpedParameters;

Fraction_LB = 0.99;
Fraction_UB = 100;
[LumpedParameters_LB, LumpedParameters_UB] = Generate_UBLB_NP_D(LumpedParameters_IG, Fraction_LB, Fraction_UB);

%% Linear Constraints
A = []; B = []; Aeq = []; Beq = [];

%% Solver Options
Options = optimoptions(@fmincon, 'Display','iter', 'Algorithm', 'sqp', 'TolFun', 1e-14, 'MaxFunEvals', 1e4, 'TolCon',1e-14, 'TolX',1e-14, 'MaxIter', 1e4);
% Options = optimoptions(@fmincon, 'Display','iter', 'Algorithm', 'sqp', 'MaxFunEvals', 1000);

end

