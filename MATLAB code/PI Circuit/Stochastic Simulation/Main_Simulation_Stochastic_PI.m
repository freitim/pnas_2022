%% Clear Workspace
close all;
clear;
clc;
Save_Flag = 1;

%% Inputs & Disturbances
G_1 = 0.002;                                                    % [pmol]
G_2 = 0.004;                                                    % [pmol]
D = 0;                                                          % [nM]
        
%% Biological Parameters
load LumpedParameters;
k = 1;
c = 1;
d = 1;
c_x = 1e3;
Ratio_gamma_gamma_prime = 1;
BioParameters = Lumped2Bio(LumpedParameters, k, c, d, c_x, Ratio_gamma_gamma_prime);
Factor = 100;
BioParameters.a_prime = BioParameters.a_prime/Factor;
BioParameters.eta = BioParameters.eta/Factor;
BioParameters.a = BioParameters.a/Factor;
BioParameters.c_x = BioParameters.c_x/Factor;
BioParameters.kappa_2 = BioParameters.kappa_2*Factor;
BioParameters.k_1 = BioParameters.k_1 * Factor;
BioParameters.k_2 = BioParameters.k_2 * Factor;

%% Simulation Settings
tf = 48*60;                                                   % [min]
N = 1000;
IC = [0; 0; 0; 0; 0; 0; 0];
N_Trajectories_Saved = 64;
N_Trajectories = 32;
Iterations = 360;
N_MaxRxnEvents = 1e7;

%% Simulation
NetworkParameters = struct(BioParameters);
NetworkParameters.G_1 = G_1;
NetworkParameters.G_2 = G_2;
NetworkParameters.D = D;
TStart = tic;
MeanM_Iteration = zeros(Iterations,N-1);
VarM_Iteration = zeros(Iterations,N-1);
FinalDistribution_Iteration = zeros(N_Trajectories,Iterations);
for iter = 1 : Iterations
    iter
    T = cell(N_Trajectories,1);
    X = cell(N_Trajectories,1);
    M = cell(N_Trajectories,1);
    parfor i = 1 : N_Trajectories
        [T{i}, X{i}] = SSA(@PropensityFunction_Stochastic, StoichiometryMatrix_Stochastic, NetworkParameters, IC, tf, N_MaxRxnEvents);
        M{i} = (X{i}(1,:) + X{i}(2,:) + 2*X{i}(3,:));
    end
    [time_vector, MeanM_Iteration(iter,:), VarM_Iteration(iter,:), FinalDistribution_Iteration(:,iter), ~, ~] = ComputeStatistics(T, M, N, 1);
    clear T X M
end
SimTime = toc(TStart)

%% Compute Statistics
MeanM = mean(MeanM_Iteration, 1);
VarM = mean(VarM_Iteration,1) + mean(MeanM_Iteration.^2,1) - MeanM.^2;
FinalDistributionM = FinalDistribution_Iteration(:);

%% Saved Trajectories
T = cell(N_Trajectories_Saved,1);
X = cell(N_Trajectories_Saved,1);
M = cell(N_Trajectories_Saved,1);
parfor i = 1 : N_Trajectories
 	[T{i}, X{i}] = SSA(@PropensityFunction_Stochastic, StoichiometryMatrix_Stochastic, NetworkParameters, IC, tf, N_MaxRxnEvents);
	M{i} = (X{i}(1,:) + X{i}(2,:) + 2*X{i}(3,:));
end

% %% General Figure Settings 
% Colors = lines(10);
% IColor = Colors(1,:); PIColor =[255, 140, 0] / 255;
% 
% SS = 4; % Screen Scale
% Figure_Width = 5 * SS;
% Figure_Height = 3.5 * SS;
% FontSize = 7 * SS;
% LineWidth = 1 * SS;
% LineWidth_Thin = 0.5 * SS;
% LineWidth_Marker = LineWidth/5;
% MarkerSize = 1.5 * SS;
% MarkerSize_Big = 3*MarkerSize;
% MarkerSize_Small = 2*MarkerSize;
% Transparency = 0.2;
% 
% %% Figure 1
% Figure1_Name = 'Trajectories';
% Handle_Figure1 = figure();
%     Handle_Figure1.PaperUnits = 'centimeters';
%     Handle_Figure1.Units = 'centimeters';
%     Handle_Figure1.Position = [1, 20, Figure_Width, Figure_Height];
%     Handle_Figure1.PaperPositionMode = 'auto';
%     Handle_Figure1.PaperSize = [Handle_Figure1.PaperPosition(3), Handle_Figure1.PaperPosition(4)];
%     
% %% Figure 2
% Figure2_Name = 'MeanVariance';
% Handle_Figure2 = figure();
%     Handle_Figure2.PaperUnits = 'centimeters';
%     Handle_Figure2.Units = 'centimeters';
%     Handle_Figure2.Position = [1+Figure_Width, 20, Figure_Width, Figure_Height];
%     Handle_Figure2.PaperPositionMode = 'auto';
%     Handle_Figure2.PaperSize = [Handle_Figure2.PaperPosition(3), Handle_Figure2.PaperPosition(4)];
%     
% %% Figure 3
% Figure3_Name = 'Distribution';
% Handle_Figure3 = figure();
%     Handle_Figure3.PaperUnits = 'centimeters';
%     Handle_Figure3.Units = 'centimeters';
%     Handle_Figure3.Position = [1+2*Figure_Width, 20, Figure_Width, Figure_Height];
%     Handle_Figure3.PaperPositionMode = 'auto';
%     Handle_Figure3.PaperSize = [Handle_Figure3.PaperPosition(3), Handle_Figure3.PaperPosition(4)];
%     
% %% Axis for Stochastic Trajectories
% Handle_Axis1 = axes(Handle_Figure1);
%     Handle_Axis1.Box = 'on';
%     Handle_Axis1.FontSize = FontSize;
%     hold(Handle_Axis1, 'on');
%     grid(Handle_Axis1, 'on');
%     Handle_Axis1.XMinorGrid = 'off';
%     Handle_Axis1.YMinorGrid = 'off';
%     Handle_Axis1.XLabel.String = '$t$';
%     Handle_Axis1.YLabel.String = '$M(t)$';
%     Handle_Axis1.XLabel.Interpreter = 'latex';
%     Handle_Axis1.YLabel.Interpreter = 'latex';
% for i = 1 : N_Trajectories_Saved
%     plot(Handle_Axis1, T{i}/60, M{i}, 'LineWidth', LineWidth, 'Color', [IColor, Transparency]);
% end
% 
% %% Axis for the Means
% Handle_Axis2 = axes(Handle_Figure2);
%     Handle_Axis2.Box = 'on';
%     Handle_Axis2.FontSize = FontSize;
%     hold(Handle_Axis2, 'on');
%     grid(Handle_Axis2, 'on');
%     Handle_Axis2.XMinorGrid = 'off';
%     Handle_Axis2.YMinorGrid = 'off';
%     Handle_Axis2.XLabel.String = '$t$';
%     Handle_Axis2.YLabel.String = 'Mean';
%     Handle_Axis2.XLabel.Interpreter = 'latex';
% plot(Handle_Axis2, time_vector/60, MeanM, 'LineWidth', LineWidth_Thin, 'Color', IColor, 'LineStyle', '--');
% plot(Handle_Axis2, time_vector/60, sqrt(VarM), 'LineWidth', LineWidth, 'Color', IColor);
% 
% %% Axis for the Variance
% Handle_Axis3 = axes(Handle_Figure3);
%     Handle_Axis3.Box = 'on';
%     Handle_Axis3.FontSize = FontSize;
%     hold(Handle_Axis3, 'on');
%     grid(Handle_Axis3, 'on');
%     Handle_Axis3.XMinorGrid = 'off';
%     Handle_Axis3.YMinorGrid = 'off';
%     Handle_Axis3.XLabel.String = '$t$';
%     Handle_Axis3.YLabel.String = 'Variance';
%     Handle_Axis3.XLabel.Interpreter = 'latex';
% histogram(Handle_Axis3, FinalDistributionM, 'BinWidth', 100, 'FaceColor', IColor);

%% Saving
if Save_Flag == 1
    save('StochasticSimulation_PI1');
end
