%% Clear Workspace
close all
clear
clc;
Save_Flag = 0;

%% Load Simulations
I = load('StochasticSimulation_I1');
PI = load('StochasticSimulation_PI1');
pd_I = fitdist(I.FinalDistributionM, 'Normal');
pd_PI = fitdist(PI.FinalDistributionM, 'Normal');
Normalization_Mean = I.MeanM(end);
Normalization_CV2 = I.VarM(end) ./ I.MeanM(end)^2;

%% General Figure Settings 
Colors = lines(10);
IColor = Colors(1,:); 
PIColor =[255, 140, 0] / 255;

SS = 4; % Screen Scale
Figure_Width = 12 * SS;
Figure_Height = 2.65 * SS;
FontSize = 7 * SS;
FontSize_Small = 4 * SS;
LineWidth = 1 * SS;
LineWidth_Thin = 0.5 * SS;
LineWidth_Marker = LineWidth/5;
MarkerSize = 1.5 * SS;
MarkerSize_Big = 3*MarkerSize;
MarkerSize_Small = 2*MarkerSize;
Transparency = 0.2;

%% Figure 1
Figure1_Name = 'Stochastic Response';
Handle_Figure1 = figure();
    Handle_Figure1.PaperUnits = 'centimeters';
    Handle_Figure1.Units = 'centimeters';
    Handle_Figure1.Position = [1, 20, Figure_Width, Figure_Height];
    Handle_Figure1.PaperPositionMode = 'auto';
    Handle_Figure1.PaperSize = [Handle_Figure1.PaperPosition(3), Handle_Figure1.PaperPosition(4)];
    
%% Axis for Stochastic Trajectories
Handle_Axis1 = axes(Handle_Figure1);
    Handle_Axis1.Position = [0.09, 0.13, 0.4, 0.72];
    Handle_Axis1.Box = 'on';
    Handle_Axis1.FontSize = FontSize;
    hold(Handle_Axis1, 'on');
    grid(Handle_Axis1, 'on');
    Handle_Axis1.XMinorGrid = 'off';
    Handle_Axis1.YMinorGrid = 'off';
    Handle_Axis1.XLabel.String = 'Time, hrs';
    Handle_Axis1.YLabel.String = {'Normalized'; 'Fluorescence'};
    Handle_Axis1.Title.String = 'Single Cell Trajectories & Population Mean';
    Handle_Axis1.YLim = [0, 1.8];
    Handle_Axis1.YTick = [0, 0.5, 1, 1.5];
    Handle_Axis1.XLim = [0, I.time_vector(end)/60];
    Handle_Axis1.XLabel.VerticalAlignment = 'bottom';
    Handle_Axis1.XLabel.Position(2) = 0;
for i = 1 : I.N_Trajectories_Saved
    if max(I.M{i}/Normalization_Mean) >= 1.3
        plot(Handle_Axis1, I.T{i}/60, I.M{i}/Normalization_Mean, 'LineWidth', LineWidth_Thin, 'Color', [IColor, Transparency]);
    end
end
for i = 1 : PI.N_Trajectories_Saved
    if max(PI.M{i}/Normalization_Mean) <= 1.3
        plot(Handle_Axis1, PI.T{i}/60, PI.M{i}/Normalization_Mean, 'LineWidth', LineWidth_Thin, 'Color', [PIColor, Transparency]);
    end
end

plot(Handle_Axis1, I.time_vector/60, I.MeanM/Normalization_Mean, 'LineWidth', LineWidth, 'Color', IColor);
plot(Handle_Axis1, PI.time_vector/60, PI.MeanM/Normalization_Mean, 'LineWidth', LineWidth, 'Color', PIColor);

%% Axis for the Means
% Handle_Axis2 = axes(Handle_Figure1);
%     Handle_Axis2.Position = [0.34, 0.22, 0.27, 0.68]; 
%     Handle_Axis2.Box = 'on';
%     Handle_Axis2.FontSize = FontSize;
%     hold(Handle_Axis2, 'on');
%     grid(Handle_Axis2, 'on');
%     Handle_Axis2.XMinorGrid = 'off';
%     Handle_Axis2.YMinorGrid = 'off';
%     Handle_Axis2.XLabel.String = 'Time, hrs';
%     Handle_Axis2.Title.String = 'Population Mean';
%     Handle_Axis2.YLim = Handle_Axis1.YLim;
%     Handle_Axis2.YTick = Handle_Axis1.YTick;
%     Handle_Axis2.YTickLabel = [];
% plot(Handle_Axis2, I.time_vector/60, I.MeanM/Normalization_Mean, 'LineWidth', LineWidth, 'Color', IColor);
% plot(Handle_Axis2, PI.time_vector/60, PI.MeanM/Normalization_Mean, 'LineWidth', LineWidth, 'Color', PIColor);

%% Axis for the CV2
Handle_Axis3 = axes(Handle_Figure1);
    Handle_Axis3.Position = [0.53, 0.13, 0.4, 0.72];
    Handle_Axis3.Box = 'on';
    Handle_Axis3.FontSize = FontSize;
    hold(Handle_Axis3, 'on');
    grid(Handle_Axis3, 'on');
    Handle_Axis3.XMinorGrid = 'off';
    Handle_Axis3.YMinorGrid = 'off';
    Handle_Axis3.XLabel.String = 'Time, hrs';
    Handle_Axis3.Title.String = 'Normalized CV^2';
    Handle_Axis3.YTick = 0 : 0.2 : 1;
    Handle_Axis3.XLim = [0, I.time_vector(end)/60];
    Handle_Axis3.XLabel.VerticalAlignment = 'bottom';
    Handle_Axis3.XLabel.Position(2) = 0;
plot(Handle_Axis3, I.time_vector/60, I.VarM ./ I.MeanM(end)^2 / Normalization_CV2, 'LineWidth', LineWidth, 'Color', IColor);
plot(Handle_Axis3, PI.time_vector/60, PI.VarM ./ PI.MeanM(end)^2 / Normalization_CV2, 'LineWidth', LineWidth, 'Color', PIColor);
% histogram(Handle_Axis3, I.FinalDistributionM, 'BinWidth', 100, 'FaceColor', IColor);
% histogram(Handle_Axis3, PI.FinalDistributionM, 'BinWidth', 100, 'FaceColor', PIColor);
Handle_Legend = legend(Handle_Axis3, 'I-Control', 'PI-Control', 'Location', 'SouthEast');

%% Save
if Save_Flag == 1
    Handle_Figure1.Color = 'none';
    set(Handle_Figure1, 'InvertHardCopy', 'off');
    print(Handle_Figure1, Figure1_Name, '-dpdf', '-painters');
end

