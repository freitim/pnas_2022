function [] = Visualize_Data_PI(DataTable)
%% Extract Inputs and Measurements
[Input_Data, Measurement_Data, ~, Measurement1_Data, Measurement2_Data, Measurement3_Data] = GenerateData_PI(DataTable);
Ratios = unique(Input_Data(:,1) ./ Input_Data(:,2));
Fluorescence_Max = 1.1 * max(max([Measurement1_Data, Measurement2_Data, Measurement3_Data]));
%% General Figure Settings 
Colors = lines(10);
MyGreen = Colors(5,:); MyRed = Colors(2,:);

SS = 4; % Screen Scale
Figure_Width = 11 * SS;
Figure_Height = 7 * SS;
FontSize = 7 * SS;
LineWidth_Thin = 0.01 * SS;
MarkerSize = 1.5 * SS;
BarWidth_Big = 0.1;
BarWidth_Small = 0.1 / 1.25;
Transparency = 0.5;
DotSpace = 0.03;

%% Figure 1 Settings
Handle_Figure1 = figure();
    Handle_Figure1.Color = [1 1 1];
    Handle_Figure1.PaperUnits = 'centimeters';
    Handle_Figure1.Units = 'centimeters';
    Handle_Figure1.Position = [1, 20, Figure_Width, Figure_Height];
    Handle_Figure1.PaperPositionMode = 'auto';
    Handle_Figure1.PaperSize = [Handle_Figure1.PaperPosition(3), Handle_Figure1.PaperPosition(4)];
figure(Handle_Figure1);

Handle_Axis1 = subplot(2,2,1);
    Handle_Axis1.Position = [0.06, 0.52, 0.45, 0.37];
    hold(Handle_Axis1, 'on');
    Handle_Axis1.Box = 'on';
    Handle_Axis1.FontSize = FontSize;
    grid(Handle_Axis1, 'on');
    Handle_Axis1.XMinorGrid = 'off';
    Handle_Axis1.YMinorGrid = 'off';
    Handle_Axis1.XLabel.String = '';
    Handle_Axis1.YLabel.String = {'Fluorescence [a.u.]'};
    Handle_Axis1.YLabel.Position = [0.08, 0];
    Handle_Axis1.Title.String = 'Open Loop';
    Handle_Axis1.XLim = [min(Ratios) - 0.1, max(Ratios) + 0.1];
    Handle_Axis1.XTick = Ratios;
    Handle_Axis1.XTickLabel = {};
	Handle_Axis1.YLim = [0, Fluorescence_Max];
    
Handle_Axis2 = subplot(2,2,2);
    Handle_Axis2.Position = [0.53, 0.52, 0.45, 0.37];
    hold(Handle_Axis2, 'on');
    Handle_Axis2.Box = 'on';
    Handle_Axis2.FontSize = FontSize;
    grid(Handle_Axis2, 'on');
    Handle_Axis2.XMinorGrid = 'off';
    Handle_Axis2.YMinorGrid = 'off';
    Handle_Axis2.XLabel.String = '';
    Handle_Axis2.Title.String = 'I-Control';
    Handle_Axis2.XLim = [min(Ratios) - 0.1, max(Ratios) + 0.1];
    Handle_Axis2.XTick = Ratios;
    Handle_Axis2.XTickLabel = {};
	Handle_Axis2.YLim = [0, Fluorescence_Max];
    Handle_Axis2.YTickLabel = {};

Handle_Axis3 = subplot(2,2,3);
    Handle_Axis3.Position = [0.06, 0.11, 0.45, 0.37];
    hold(Handle_Axis3, 'on');
    Handle_Axis3.Box = 'on';
    Handle_Axis3.FontSize = FontSize;
    grid(Handle_Axis3, 'on');
    Handle_Axis3.XMinorGrid = 'off';
    Handle_Axis3.YMinorGrid = 'off';
    Handle_Axis3.XLabel.String = '$G_1/G_2$';
    Handle_Axis3.XLabel.Interpreter = 'latex';
    Handle_Axis3.Title.String = 'P-Control';
    Handle_Axis3.XLim = [min(Ratios) - 0.1, max(Ratios) + 0.1];
    Handle_Axis3.XTick = Ratios;
    Handle_Axis3.XTickLabel = cellstr(strtrim(rats(Ratios)));
	Handle_Axis3.YLim = [0, Fluorescence_Max];
    
Handle_Axis4 = subplot(2,2,4);
    Handle_Axis4.Position = [0.53, 0.11, 0.45, 0.37];
    hold(Handle_Axis4, 'on');
    Handle_Axis4.Box = 'on';
    Handle_Axis4.FontSize = FontSize;
    grid(Handle_Axis4, 'on');
    Handle_Axis4.XMinorGrid = 'off';
    Handle_Axis4.YMinorGrid = 'off';
    Handle_Axis4.XLabel.String = '$G_1/G_2$';
    Handle_Axis4.XLabel.Interpreter = 'latex';
    Handle_Axis4.Title.String = 'PI-Control';
    Handle_Axis4.XLim = [min(Ratios) - 0.1, max(Ratios) + 0.1];
    Handle_Axis4.XTick = Ratios;
    Handle_Axis4.XTickLabel = cellstr(strtrim(rats(Ratios)));
	Handle_Axis4.YLim = [0, Fluorescence_Max];
    Handle_Axis4.YTickLabel = {};
    
%% Plotting Figure 1
for i = 1 : size(Input_Data,1)
    % Choose Axis
    switch Input_Data(i,4)
        case 0
            Handle_Axis = Handle_Axis1;
        case 1
            Handle_Axis = Handle_Axis2;
        case 2
            Handle_Axis = Handle_Axis3;
        case 3
            Handle_Axis = Handle_Axis4;
    end 
    % Choose Disturbance Scenario
    if Input_Data(i,3) == 0
        BarColor = MyGreen;
        BarWidth = BarWidth_Big;
    else
        BarColor = MyRed;
        BarWidth = BarWidth_Small;
    end
    % Bars
    Handle_Bar = bar(Handle_Axis, Input_Data(i,1) ./ Input_Data(i,2), Measurement_Data(i), BarWidth);
        Handle_Bar.FaceColor = BarColor;
        Handle_Bar.FaceAlpha = Transparency;
    % Data Points
    plot(Handle_Axis, Input_Data(i,1) ./ Input_Data(i,2) - DotSpace, Measurement1_Data(i), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', BarColor, 'Color', 'k');
    plot(Handle_Axis, Input_Data(i,1) ./ Input_Data(i,2), Measurement2_Data(i), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', BarColor, 'Color', 'k');
    plot(Handle_Axis, Input_Data(i,1) ./ Input_Data(i,2) + DotSpace, Measurement3_Data(i), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', BarColor, 'Color', 'k');
end

% -------
% Legend
% -------
Handle_White = plot(Handle_Axis2, -1, -1, 'Color', [1, 1, 1]);
Handle_DataPoint = plot(Handle_Axis2, -1, -1, 'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', [0.5, 0.5, 0.5], 'Color', 'k'); 
Handle_Bar = bar(Handle_Axis2, -1, -1, 10);
    Handle_Bar.FaceColor = [0.5, 0.5, 0.5];
    Handle_Bar.FaceAlpha = Transparency;
Handle_Legend1 = legend(Handle_Axis2, [Handle_DataPoint, Handle_White, Handle_Bar, Handle_White], 'Measurement', '', 'Average', '');
    Handle_Legend1.Orientation = 'Horizontal';
    Handle_Legend1.EdgeColor = 'none';
    Handle_Legend1.Position = [0.065, 0.945, 0.45, 0.05];
    Handle_Legend1.Color = 'none';
Handle_Box = annotation('rectangle', [Handle_Legend1.Position(1) + Handle_Legend1.Position(3) + 0.01, Handle_Legend1.Position(2), Handle_Legend1.Position(3:4) + [0.02, 0]]);
Handle_Color1 = annotation('rectangle');
    Handle_Color1.Position = [Handle_Box.Position(1), Handle_Box.Position(2)+0.01, 0.01*2.5, 2*0.015];
    Handle_Color1.EdgeColor = 'none';
    Handle_Color1.FaceColor = MyGreen;
Handle_Text1 = annotation('textbox');
    Handle_Text1.Position = Handle_Color1.Position + [0.025, 0, 0.2, 0];
    Handle_Text1.EdgeColor = 'none';
    Handle_Text1.String = 'Without Disturbance';
    Handle_Text1.FontSize = Handle_Legend1.FontSize;
    Handle_Text1.FontName = Handle_Legend1.FontName;
    Handle_Text1.VerticalAlignment = 'Middle';
Handle_Color3 = annotation('rectangle');
    Handle_Color3.Position = [Handle_Text1.Position(1) + Handle_Text1.Position(3), Handle_Color1.Position(2:4)];
    Handle_Color3.EdgeColor = 'none';
    Handle_Color3.FaceColor = MyRed;
Handle_Text2 = annotation('textbox');
    Handle_Text2.Position = Handle_Color3.Position + [0.025, 0, 0.2, 0];
    Handle_Text2.EdgeColor = 'none';
    Handle_Text2.String = 'With Disturbance';
    Handle_Text2.FontSize = Handle_Legend1.FontSize;
    Handle_Text2.FontName = Handle_Legend1.FontName;
    Handle_Text2.VerticalAlignment = 'Middle';
Handle_Box.Position = [0.06, 0.94, 0.92, 0.06];

end

