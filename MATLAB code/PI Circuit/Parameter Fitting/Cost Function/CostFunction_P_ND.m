function J = CostFunction_P_ND(LumpedParameters, Input_Data, Measurement_Data)
%% Compute Fixed Points
[Measurement_Green, ~, ~, ~, ~, ~, ~] = LumpedFixedPoint_FiniteEta(Input_Data(:,1:3), LumpedParameters, Input_Data(:,4));

%% Compute Error
Error = abs(Measurement_Green - Measurement_Data(:,1));

%% Optimization Weights
Weights = ones(size(Input_Data,1), 1);
Weights(Input_Data(:,4) == 0 & Input_Data(:,3) == 0) = 0;
Weights(Input_Data(:,4) == 0 & Input_Data(:,3) > 0) = 0;

Weights(Input_Data(:,4) == 1 & Input_Data(:,3) == 0) = 0;
Weights(Input_Data(:,4) == 1 & Input_Data(:,3) > 0) = 0;

Weights(Input_Data(:,4) == 2 & Input_Data(:,3) == 0) = 1;
Weights(Input_Data(:,4) == 2 & Input_Data(:,3) > 0) = 0;

Weights(Input_Data(:,4) == 3 & Input_Data(:,3) == 0) = 0;
Weights(Input_Data(:,4) == 3 & Input_Data(:,3) > 0) = 0;

%% Compute Cost Function
J = norm(Weights.*Error, 2);
end

