function [] = Compare_FullData_Simulation_Prediction_Normalized(DataTable, LumpedParameters, N_Sim, XScale)
%% Extract Inputs and Measurements
[Input_Data, Measurement_Data, ~, Measurement1_Data, Measurement2_Data, Measurement3_Data] = GenerateData_PI(DataTable);

%% Construct Inputs
G_1_Min = 0;
G_1_Max = Input_Data(1,2)*1.2;
G_1 = linspace(G_1_Min, G_1_Max, N_Sim)';
G_2 = Input_Data(1,2) * ones(N_Sim,1);

%% Compute Fixed Points for the Open Loop Without P-Control
Type = zeros(N_Sim,1);
% Without Disturbance
D = zeros(N_Sim,1);
Input = [G_1, G_2, D];
Measurement_OL_ND = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);
% With Disturbance
D = Input_Data(end,3) * ones(N_Sim,1);
Input = [G_1, G_2, D];
Measurement_OL_D = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);

%% Compute Fixed Points for the Open Loop With P-Control
Type = 2 * ones(N_Sim,1);
% Without Disturbance
D = zeros(N_Sim,1);
Input = [G_1, G_2, D];
Measurement_P_ND = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);
% With Disturbance
D = Input_Data(end,3) * ones(N_Sim,1);
Input = [G_1, G_2, D];
Measurement_P_D = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);

%% Compute Fixed Points for the Closed Loop Without Network Perturbation
Type = ones(N_Sim,1);
% Without Disturbance
D = zeros(N_Sim,1);
Input = [G_1, G_2, D];
Measurement_I_ND = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);
% With Disturbance
D = Input_Data(end,3) * ones(N_Sim,1);
Input = [G_1, G_2, D];
Measurement_I_D = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);

%% Compute Fixed Points for the Closed Loop With Network Perturbation
Type = 3 * ones(N_Sim,1);
% Without Disturbance
D = zeros(N_Sim,1);
Input = [G_1, G_2, D];
Measurement_PI_ND = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);
% With Disturbance
D = Input_Data(end,3) * ones(N_Sim,1);
Input = [G_1, G_2, D];
Measurement_PI_D = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);

%% General Figure Settings 
Colors = lines(10);
MyYellow = Colors(3,:); 
MyRed = [228,26,28]/255;
MyGreen = [77,175,74]/255;
MyOrange = [255,127,0]/255;
% MyYellow = (MyOrange + MyRed)/2;
MyBlue = [55,126,184]/255;
DrugColor = MyRed;
NetworkColor = MyOrange;
DrugNetworkColor = MyYellow;

SS = 4; % Screen Scale
Figure_Width = 12 * SS;
Figure_Height = 4 * SS;
FontSize = 7 * SS;
LineWidth = 1 * SS;
LineWidth_Marker = LineWidth/5;
MarkerSize = 1.5 * SS;
MarkerSize_Big = 3*MarkerSize;
MarkerSize_Small = 2*MarkerSize;
DotSpace = 0.001;

%% Figure 1 Settings
Handle_Figure1 = figure();
    Handle_Figure1.Color = [1 1 1];
    Handle_Figure1.PaperUnits = 'centimeters';
    Handle_Figure1.Units = 'centimeters';
    Handle_Figure1.Position = [1, 20, Figure_Width, Figure_Height];
    Handle_Figure1.PaperPositionMode = 'auto';
    Handle_Figure1.PaperSize = [Handle_Figure1.PaperPosition(3), Handle_Figure1.PaperPosition(4)];
figure(Handle_Figure1);

Handle_Axis1 = subplot(2,2,1);
    Handle_Axis1.Position = [0.09, 0.17, 0.39, 0.66];
    hold(Handle_Axis1, 'on');
    Handle_Axis1.Box = 'on';
    Handle_Axis1.FontSize = FontSize;
    hold(Handle_Axis1, 'on');
    grid(Handle_Axis1, 'on');
    Handle_Axis1.XMinorGrid = 'off';
    Handle_Axis1.YMinorGrid = 'off';
    Handle_Axis1.XLabel.String = '$G_1/G_2$';
    Handle_Axis1.XLabel.Interpreter = 'latex';
    Handle_Axis1.YLabel.String = {'Normalized'; 'Fluorescence'};
    Handle_Axis1.Title.String = 'No I-Control';
    Handle_Axis1.XTick = [0, 1/4, 1/2, 1];
    Handle_Axis1.XTickLabel = {'0', '1/4', '1/2', '1'};
    Handle_Axis1.XLim = [0, 1.02];
    Handle_Axis1.YLim = [0, 1.2];
    Handle_Axis1.XScale = XScale;
    
Handle_Axis2 = subplot(2,2,2);
    Handle_Axis2.Position = [0.54, 0.17, 0.39, 0.66];
    hold(Handle_Axis2, 'on');
    Handle_Axis2.Box = 'on';
    Handle_Axis2.FontSize = FontSize;
    hold(Handle_Axis2, 'on');
    grid(Handle_Axis2, 'on');
    Handle_Axis2.XMinorGrid = 'off';
    Handle_Axis2.YMinorGrid = 'off';
    Handle_Axis2.XLabel.String = '$G_1/G_2$';
    Handle_Axis2.XLabel.Interpreter = 'latex';
    Handle_Axis2.Title.String = 'I-Control';
    Handle_Axis2.XTick = [0, 1/4, 1/2, 1];
    Handle_Axis2.XTickLabel = {'0', '1/4', '1/2', '1'};
    Handle_Axis2.XLim = [0, 1.02];
    Handle_Axis2.YLim = [0, 1.2];
    Handle_Axis2.XScale = XScale;
    Handle_Axis2.YTickLabel = {};
    
%% Indeces
Indeces_OL_ND = find(Input_Data(:,4) == 0 & Input_Data(:,3) == 0);
Indeces_OL_D = find(Input_Data(:,4) == 0 & Input_Data(:,3) > 0);

Indeces_I_ND = find(Input_Data(:,4) == 1 & Input_Data(:,3) == 0);
Indeces_I_D = find(Input_Data(:,4) == 1 & Input_Data(:,3) > 0);

Indeces_P_ND = find(Input_Data(:,4) == 2 & Input_Data(:,3) == 0);
Indeces_P_D = find(Input_Data(:,4) == 2 & Input_Data(:,3) > 0);

Indeces_PI_ND = find(Input_Data(:,4) == 3 & Input_Data(:,3) == 0);
Indeces_PI_D = find(Input_Data(:,4) == 3 & Input_Data(:,3) > 0);

%% Plotting Figure 1
Normalization_OL = max(Measurement_Data([Indeces_OL_ND; Indeces_OL_D; Indeces_P_ND; Indeces_P_D]));
Normalization_CL = max(Measurement_Data([Indeces_I_ND; Indeces_I_D; Indeces_PI_ND; Indeces_PI_D]));

% ------------------------
% Open Loop No Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis1, G_1 ./ G_2, Measurement_OL_ND / Normalization_OL, 'LineWidth', LineWidth, 'Color', MyGreen);
% Diamonds
plot(Handle_Axis1, Input_Data(Indeces_OL_ND,1) ./ Input_Data(Indeces_OL_ND,2), Measurement_Data(Indeces_OL_ND) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
% Data Points 
plot(Handle_Axis1, Input_Data(Indeces_OL_ND,1) ./ Input_Data(Indeces_OL_ND,2) - DotSpace, Measurement1_Data(Indeces_OL_ND) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis1, Input_Data(Indeces_OL_ND,1) ./ Input_Data(Indeces_OL_ND,2), Measurement2_Data(Indeces_OL_ND) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis1, Input_Data(Indeces_OL_ND,1) ./ Input_Data(Indeces_OL_ND,2) + DotSpace, Measurement3_Data(Indeces_OL_ND) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
 
% ------------------------
% Open Loop With Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis1, G_1 ./ G_2, Measurement_OL_D / Normalization_OL, 'LineWidth', LineWidth, 'Color', DrugColor);
% Diamonds
plot(Handle_Axis1, Input_Data(Indeces_OL_D,1) ./ Input_Data(Indeces_OL_D,2), Measurement_Data(Indeces_OL_D) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
% Data Points 
plot(Handle_Axis1, Input_Data(Indeces_OL_D,1) ./ Input_Data(Indeces_OL_D,2) - DotSpace, Measurement1_Data(Indeces_OL_D) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis1, Input_Data(Indeces_OL_D,1) ./ Input_Data(Indeces_OL_D,2), Measurement2_Data(Indeces_OL_D) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis1, Input_Data(Indeces_OL_D,1) ./ Input_Data(Indeces_OL_D,2) + DotSpace, Measurement3_Data(Indeces_OL_D) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
 
% ------------------------
% P-Control No Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis1, G_1 ./ G_2, Measurement_P_ND / Normalization_OL, 'LineWidth', LineWidth, 'Color', NetworkColor);
% Diamonds
plot(Handle_Axis1, Input_Data(Indeces_P_ND,1) ./ Input_Data(Indeces_P_ND,2), Measurement_Data(Indeces_P_ND) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
% Data Points 
plot(Handle_Axis1, Input_Data(Indeces_P_ND,1) ./ Input_Data(Indeces_P_ND,2) - DotSpace, Measurement1_Data(Indeces_P_ND) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
plot(Handle_Axis1, Input_Data(Indeces_P_ND,1) ./ Input_Data(Indeces_P_ND,2), Measurement2_Data(Indeces_P_ND) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
plot(Handle_Axis1, Input_Data(Indeces_P_ND,1) ./ Input_Data(Indeces_P_ND,2) + DotSpace, Measurement3_Data(Indeces_P_ND) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
 
% ------------------------
% P-Control With Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis1, G_1 ./ G_2, Measurement_P_D / Normalization_OL, 'LineWidth', LineWidth/1.5, 'Color', DrugNetworkColor, 'LineStyle', '--');
% Diamonds
plot(Handle_Axis1, Input_Data(Indeces_P_D,1) ./ Input_Data(Indeces_P_D,2), Measurement_Data(Indeces_P_D) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');
% Data Points 
plot(Handle_Axis1, Input_Data(Indeces_P_D,1) ./ Input_Data(Indeces_P_D,2) - DotSpace, Measurement1_Data(Indeces_P_D) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');
plot(Handle_Axis1, Input_Data(Indeces_P_D,1) ./ Input_Data(Indeces_P_D,2), Measurement2_Data(Indeces_P_D) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');
plot(Handle_Axis1, Input_Data(Indeces_P_D,1) ./ Input_Data(Indeces_P_D,2)+ DotSpace, Measurement3_Data(Indeces_P_D) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');

yyaxis(Handle_Axis1, 'right');
Handle_Axis1.YLim = [0, 1.2*Normalization_OL];
Handle_Axis1.YAxis(2).Color = MyGreen;

% ------------------------
% I-Control No Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis2, G_1 ./ G_2, Measurement_I_ND / Normalization_CL, 'LineWidth', LineWidth, 'Color', MyGreen);
% Diamonds
plot(Handle_Axis2, Input_Data(Indeces_I_ND,1) ./ Input_Data(Indeces_I_ND,2), Measurement_Data(Indeces_I_ND) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
% Data Points 
plot(Handle_Axis2, Input_Data(Indeces_I_ND,1) ./ Input_Data(Indeces_I_ND,2) - DotSpace, Measurement1_Data(Indeces_I_ND) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis2, Input_Data(Indeces_I_ND,1) ./ Input_Data(Indeces_I_ND,2), Measurement2_Data(Indeces_I_ND) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis2, Input_Data(Indeces_I_ND,1) ./ Input_Data(Indeces_I_ND,2) + DotSpace, Measurement3_Data(Indeces_I_ND) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', MyGreen, 'Color', 'k');
 
% ------------------------
% I-Control With Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis2, G_1 ./ G_2, Measurement_I_D / Normalization_CL, 'LineWidth', LineWidth, 'Color', DrugColor);
% Diamonds
plot(Handle_Axis2, Input_Data(Indeces_I_D,1) ./ Input_Data(Indeces_I_D,2), Measurement_Data(Indeces_I_D) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
% Data Points 
plot(Handle_Axis2, Input_Data(Indeces_I_D,1) ./ Input_Data(Indeces_I_D,2) - DotSpace, Measurement1_Data(Indeces_I_D) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis2, Input_Data(Indeces_I_D,1) ./ Input_Data(Indeces_I_D,2), Measurement2_Data(Indeces_I_D) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis2, Input_Data(Indeces_I_D,1) ./ Input_Data(Indeces_I_D,2) + DotSpace, Measurement3_Data(Indeces_I_D) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugColor, 'Color', 'k');
 
% ------------------------
% PI-Control No Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis2, G_1 ./ G_2, Measurement_PI_ND / Normalization_CL, 'LineWidth', LineWidth, 'Color', NetworkColor);
% Diamonds
plot(Handle_Axis2, Input_Data(Indeces_PI_ND,1) ./ Input_Data(Indeces_PI_ND,2), Measurement_Data(Indeces_PI_ND) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
% Data Points 
plot(Handle_Axis2, Input_Data(Indeces_PI_ND,1) ./ Input_Data(Indeces_PI_ND,2) - DotSpace, Measurement1_Data(Indeces_PI_ND) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
plot(Handle_Axis2, Input_Data(Indeces_PI_ND,1) ./ Input_Data(Indeces_PI_ND,2), Measurement2_Data(Indeces_PI_ND) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
plot(Handle_Axis2, Input_Data(Indeces_PI_ND,1) ./ Input_Data(Indeces_PI_ND,2) + DotSpace, Measurement3_Data(Indeces_PI_ND) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', NetworkColor, 'Color', 'k');
 
% ------------------------
% PI-Control With Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis2, G_1 ./ G_2, Measurement_PI_D / Normalization_CL, 'LineWidth', LineWidth/1.5, 'Color', DrugNetworkColor, 'LineStyle', '--');
% Diamonds
plot(Handle_Axis2, Input_Data(Indeces_PI_D,1) ./ Input_Data(Indeces_PI_D,2), Measurement_Data(Indeces_PI_D) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');
% Data Points 
plot(Handle_Axis2, Input_Data(Indeces_PI_D,1) ./ Input_Data(Indeces_PI_D,2) - DotSpace, Measurement1_Data(Indeces_PI_D) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');
plot(Handle_Axis2, Input_Data(Indeces_PI_D,1) ./ Input_Data(Indeces_PI_D,2), Measurement2_Data(Indeces_PI_D) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');
plot(Handle_Axis2, Input_Data(Indeces_PI_D,1) ./ Input_Data(Indeces_PI_D,2)+ DotSpace, Measurement3_Data(Indeces_PI_D) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', DrugNetworkColor, 'Color', 'k');
 
yyaxis(Handle_Axis2, 'right');
Handle_Axis2.YLim = [0, 1.2*Normalization_CL];
Handle_Axis2.YLabel.String = 'Fluorescence [a.u.]';
Handle_Axis2.YAxis(2).Color = MyGreen;

% -------
% Legends
% -------
Handle_White = plot(Handle_Axis1, -1, -1, 'Color', [1, 1, 1]);
Handle_DataPoint = plot(Handle_Axis1, -1, -1, 'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', [0.5, 0.5, 0.5], 'Color', 'k'); 
Handle_Fit = plot(Handle_Axis1, -1, -1, 'LineWidth', LineWidth, 'Color', [0.5, 0.5, 0.5], 'LineStyle', '-');
Handle_Prediction = plot(Handle_Axis1, -1, -1, 'LineWidth', LineWidth/1.4, 'Color', [0.5, 0.5, 0.5], 'LineStyle', '--');
Handle_Diamond = plot(Handle_Axis1, -1,-1, 'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', MarkerSize_Big, 'LineWidth', LineWidth_Marker, 'MarkerFaceColor', [0.5, 0.5, 0.5], 'Color', 'k');
Handle_Legend1 = legend(Handle_Axis1, [Handle_DataPoint, Handle_Diamond, Handle_Fit, Handle_Prediction], 'Measurement{      }', 'Average{      }', 'Model Calibration{      }', 'Model Prediction');
    Handle_Legend1.Orientation = 'Horizontal';
    Handle_Legend1.EdgeColor = 'none';
    Handle_Legend1.Position = [0.3, 0.935, 0.4, 0.05];
    Handle_Legend1.Color = 'none';
Handle_Box = annotation('rectangle', [Handle_Legend1.Position(1) + Handle_Legend1.Position(3) + 0.01, Handle_Legend1.Position(2), Handle_Legend1.Position(3:4) + [0.02, 0]]);
yyaxis(Handle_Axis2, 'left');
Handle_NNP_ND = bar(Handle_Axis2, -1, -1, 'FaceColor', MyGreen, 'EdgeColor', MyGreen);
Handle_NNP_D = bar(Handle_Axis2, -1, -1, 'FaceColor', DrugColor, 'EdgeColor', DrugColor);
Handle_NP_ND = bar(Handle_Axis2, -1, -1, 'FaceColor', NetworkColor, 'EdgeColor', NetworkColor);
Handle_NP_D = bar(Handle_Axis2, -1, -1, 'FaceColor', DrugNetworkColor, 'EdgeColor', DrugNetworkColor);
Handle_Legend2 = legend(Handle_Axis2, [Handle_NNP_ND, Handle_NNP_D, Handle_NP_ND, Handle_NP_D],  {'Without Disturbance, Without P-Control', 'With Disturbance, Without P-Control', 'Without Disturbance, With P-Control', 'With Disturbance, With P-Control'});
    Handle_Legend2.FontSize = FontSize / 1.65;
    Handle_Legend2.Location = 'SouthEast';
Handle_Box.Position = [0.15, 0.92, 0.7, 0.08];
end

