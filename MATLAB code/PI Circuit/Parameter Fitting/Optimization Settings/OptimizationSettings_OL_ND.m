function [Input_Data, Measurement_Data, Measurement_SEM, ...
          LumpedParameters_IG,  LumpedParameters_LB,  LumpedParameters_UB,  ...
          A, B, Aeq, Beq, Options] = OptimizationSettings_OL_ND(DataFile)
      
%% Generate Data
Data = load(DataFile);
[Input_Data, Measurement_Data, Measurement_SEM, ~, ~, ~] = GenerateData_PI(Data.DataTable);
clear Data;

%% Initial Guess for Lumped Parameters
LumpedParameters_IG = Class_LumpedParameters();
LumpedParameters_IG.gamma_bar = 1.01e-5;          	% [1/a.u.]                  gamma_bar = (gamma/k) * ((c+gamma)/c) / c_x
LumpedParameters_IG.delta_bar = 9.5e-3;             % [Dimensionless]           delta_bar = delta / k_1
LumpedParameters_IG.eta_bar = 2.59e3;             	% [1/nM]                    eta_bar = eta / k_1
LumpedParameters_IG.kappa_bar = 10.1e4;             % [a.u.]                    kappa_bar = c_x * kappa
LumpedParameters_IG.kappa_bar_2 = 1.22e+04;         % [a.u.]                    kappa_bar_2 = c_x * sqrt(kappa * kappa_2)
LumpedParameters_IG.k_bar_0 = 0;                    % [Dimensionless]           k_bar_0 = k_0 / k_1
LumpedParameters_IG.k_bar_2 = 2.13;                 % [Dimensionless]           k_bar_2 = k_2 / k_1
LumpedParameters_IG.Delta_1 = 0;                    % [1/nM]                    Delta_1 = k_3 / (kappa_3*c_bar)
LumpedParameters_IG.Delta_2 = 1.26e-06;             % [1/a.u./nM]            	Delta_2 = (k_prime_3*gamma) / (kappa_prime_3^2*c_bar*c*c_x)
LumpedParameters_IG.Delta_prime_1 = 0;              % [1/a.u.]               	Delta_prime_1 = gamma / (kappa_3*c*c_x)
LumpedParameters_IG.Delta_prime_2 = 0;              % [1/a.u.^2]               	Delta_prime_2 = gamma^2 / (kappa_prime_3^2*c^2*c_x^2)
LumpedParameters_IG.a_prime_bar =  0.1;             % [1/nM]                 	a_prime_bar = a_prime / gamma_prime
LumpedParameters_IG.d_prime_bar = 0.1;              % [Dimensionless]        	d_prime_bar = d_prime_bar = d_prime/delta
LumpedParameters_IG.k_bar = 1;                      % [Dimensionless]           k_prime_bar = k / k_1

Fraction_LB = 0.9;
Fraction_UB = 10;
[LumpedParameters_LB, LumpedParameters_UB] = Generate_UBLB_OL_ND(LumpedParameters_IG, Fraction_LB, Fraction_UB);

%% Linear Constraints
A = []; B = []; Aeq = []; Beq = [];

%% Solver Options
Options = optimoptions(@fmincon, 'Display','iter', 'Algorithm', 'sqp', 'TolFun', 1e-14, 'MaxFunEvals', 1e4, 'TolCon',1e-14, 'TolX',1e-14, 'MaxIter', 1e4);
% Options = optimoptions(@fmincon, 'Display','iter', 'Algorithm', 'sqp', 'MaxFunEvals', 1000);

end

