function [Input_Data, Measurement_Data, Measurement_SEM, ...
          Measurement1_Data, Measurement2_Data, Measurement3_Data] = GenerateData_PI(DataTable)
% Input_Data = [G_1, G_2, D, Type] in [pmol, pmol, nM, {0,1,2,3}]
%           Type:
%           0 : Open Loop
%           1 : I-Control
%           2 : P-Control
%           3 : PI-Control
% Measurement_Data = Green in [a.u.]
% The data generated are sorted first in increasing setpoints, then as Open
% Loop, I, P, PI, then Without/With Disturbance

%% Extract Inputs and Measurements
N = size(DataTable,1) / 3;
Input_Data = zeros(N,4);
Measurement_Data = zeros(N,1);
Measurement_SEM = zeros(N,1);
Measurement1_Data = zeros(N,1);
Measurement2_Data = zeros(N,1);
Measurement3_Data = zeros(N,1);
for i = 1 : N
    Input_Data(i,1) = DataTable{3*(i-1)+1,5};                                       % G_1   [pmol]
    Input_Data(i,2) = DataTable{3*(i-1)+1,6};                                       % G_2   [pmol]
    Input_Data(i,3) = DataTable{3*(i-1)+1,2} * 1e3;                                 % D     [nM]
    if (DataTable{3*(i-1)+1,7} == 'Open' && DataTable{3*(i-1)+1,8} == 'No')
        Input_Data(i,4) = 0;
    elseif (DataTable{3*(i-1)+1,7} == 'Closed' && DataTable{3*(i-1)+1,8} == 'No')
        Input_Data(i,4) = 1;
    elseif (DataTable{3*(i-1)+1,7} == 'Open' && DataTable{3*(i-1)+1,8} == 'Yes')
        Input_Data(i,4) = 2;
    elseif (DataTable{3*(i-1)+1,7} == 'Closed' && DataTable{3*(i-1)+1,8} == 'Yes')
        Input_Data(i,4) = 3;
    else
        error('Only Open Loop, I, P and PI controls are allowed.');
    end
    Measurement_Data(i) = mean(DataTable{3*(i-1)+1:3*i,4});                         % Green [a.u.]
    Measurement_SEM(i) = std(DataTable{3*(i-1)+1:3*i,4})/sqrt(3);                   % Green [a.u.]
    Measurement1_Data(i,1) = DataTable{3*(i-1)+1,4};                                % Green [a.u.]
    Measurement2_Data(i,1) = DataTable{3*(i-1)+2,4};                                % Green [a.u.]
    Measurement3_Data(i,1) = DataTable{3*(i-1)+3,4};                                % Green [a.u.]
end

%% Sort 
[Input_Data, Indeces] = sortrows(Input_Data, [1, 4, 3]);
Measurement_Data = Measurement_Data(Indeces);
Measurement_SEM = Measurement_SEM(Indeces);
Measurement1_Data = Measurement1_Data(Indeces);
Measurement2_Data = Measurement2_Data(Indeces);
Measurement3_Data = Measurement3_Data(Indeces);

end