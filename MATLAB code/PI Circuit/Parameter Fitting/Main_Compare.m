%% Clear Workspace
close all;
clear;
clc;
Save_Flag = 0;

%% Select Parameter Fit
load Results_NP_ND;
% load Results_OL_ND;
% load Results_I_ND;
% load Results_P_ND;
% load Results_OL_D;

%% Generate Data
Data = load('FullData_PI1');
[Input_Data, Measurement_Data, ~, ~, ~, ~] = GenerateData_PI(Data.DataTable);

%% Override Manual Tuning
LumpedParameters.gamma_bar = (25.4/9.5)*LumpedParameters.gamma_bar;          	% [1/a.u.]                  gamma_bar = (gamma/k) * ((c+gamma)/c) / c_x
LumpedParameters.delta_bar = (9.5/25.4)*LumpedParameters.delta_bar;             % [dimensionless]           delta_bar = delta / k_1
LumpedParameters.eta_bar = 10*LumpedParameters.eta_bar;                         % [1/nM]                    eta_bar = eta / k_1
LumpedParameters.kappa_bar = sqrt(0.1)*LumpedParameters.kappa_bar;           	% [a.u.]                    kappa_bar = c_x * kappa
LumpedParameters.kappa_bar_2 = sqrt(0.17)*LumpedParameters.kappa_bar_2;      	% [a.u.]                    kappa_bar_2 = c_x * sqrt(kappa * kappa_2)
% LumpedParameters.k_bar_0 = LumpedParameters.k_bar_0;                        % [Dimensionless]           k_bar_0 = k_0 / k_1
LumpedParameters.k_bar_2 = 0.25*LumpedParameters.k_bar_2;                        % [Dimensionless]           k_bar_2 = k_2 / k_1
% LumpedParameters.Delta_1 = LumpedParameters.Delta_1;                        % [1/nM]                    Delta_1 = k_3 / (kappa_3*c_bar)
LumpedParameters.Delta_2 = (1/6)*LumpedParameters.Delta_2;                    % [1/a.u./nM]            	Delta_2 = (k_prime_3*gamma) / (kappa_prime_3^2*c_bar*c*c_x)
% LumpedParameters.Delta_prime_1 = LumpedParameters.Delta_prime_1;            % [1/a.u.]               	Delta_prime_1 = gamma / (kappa_3*c*c_x)
% LumpedParameters.Delta_prime_2 = LumpedParameters.Delta_prime_2;            % [1/a.u.^2]               	Delta_prime_2 = gamma^2 / (kappa_prime_3^2*c^2*c_x^2)
LumpedParameters.a_prime_bar = 9.1;
LumpedParameters.d_prime_bar = 903;
LumpedParameters.k_bar = 1.8;
LumpedParameters

%% Select Compare Function
N = 100;
Compare_FullData_Simulation_Prediction_Normalized(Data.DataTable, LumpedParameters, N, 'linear');
% Compare_FullData_Simulation_Prediction_Normalized_Mini(Data.DataTable, LumpedParameters, N, 'linear');


if Save_Flag == 1
    Handle_Figure1 = gcf;
    Handle_Figure1.Color = 'none';
    set(Handle_Figure1, 'InvertHardCopy', 'off');
    print(gcf, 'Model_Fit_PI_Normalized','-dpdf');
%     print(gcf, 'Model_Fit_PI_Normalized_Mini','-dpdf');
    Handle_Figure1.Color = [1 1 1];
end
