function S = StoichiometryMatrix()
% Stoichiometry Matrix for PI-Circuit
% 	 Species: 		 X = [X_1; X_2; A; Z_1; Z_2; Z_prime_1; X_prime_1]
% 	 Reactions: 	R1:         X_1                     -->         phi                     [gamma*X_1 + D * (k_3 * (X_1/kappa_3) + k_prime_3 * (X_1/kappa_prime_3)^2 ) / (1 + (X_1/kappa_3) + (X_1/kappa_prime_3)^2)]
% 				    R2:         X_1                     -->         X_2                     [c*X_1]
% 				    R3:         X_2                     -->         phi                     [gamma*X_2]
%                   R4:         A                       -->         phi                     [gamma*A]
%                   R5:         X2 + X2                 -->         A                       [a*X_2^2]
%                   R6:         A                       -->         X2 + X2                 [d*A]
% 				    R7:         Z1                      -->         X_prime_1 +  Z1         [k*Z_1]
% 				    R8:         X_prime_1				-->         phi                     [gamma_prime*X_prime_1]
% 				    R9:         X_prime_1 +  Z_1		-->         Z_prime_1				[a_prime*X_prime_1*Z_1]
% 				    R10:        Z_prime_1				-->         X_prime_1 +  Z_1		[d_prime*Z_prime_1]
% 				    R11:		Z_prime_1				-->         phi                     [delta*Z_prime_1]
% 				    R12:		Z_prime_1 +  Z_2        -->         phi                     [eta*Z_prime_1*Z_2]
% 				    R13:		phi                     -->         Z_1                     [k_1*G_1]
% 				    R14:        A                       -->         A +  Z_2				[G_2 * (k_0 + k_2 * (A/kappa_2)) / (1 + A/kappa_2)]
% 				    R15:		Z_1 +  Z_2				-->         phi                     [eta*Z_1*Z_2]
% 				    R16:		Z_1                     -->         X_1 +  Z_1				[k*Z_1]
% 				    R17:		Z_1                     -->         phi                     [delta*Z_1]
% 				    R18:		Z_2                     -->         phi                     [delta*Z_2]

S = [   -1,     -1,     0,      0,      0,      0,      0,      0,    	0,      0,      0,      0,      0,      0,      0,      1,      0,      0; ...                        
        0,      1,      -1,     0,      -2,     2,      0,      0,  	0,      0,      0,      0,      0,      0,      0,      0,      0,      0; ...
        0,      0,      0,      -1,     1,      -1,     0,      0,     	0,      0,      0,      0,      0,      0,      0,      0,      0,      0; ...
        0,      0,      0,      0,      0,      0,      0,      0,    	-1,     1,      0,      0,      1,      0,      -1,     0,      -1,     0; ...
        0,      0,      0,      0,      0,      0,      0,      0,    	0,      0,      0,      -1,     0,      1,      -1,     0,      0,      -1; ...
        0,      0,      0,      0,      0,      0,      0,      0,      1,      -1,     -1,     -1,     0,      0,      0,      0,      0,      0; ...
        0,      0,      0,      0,      0,      0,      1,      -1,   	-1,     1,      0,      0,      0,      0,      0,      0,      0,      0    ];
end