%% Clear Workspace
% close all;
clear;
clc;
Save_Flag = 0;

%% Inputs & Disturbances
G_1 = 0.002;                                                    % [pmol]
G_2 = 0.004;                                                    % [pmol]
D = 0;                                                          % [nM]
Input = [G_1, G_2, D];
Type = 3;                                                       % Type 0 : Open Loop
                                                                % Type 1 : I Control
                                                                % Type 2 : P Control
                                                                % Type 3 : PI Control
        
%% Biological Parameters
load LumpedParameters;
k = 5;
c = 10;
d = 10;
c_x = 1e3;
Ratio_gamma_gamma_prime = 1;
BioParameters = Lumped2Bio(LumpedParameters, k, c, d, c_x, Ratio_gamma_gamma_prime);
Factor = 1e3;
BioParameters.a_prime = BioParameters.a_prime/Factor;
BioParameters.eta = BioParameters.eta/Factor;
BioParameters.a = BioParameters.a/Factor;
BioParameters.c_x = BioParameters.c_x/Factor;
BioParameters.kappa_2 = BioParameters.kappa_2*Factor;
BioParameters.k_1 = BioParameters.k_1 * Factor;
BioParameters.k_2 = BioParameters.k_2 * Factor;


%% Time Horizon and Initial Conditions
tf = 12*60;                                                   % [min]
N = 1000;
IC = [0; 0; 0; 0; 0; 0; 0];

%% Select Scenario
switch Type
    case 0      % Type 0 : Open Loop 
        BioParameters.a_prime = 0;
        BioParameters.kappa_2 = 1e10;    
    case 1      % Type 1 : I Control
        BioParameters.a_prime = 0;
    case 2      % Type 2 : P Control
        BioParameters.kappa_2 = 1e10;
    case 3      % Type 3 : PI Control
end
        
%% Simulation
Solver = 'ODE23s';
NetworkParameters = struct(BioParameters);
NetworkParameters.G_1 = G_1;
NetworkParameters.G_2 = G_2;
NetworkParameters.D = D;
[time_vector, Solution] = DSA(StoichiometryMatrix(), @PropensityFunction, NetworkParameters, IC, tf, N, Solver);
X_1 = Solution(1,:); X_2 = Solution(2,:); A = Solution(3,:);
Z_1 = Solution(4,:); Z_2 = Solution(5,:);
Z_prime_1 = Solution(6,:); X_prime_1 = Solution(7,:);

%% Fixed Point
LumpedParameters = Bio2Lumped(BioParameters);
tic
[X_tilde_1, X_tilde_2, A_tilde, Z_bar_1, Z_bar_2, Z_bar_prime_1, ~] = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);
toc
X_bar_1 = X_tilde_1 * BioParameters.gamma / BioParameters.c / BioParameters.c_x;
X_bar_2 = X_tilde_2 / BioParameters.c_x;
A_bar = A_tilde / BioParameters.c_x;
X_bar_prime_1 = BioParameters.k_1 * (LumpedParameters.k_bar * Z_bar_1 - Z_bar_prime_1 * (LumpedParameters.eta_bar * Z_bar_2 + LumpedParameters.delta_bar)) / BioParameters.gamma_prime;

%% Figure Settings
SS = 4; % Screen Scale
Colors = lines(10);
MyBlue = Colors(1,:); MyRed = Colors(2,:); MyGreen = Colors(5,:); MyPurple = Colors(3,:);
Figure_Width = 7 * SS;
Figure_Height = 8 * SS;
FontSize = 7 * SS;
FontSize_Small = 4 * SS;
LineWidth = 0.5 * SS;
LineWidth_Thick = 1 * SS;
MarkerSize = 5 * SS;
% Figure 1
Handle_Figure1 = figure();
    Handle_Figure1.Color = [1 1 1];
    Handle_Figure1.PaperUnits = 'centimeters';
    Handle_Figure1.Units = 'centimeters';
    Handle_Figure1.Position = [1, 20, Figure_Width, Figure_Height];
    Handle_Figure1.PaperPositionMode = 'auto';
    Handle_Figure1.PaperSize = [Handle_Figure1.PaperPosition(3), Handle_Figure1.PaperPosition(4)];
Handle_Axis1 = subplot(3,3,1);
    Handle_Axis1.Box = 'on';
    Handle_Axis1.FontSize = FontSize;
    hold(Handle_Axis1, 'on');
    grid(Handle_Axis1, 'on');
    Handle_Axis1.XMinorGrid = 'on';
    Handle_Axis1.YMinorGrid = 'on';
    Handle_Axis1.XLabel.String = '$t$';
    Handle_Axis1.YLabel.String = '$X_1$';
    Handle_Axis1.XLabel.Interpreter = 'latex';
    Handle_Axis1.YLabel.Interpreter = 'latex';
Handle_Axis2 = subplot(3,3,2);
    Handle_Axis2.Box = 'on';
    Handle_Axis2.FontSize = FontSize;
    hold(Handle_Axis2, 'on');
    grid(Handle_Axis2, 'on');
    Handle_Axis2.XMinorGrid = 'on';
    Handle_Axis2.YMinorGrid = 'on';
    Handle_Axis2.XLabel.String = '$t$';
    Handle_Axis2.YLabel.String = '$X_2$';
    Handle_Axis2.XLabel.Interpreter = 'latex';
    Handle_Axis2.YLabel.Interpreter = 'latex';
Handle_Axis3 = subplot(3,3,3);
    Handle_Axis3.Box = 'on';
    Handle_Axis3.FontSize = FontSize;
    hold(Handle_Axis3, 'on');
    grid(Handle_Axis3, 'on');
    Handle_Axis3.XMinorGrid = 'on';
    Handle_Axis3.YMinorGrid = 'on';
    Handle_Axis3.XLabel.String = '$t$';
    Handle_Axis3.YLabel.String = '$A$'; 
    Handle_Axis3.XLabel.Interpreter = 'latex';
    Handle_Axis3.YLabel.Interpreter = 'latex';
Handle_Axis4 = subplot(3,3,4);
    Handle_Axis4.Box = 'on';
    Handle_Axis4.FontSize = FontSize;
    hold(Handle_Axis4, 'on');
    grid(Handle_Axis4, 'on');
    Handle_Axis4.XMinorGrid = 'on';
    Handle_Axis4.YMinorGrid = 'on';
    Handle_Axis4.XLabel.String = '$t$';
    Handle_Axis4.YLabel.String = '$Z_1$'; 
    Handle_Axis4.XLabel.Interpreter = 'latex';
    Handle_Axis4.YLabel.Interpreter = 'latex';
Handle_Axis5 = subplot(3,3,5);
    Handle_Axis5.Box = 'on';
    Handle_Axis5.FontSize = FontSize;
    hold(Handle_Axis5, 'on');
    grid(Handle_Axis5, 'on');
    Handle_Axis5.XMinorGrid = 'on';
    Handle_Axis5.YMinorGrid = 'on';
    Handle_Axis5.XLabel.String = '$t$';
    Handle_Axis5.YLabel.String = '$Z_2$'; 
    Handle_Axis5.XLabel.Interpreter = 'latex';
    Handle_Axis5.YLabel.Interpreter = 'latex';
Handle_Axis6 = subplot(3,3,6);
    Handle_Axis6.Box = 'on';
    Handle_Axis6.FontSize = FontSize;
    hold(Handle_Axis6, 'on');
    grid(Handle_Axis6, 'on');
    Handle_Axis6.XMinorGrid = 'on';
    Handle_Axis6.YMinorGrid = 'on';
    Handle_Axis6.XLabel.String = '$t$';
    Handle_Axis6.YLabel.String = '$Z_1''$'; 
    Handle_Axis6.XLabel.Interpreter = 'latex';
    Handle_Axis6.YLabel.Interpreter = 'latex';
Handle_Axis7 = subplot(3,3,7);
    Handle_Axis7.Box = 'on';
    Handle_Axis7.FontSize = FontSize;
    hold(Handle_Axis7, 'on');
    grid(Handle_Axis7, 'on');
    Handle_Axis7.XMinorGrid = 'on';
    Handle_Axis7.YMinorGrid = 'on';
    Handle_Axis7.XLabel.String = '$t$';
    Handle_Axis7.YLabel.String = '$X_1''$'; 
    Handle_Axis7.XLabel.Interpreter = 'latex';
    Handle_Axis7.YLabel.Interpreter = 'latex';
Handle_Axis8 = subplot(3,3,8);
    Handle_Axis8.Box = 'on';
    Handle_Axis8.FontSize = FontSize;
    hold(Handle_Axis8, 'on');
    grid(Handle_Axis8, 'on');
    Handle_Axis8.XMinorGrid = 'on';
    Handle_Axis8.YMinorGrid = 'on';
    Handle_Axis8.XLabel.String = '$t$';
    Handle_Axis8.YLabel.String = '$M$'; 
    Handle_Axis8.XLabel.Interpreter = 'latex';
    Handle_Axis8.YLabel.Interpreter = 'latex';

%% Response
time_vector = time_vector / 60;
plot(Handle_Axis1, time_vector, X_1, 'Color', MyGreen, 'LineWidth', LineWidth);
plot(Handle_Axis2, time_vector, X_2, 'Color', MyGreen, 'LineWidth', LineWidth);
plot(Handle_Axis3, time_vector, A, 'Color', MyGreen, 'LineWidth', LineWidth);
plot(Handle_Axis4, time_vector, Z_1, 'Color', MyBlue, 'LineWidth', LineWidth);
plot(Handle_Axis5, time_vector, Z_2, 'Color', MyBlue, 'LineWidth', LineWidth);
plot(Handle_Axis6, time_vector, Z_prime_1, 'Color', MyPurple, 'LineWidth', LineWidth);
plot(Handle_Axis7, time_vector, X_prime_1, 'Color', MyPurple, 'LineWidth', LineWidth);
plot(Handle_Axis8, time_vector, X_1 + X_2 + 2*A, 'Color', MyGreen, 'LineWidth', LineWidth);


%% Steady State
plot(Handle_Axis1, time_vector(end), X_bar_1, 'Color', MyGreen, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis2, time_vector(end), X_bar_2, 'Color', MyGreen, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis3, time_vector(end), A_bar, 'Color', MyGreen, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis4, time_vector(end), Z_bar_1, 'Color', MyBlue, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis5, time_vector(end), Z_bar_2, 'Color', MyBlue, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis6, time_vector(end), Z_bar_prime_1, 'Color', MyPurple, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis7, time_vector(end), X_bar_prime_1, 'Color', MyPurple, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis8, time_vector(end), X_bar_1 + X_bar_2 + 2*A_bar, 'Color', MyGreen, 'LineWidth', LineWidth, 'Marker', 'x', 'MarkerSize', MarkerSize);

%% Saving
if Save_Flag == 1
    print(Handle_Figure1, 'Response', '-dpdf');
end


