function [BioParameters] = Lumped2Bio(LumpedParameters, k, c, d, c_x, Ratio_gamma_gamma_prime)
%% Extract Lumped Parameters
gamma_bar = LumpedParameters.gamma_bar;             % [1/a.u.]                  gamma_bar = (gamma/k) * ((c+gamma)/c) / c_x
delta_bar = LumpedParameters.delta_bar;             % [dimensionless]           delta_bar = delta / k_1
eta_bar = LumpedParameters.eta_bar;                 % [1/nM]                    eta_bar = eta / k_1
kappa_bar = LumpedParameters.kappa_bar;             % [a.u.]                    kappa_bar = c_x * kappa
kappa_bar_2 = LumpedParameters.kappa_bar_2;         % [a.u.]                    kappa_bar_2 = c_x * sqrt(kappa * kappa_2)
k_bar_0 = LumpedParameters.k_bar_0;                 % [Dimensionless]           k_bar_0 = k_0 / k_1
k_bar_2 = LumpedParameters.k_bar_2;                 % [Dimensionless]           k_bar_2 = k_2 / k_1
Delta_1 = LumpedParameters.Delta_1;                 % [1/nM]                    Delta_1 = k_3 / (kappa_3*c_bar)
Delta_2 = LumpedParameters.Delta_2;                 % [1/a.u./nM]            	Delta_2 = (k_prime_3*gamma) / (kappa_prime_3^2*c_bar*c*c_x)
Delta_prime_1 = LumpedParameters.Delta_prime_1;  	% [1/a.u.]               	Delta_prime_1 = gamma / (kappa_3*c*c_x)
Delta_prime_2 = LumpedParameters.Delta_prime_2;   	% [1/a.u.^2]               	Delta_prime_2 = gamma^2 / (kappa_prime_3^2*c^2*c_x^2)
a_prime_bar = LumpedParameters.a_prime_bar;         % [1/nM]                    a_prime_bar = a_prime / gamma_prime               
d_prime_bar = LumpedParameters.d_prime_bar;       	% [Dimensionless]           d_prime_bar = d_prime / delta          
k_bar = LumpedParameters.k_bar;                     % [Dimensionless]           k_bar = k / k_1

%% Calculate Bio Parameters
k_1 = k / k_bar;
delta = delta_bar * k_1;
eta = eta_bar * k_1;
k_0 = k_bar_0 * k_1;
k_2 = k_bar_2 * k_1;
gamma = (1/2) * (-c + sqrt(c^2 + 4*gamma_bar*k*c*c_x) );
kappa = kappa_bar / c_x;
kappa_2 = (kappa_bar_2/c_x)^2 / kappa;
gamma_prime = gamma / Ratio_gamma_gamma_prime;
a_prime = a_prime_bar * gamma_prime;
d_prime = d_prime_bar * delta;
a = (d + gamma) / kappa;

%% Construct BioParameters Object
BioParameters = Class_BioParameters();
BioParameters.gamma = gamma;                                    % [1/hr]
BioParameters.delta = delta;                                    % [1/hr]
BioParameters.eta = eta;                                        % [1/nM/hr]
BioParameters.k = k;                                            % [1/hr]
BioParameters.c = c;                                            % [1/hr]
BioParameters.a = a;                                            % [1/nM/hr]
BioParameters.d = d;                                            % [1/hr]
BioParameters.k_0 = k_0;                                        % [(nM/pmol)/hr]
BioParameters.k_1 = k_1;                                        % [(nM/pmol)/hr]
BioParameters.k_2 = k_2;                                        % [(nM/pmol)/hr]
BioParameters.k_3 = 1;                                          % [1/hr]
BioParameters.k_prime_3 = 1;                                    % [1/hr]
BioParameters.kappa_2 = kappa_2;                                % [nM]
BioParameters.kappa_3 = 1;                                      % [nM]
BioParameters.kappa_prime_3 = 1;                                % [nM]
BioParameters.c_x = c_x;                                        % [a.u./nM]
BioParameters.gamma_prime = gamma_prime;                        % [1/hr]
BioParameters.a_prime = a_prime;                                % [1/nM/hr]
BioParameters.d_prime = d_prime;                                % [1/hr]

end