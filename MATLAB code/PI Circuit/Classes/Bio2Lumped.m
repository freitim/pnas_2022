function [LumpedParameters] = Bio2Lumped(BioParameters)
%% Extract Bio Parameters
gamma = BioParameters.gamma;                        % [1/hr]
delta = BioParameters.delta;                        % [1/hr]
eta = BioParameters.eta;                            % [1/nM/hr]
k = BioParameters.k;                                % [1/hr]
c = BioParameters.c;                                % [1/hr]
a = BioParameters.a;                                % [1/nM/hr]
d = BioParameters.d;                                % [1/hr]
k_0 = BioParameters.k_0;                            % [(nM/pmol)/hr]
k_1 = BioParameters.k_1;                            % [(nM/pmol)/hr]
k_2 = BioParameters.k_2;                            % [(nM/pmol)/hr]
k_3 = BioParameters.k_3;                            % [1/hr]
k_prime_3 = BioParameters.k_prime_3;                % [1/hr]
kappa_2 = BioParameters.kappa_2;                    % [nM]
kappa_3 = BioParameters.kappa_3;                    % [nM]
kappa_prime_3 = BioParameters.kappa_prime_3;        % [nM]
c_x = BioParameters.c_x;                            % [a.u./nM]
gamma_prime = BioParameters.gamma_prime;            % [1/hr]
a_prime = BioParameters.a_prime;                    % [1/nM/hr]
d_prime = BioParameters.d_prime;                    % [1/hr]

c_bar = c + gamma;
kappa = (d + gamma) / a;

%% Calculate Lumped Parameters
LumpedParameters = Class_LumpedParameters();
LumpedParameters.gamma_bar = (gamma/k) * ((c+gamma)/c) / c_x;                   % [1/a.u.]                 
LumpedParameters.delta_bar = delta / k_1;                                       % [dimensionless]           
LumpedParameters.eta_bar = eta / k_1;                                           % [1/nM]                    
LumpedParameters.kappa_bar = c_x * kappa;                                       % [a.u.]                  
LumpedParameters.kappa_bar_2 = c_x * sqrt(kappa * kappa_2);                     % [a.u.]                    
LumpedParameters.k_bar_0 = k_0 / k_1;                                           % [Dimensionless]          
LumpedParameters.k_bar_2 = k_2 / k_1;                                           % [Dimensionless]          
LumpedParameters.Delta_1 = k_3 / (kappa_3*c_bar);                               % [1/nM]                  
LumpedParameters.Delta_2 = (k_prime_3*gamma) / (kappa_prime_3^2*c_bar*c*c_x);  	% [1/a.u./nM]            
LumpedParameters.Delta_prime_1 = gamma / (kappa_3*c*c_x);                       % [1/a.u.]              
LumpedParameters.Delta_prime_2 = gamma^2 / (kappa_prime_3^2*c^2*c_x^2);      	% [1/a.u.^2]  
LumpedParameters.a_prime_bar = a_prime / gamma_prime;                           % [1/nM]                  
LumpedParameters.d_prime_bar = d_prime / delta;                                 % [Dimensionless]                    
LumpedParameters.k_bar = k / k_1;                                               % [Dimensionless]          

end