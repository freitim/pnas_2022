function [X_tilde_1, X_tilde_2, A_tilde, Z_bar_1, Z_bar_2] = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type)
%% Extract Lumped Parameters
gamma_bar = LumpedParameters.gamma_bar;             % [1/a.u.]                  gamma_bar = (gamma/k) * ((c+gamma)/c) / c_x
delta_bar = LumpedParameters.delta_bar;             % [dimensionless]           delta_bar = delta / k_1
eta_bar = LumpedParameters.eta_bar;                 % [1/nM]                    eta_bar = eta / k_1
kappa_bar = LumpedParameters.kappa_bar;             % [a.u.]                    kappa_bar = c_x * kappa
kappa_bar_2 = LumpedParameters.kappa_bar_2;         % [a.u.]                    kappa_bar_2 = c_x * sqrt(kappa * kappa_2)
k_bar_0 = LumpedParameters.k_bar_0;                 % [Dimensionless]           k_bar_0 = k_0 / k_1
k_bar_2 = LumpedParameters.k_bar_2;                 % [Dimensionless]           k_bar_2 = k_2 / k_1
Delta_1 = LumpedParameters.Delta_1;                 % [1/nM]                    Delta_1 = k_3 / (kappa_3*c_bar)
Delta_2 = LumpedParameters.Delta_2;                 % [1/a.u./nM]            	Delta_2 = (k_prime_3*gamma) / (kappa_prime_3^2*c_bar*c*c_x)
Delta_prime_1 = LumpedParameters.Delta_prime_1;  	% [1/a.u.]               	Delta_prime_1 = gamma / (kappa_3*c*c_x)
Delta_prime_2 = LumpedParameters.Delta_prime_2;   	% [1/a.u.^2]               	Delta_prime_2 = gamma^2 / (kappa_prime_3^2*c^2*c_x^2)

%% Compute Fixed Points
N = size(Input, 1);
X_tilde_1 = zeros(N,1);
X_tilde_2 = zeros(N,1);
A_tilde = zeros(N,1);
Z_bar_1 = zeros(N,1);
Z_bar_2 = zeros(N,1);
for i = 1 : N
    % Inputs & Disturbance
    G_1 = Input(i,1);
    G_2 = Input(i,2);
    D = Input(i,3);
    switch Type(i)
        case 0 % Open Loop
         	% Solution of X_tilde_1-Polynomial
           	Coefficients = X_tilde_1Coefficients_OL(D,Delta_1,Delta_2,Delta_prime_1,Delta_prime_2,G_1,G_2,delta_bar,eta_bar,gamma_bar,k_bar_0);       
          	ROOTS = roots(Coefficients);
          	X_1 = ROOTS(imag(ROOTS)==0 & ROOTS>0);
            X_2 = (-1 + sqrt(1 + 8*X_1/kappa_bar)) / (4/kappa_bar);
            A = X_2.^2 / kappa_bar;
            Z_1 = gamma_bar * (X_1 + D * (Delta_1*X_1 + Delta_2*X_1.^2) ./ (1 + Delta_prime_1*X_1 + Delta_prime_2*X_1.^2));
            Z_2 = G_1 ./ (eta_bar * Z_1) - delta_bar / eta_bar;
            if length(X_1) == 1
                X_tilde_1(i) = X_1;
                X_tilde_2(i) = X_2;
                A_tilde(i) = A;
                Z_bar_1(i) = Z_1;
                Z_bar_2(i) = Z_2;
            elseif isempty(X_1)
                keyboard
                X_tilde_1(i) = NaN;
                X_tilde_2(i) = NaN;
                A_tilde(i) = NaN;
                Z_bar_1(i) = NaN;
                Z_bar_2(i) = NaN;
            else 
                Equation = k_bar_0*G_2 - eta_bar * Z_1 .* Z_2 - delta_bar * Z_2;
                [~, Index] = min(abs(Equation));
                X_tilde_1(i) = X_1(Index);
                X_tilde_2(i) = X_2(Index);
                A_tilde(i) = A(Index);
                Z_bar_1(i) = Z_1(Index);
                Z_bar_2(i) = Z_2(Index);
            end
        case 1 % Closed Loop Finite Eta
         	% Solution of X_tilde_2-Polynomial
           	Coefficients = X_tilde_2Coefficients_CL(D,Delta_1,Delta_2,Delta_prime_1,Delta_prime_2,G_1,G_2,delta_bar,eta_bar,gamma_bar,k_bar_0,k_bar_2,kappa_bar,kappa_bar_2);       
          	ROOTS = roots(Coefficients);
          	X_2 = ROOTS(imag(ROOTS)==0 & ROOTS>0);
            A = X_2.^2/kappa_bar;
            X_1 = (X_2 + 2*A);
            Delta = X_1 + D * (Delta_1*X_1 + Delta_2*X_1.^2) ./ (1 + Delta_prime_1*X_1 + Delta_prime_2*X_1.^2);
            Z_1 = gamma_bar * Delta;
            Z_2 = G_1 ./ (eta_bar*Z_1) - delta_bar / eta_bar;
            if length(X_2) == 1
                X_tilde_1(i) = X_1;
                X_tilde_2(i) = X_2;
                A_tilde(i) = A;
                Z_bar_1(i) = Z_1;
                Z_bar_2(i) = Z_2;
            elseif isempty(X_2)
                keyboard
                X_tilde_1(i) = NaN;
                X_tilde_2(i) = NaN;
                A_tilde(i) = NaN;
                Z_bar_1(i) = NaN;
                Z_bar_2(i) = NaN;
            else 
                Equation = (k_bar_0 + (k_bar_2 - k_bar_0) * (X_2/kappa_bar_2).^2 ./ (1 + (X_2/kappa_bar_2).^2)) * G_2 - eta_bar * Z_1 .* Z_2 - delta_bar * Z_2;
                [~, Index] = min(abs(Equation));
                X_tilde_1(i) = X_1(Index);
                X_tilde_2(i) = X_2(Index);
                A_tilde(i) = A(Index);
                Z_bar_1(i) = Z_1(Index);
                Z_bar_2(i) = Z_2(Index);
            end
    end
end



end

