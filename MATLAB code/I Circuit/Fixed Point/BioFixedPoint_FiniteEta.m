function [X_bar_1, X_bar_2, A_bar, Z_bar_1, Z_bar_2] = BioFixedPoint_FiniteEta(Input, BioParameters, Type)
%% Extract Bio Parameters
gamma = BioParameters.gamma;                        % [1/min]
delta = BioParameters.delta;                        % [1/min]
eta = BioParameters.eta;                            % [1/nM/min]
k = BioParameters.k;                                % [1/min]
c = BioParameters.c;                                % [1/min]
a = BioParameters.a;                                % [1/nM/min]
d = BioParameters.d;                                % [1/min]
k_0 = BioParameters.k_0;                            % [(nM/pmol)/min]
k_1 = BioParameters.k_1;                            % [(nM/pmol)/min]
k_2 = BioParameters.k_2;                            % [(nM/pmol)/min]
k_3 = BioParameters.k_3;                            % [1/min]
k_prime_3 = BioParameters.k_prime_3;                % [1/min]
kappa_2 = BioParameters.kappa_2;                    % [nM]
kappa_3 = BioParameters.kappa_3;                    % [nM]
kappa_prime_3 = BioParameters.kappa_prime_3;        % [nM]
c_x = BioParameters.c_x;                            % [a.u./nM]
c_bar = c + gamma;
kappa = (d + gamma) / a;

%% Compute Fixed Points
N = size(Input, 1);
X_bar_1 = zeros(N,1);
X_bar_2 = zeros(N,1);
A_bar = zeros(N,1);
Z_bar_1 = zeros(N,1);
Z_bar_2 = zeros(N,1);
for i = 1 : N
    % Inputs & Disturbance
    G_1 = Input(i,1);
    G_2 = Input(i,2);
    D = Input(i,3);
    switch Type(i)
        case 0 % Open Loop
         	% Solution of X_bar_1-Polynomial
           	Coefficients = X_bar_1Coefficients_OL(D,G_1,G_2,c,delta,eta,gamma,k,k_0,k_1,k_3,k_prime_3,kappa_3,kappa_prime_3);       
          	ROOTS = roots(Coefficients);
          	X_1 = ROOTS(imag(ROOTS)==0 & ROOTS>0);
            X_2 = (-1 + sqrt(1 + 8*c*X_1/kappa/gamma)) / (4/kappa);
            A = X_2.^2 / kappa;
            Z_1 = (1/k) * ((c + gamma) * X_1 + D * (k_3*X_1/kappa_3 + k_prime_3*X_1^2/kappa_prime_3^2) / (1 + X_1/kappa_3 + X_1^2/kappa_prime_3^2));
            Z_2 = (k_1*G_1) / (eta*Z_1) - delta / eta;
            if length(X_1) == 1
                X_bar_1(i) = X_1;
                X_bar_2(i) = X_2;
                A_bar(i) = A;
                Z_bar_1(i) = Z_1;
                Z_bar_2(i) = Z_2;
            else 
                keyboard
            end
        case 1 % Closed Loop Finite Eta
         	% Solution of X_bar_2-Polynomial
           	Coefficients = X_bar_2Coefficients_CL(D,G_1,G_2,c,delta,eta,gamma,k,k_0,k_1,k_2,k_3,k_prime_3,kappa,kappa_2,kappa_3,kappa_prime_3);       
          	ROOTS = roots(Coefficients);
          	X_2 = ROOTS(imag(ROOTS)==0 & ROOTS>0);
            A = X_2.^2/kappa;
            X_1 = (gamma/c) * (X_2 + 2*A);
            Delta = (c + gamma) * X_1 + D * (k_3*X_1/kappa_3 + k_prime_3*X_1.^2/kappa_prime_3^2) ./ (1 + X_1/kappa_3 + X_1.^2/kappa_prime_3^2);
            Z_1 = (1/k) * Delta;
            Z_2 = (k_1*G_1) ./ (eta*Z_1) - delta / eta;
            if length(X_2) == 1
                X_bar_1(i) = X_1;
                X_bar_2(i) = X_2;
                A_bar(i) = A;
                Z_bar_1(i) = Z_1;
                Z_bar_2(i) = Z_2;
            elseif isempty(X_2)
                keyboard
                X_bar_1(i) = NaN;
                X_bar_2(i) = NaN;
                A_bar(i) = NaN;
                Z_bar_1(i) = NaN;
                Z_bar_2(i) = NaN;
            else 
                keyboard
                Equation = (k_0 + (k_2 - k_1) * (A/kappa_2) ./ (1 + A/kappa_2)) * G_2 - eta * Z_1 .* Z_2 - delta * Z_2;
                [~, Index] = min(abs(Equation));
                X_bar_1(i) = X_1(Index);
                X_bar_2(i) = X_2(Index);
                A_bar(i) = A(Index);
                Z_bar_1(i) = Z_1(Index);
                Z_bar_2(i) = Z_2(Index);
            end
    end
end



end

