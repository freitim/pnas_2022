function [] = Visualize_Data(DataTable)
%% Extract Inputs and Measurements
N = size(DataTable,1) / 3;
Input_Data = zeros(N,4);
Measurement1_Data = zeros(N,1);
Measurement2_Data = zeros(N,1);
Measurement3_Data = zeros(N,1);
for i = 1 : N
    Input_Data(i,1) = DataTable{3*(i-1)+1,3};                           % D_1T [pmol]
    Input_Data(i,2) = DataTable{3*(i-1)+1,4};                           % D_2T [pmol]
    Input_Data(i,3) = DataTable{3*(i-1)+1,5} * 1e3;                     % G_T [nM]
    if (DataTable{3*(i-1)+1,1} == 'Open')
        Input_Data(i,4) = 0;
    elseif (DataTable{3*(i-1)+1,1} == 'Closed')
        Input_Data(i,4) = 1;
    else
        error('Only Open and Closed Loop are allowed.');
    end
    Measurement1_Data(i,1) = DataTable{3*(i-1)+1,7};                    % Green [a.u.]
    Measurement2_Data(i,1) = DataTable{3*(i-1)+2,7};                    % Green [a.u.]
    Measurement3_Data(i,1) = DataTable{3*(i-1)+3,7};                    % Green [a.u.]
end

%% Sort with and without Disturbance
[Input_Data, Indeces] = sortrows(Input_Data, [4, 3, 1]);
Measurement1_Data = Measurement1_Data(Indeces);
Measurement2_Data = Measurement2_Data(Indeces);
Measurement3_Data = Measurement3_Data(Indeces);

%% Compute the Means
Measurement_Data = mean([Measurement1_Data, Measurement2_Data, Measurement3_Data], 2);

%% General Figure Settings 
Colors = lines(10);
MyGreen = Colors(5,:); MyRed = Colors(2,:); MyYellow = Colors(3,:);
DrugColor = MyRed ;

SS = 4; % Screen Scale
Figure_Width = 12 * SS;
Figure_Height = 4 * SS;
FontSize = 7 * SS;
FontSize_Small = 4 * SS;
LineWidth_Thin = 0.01 * SS;
LineWidth = 1 * SS;
LineWidth_Thick = 1 * SS;
MarkerSize = 2.5 * SS;
BarWidth = 0.75;
BarWidth_Small = BarWidth / 1.25;
Transparency = 0.5;
DotSpace = 0.2;

%% Counting Parameters
N_OL = find(Input_Data(:,4) == 1, 1) - 1;
N_OL_ND = find(Input_Data(:,3) > 0, 1) - 1;
N_CL_ND = find(Input_Data(N_OL+1:end,3) > 0, 1) - 1;
ratio_OL = Input_Data(1:N_OL_ND,1) ./ Input_Data(1:N_OL_ND,2);
ratio_CL = Input_Data(N_OL+1:N_CL_ND+N_OL,1) ./ Input_Data(N_OL+1:N_CL_ND+N_OL,2);

%% Compute Steady-State Error
SSError_OL = round(100 * abs(Measurement_Data(N_OL_ND+1:N_OL) - Measurement_Data(1:N_OL_ND)) ./ Measurement_Data(1:N_OL_ND));
SSError_CL = round(100 * abs(Measurement_Data(N_CL_ND+N_OL+1:end) - Measurement_Data(N_OL+1:N_CL_ND+N_OL)) ./ Measurement_Data(N_OL+1:N_CL_ND+N_OL));

%% Figure 1 Settings
Handle_Figure1 = figure();
    Handle_Figure1.Color = [1 1 1];
    Handle_Figure1.PaperUnits = 'centimeters';
    Handle_Figure1.Units = 'centimeters';
    Handle_Figure1.Position = [1, 20, Figure_Width, Figure_Height];
    Handle_Figure1.PaperPositionMode = 'auto';
    Handle_Figure1.PaperSize = [Handle_Figure1.PaperPosition(3), Handle_Figure1.PaperPosition(4)];
figure(Handle_Figure1);

Handle_Axis1 = subplot(1,2,1);
    Handle_Axis1.Position = [0.075, 0.17, 0.45, 0.66];
    hold(Handle_Axis1, 'on');
    Handle_Axis1.Box = 'on';
    Handle_Axis1.FontSize = FontSize;
    hold(Handle_Axis1, 'on');
    grid(Handle_Axis1, 'on');
    Handle_Axis1.XMinorGrid = 'off';
    Handle_Axis1.YMinorGrid = 'off';
    Handle_Axis1.XLabel.String = '$G_1/G_2$';
    Handle_Axis1.XLabel.Interpreter = 'latex';
    Handle_Axis1.YLabel.String = 'Fluorescence [a.u.]';
    Handle_Axis1.Title.String = 'Open Loop';
    Handle_Axis1.XLim = [0, N_OL_ND + 1];
    Handle_Axis1.XTick = 1 : N_OL_ND;
    Handle_Axis1.XTickLabel = cellstr(strtrim(rats(ratio_OL)));
	Handle_Axis1.YLim = [0, 9.5e4];
%   	Handle_Axis1.YLim = [2e3, 1e6];
%     Handle_Axis1.YScale = 'log';
    
Handle_Axis2 = subplot(1,2,2);
    Handle_Axis2.Position = [0.54, 0.17, 0.45, 0.66];
    hold(Handle_Axis2, 'on');
    Handle_Axis2.Box = 'on';
    Handle_Axis2.FontSize = FontSize;
    hold(Handle_Axis2, 'on');
    grid(Handle_Axis2, 'on');
    Handle_Axis2.XMinorGrid = 'off';
    Handle_Axis2.YMinorGrid = 'off';
    Handle_Axis2.XLabel.String = '$G_1/G_2$';
    Handle_Axis2.XLabel.Interpreter = 'latex';
    Handle_Axis2.Title.String = 'Closed Loop';
    Handle_Axis2.XLim = [0, N_CL_ND + 1];
    Handle_Axis2.YLim = Handle_Axis1.YLim;
    Handle_Axis2.XTick = 1 : N_CL_ND;
    Handle_Axis2.XTickLabel = cellstr(strtrim(rats(ratio_CL)));
    Handle_Axis2.YTickLabel = [];
%     Handle_Axis2.YScale = 'log';
    
%% Plotting Figure 1
% ------------------------
% Open Loop No Disturbance
% ------------------------
% Green Bars
Handle_Bar11 = bar(Handle_Axis1, 1:N_OL_ND, Measurement_Data(1:N_OL_ND), BarWidth);
    Handle_Bar11.FaceColor = MyGreen;
    Handle_Bar11.FaceAlpha = Transparency;
% Green Data Points
plot(Handle_Axis1, (1:N_OL_ND) - DotSpace, Measurement1_Data(1:N_OL_ND), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis1, 1:N_OL_ND, Measurement2_Data(1:N_OL_ND), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis1, (1:N_OL_ND) + DotSpace, Measurement3_Data(1:N_OL_ND), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', MyGreen, 'Color', 'k');

% --------------------------
% Open Loop With Disturbance
% --------------------------
% Green Bars
Handle_Bar12 = bar(Handle_Axis1, 1:N_OL-N_OL_ND, Measurement_Data(N_OL_ND+1:N_OL), BarWidth_Small); 
    Handle_Bar12.FaceColor = DrugColor;
    Handle_Bar12.FaceAlpha = Transparency;
% Green Data Points
plot(Handle_Axis1, (1:N_OL-N_OL_ND) - DotSpace, Measurement1_Data(N_OL_ND+1:N_OL), ... 
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis1, 1:N_OL-N_OL_ND, Measurement2_Data(N_OL_ND+1:N_OL), ... 
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis1, (1:N_OL-N_OL_ND) + DotSpace, Measurement3_Data(N_OL_ND+1:N_OL), ... 
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', DrugColor, 'Color', 'k');

% --------------------------
% Closed Loop No Disturbance
% --------------------------
% Green Bars
Handle_Bar21 = bar(Handle_Axis2, 1:N_CL_ND, Measurement_Data(N_OL+1:N_OL+N_CL_ND), BarWidth);
    Handle_Bar21.FaceColor = MyGreen;
    Handle_Bar21.FaceAlpha = Transparency;
% Green Data Points
plot(Handle_Axis2, (1:N_CL_ND) - DotSpace, Measurement1_Data(N_OL+1:N_OL+N_CL_ND), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis2, 1:N_CL_ND, Measurement2_Data(N_OL+1:N_OL+N_CL_ND), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis2, (1:N_CL_ND) + DotSpace, Measurement3_Data(N_OL+1:N_OL+N_CL_ND), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', MyGreen, 'Color', 'k');

% ----------------------------
% Closed Loop With Disturbance
% ----------------------------
% Green Bars
Handle_Bar22 = bar(Handle_Axis2, 1:size(Input_Data,1) - N_OL - N_CL_ND, Measurement_Data(N_OL+N_CL_ND+1:end), BarWidth_Small);
    Handle_Bar22.FaceColor = DrugColor;
    Handle_Bar22.FaceAlpha = Transparency;
% Green Data Points
plot(Handle_Axis2, (1:size(Input_Data,1) - N_OL - N_CL_ND) - DotSpace, Measurement1_Data(N_OL+N_CL_ND+1:end), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis2, 1:size(Input_Data,1) - N_OL - N_CL_ND, Measurement2_Data(N_OL+N_CL_ND+1:end), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis2, (1:size(Input_Data,1) - N_OL - N_CL_ND) + DotSpace, Measurement3_Data(N_OL+N_CL_ND+1:end), ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', DrugColor, 'Color', 'k');

% ---------------------
% Disturbance Rejection
% ---------------------
% text(Handle_Axis1, 1:N_OL_ND, 1.01*max([Measurement1_Data(1:N_OL_ND), Measurement2_Data(1:N_OL_ND), Measurement3_Data(1:N_OL_ND), Measurement1_Data(N_OL_ND+1:N_OL), Measurement2_Data(N_OL_ND+1:N_OL,1), Measurement3_Data(N_OL_ND+1:N_OL)], [], 2) , num2str(SSError_OL), 'vert', 'bottom', 'horiz','center', 'FontSize', FontSize/2, 'Color', MyRed);
% text(Handle_Axis2, 1:N_CL_ND, 1.01*max([Measurement1_Data(N_OL+1:N_OL+N_CL_ND), Measurement2_Data(N_OL+1:N_OL+N_CL_ND), Measurement3_Data(N_OL+1:N_OL+N_CL_ND), Measurement1_Data(N_OL+N_CL_ND+1:end), Measurement2_Data(N_OL+N_CL_ND+1:end), Measurement3_Data(N_OL+N_CL_ND+1:end)], [], 2), num2str(SSError_CL), 'vert', 'bottom', 'horiz', 'center', 'FontSize', FontSize/2, 'Color', MyRed);

% -------
% Legends
% -------
Handle_White = plot(Handle_Axis2, -1, -1, 'Color', [1, 1, 1]);
Handle_DataPoint = plot(Handle_Axis2, -1, -1, 'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', [0.5, 0.5, 0.5], 'Color', 'k'); 
Handle_Bar = bar(Handle_Axis2, -1, -1, 10);
    Handle_Bar.FaceColor = [0.5, 0.5, 0.5];
    Handle_Bar.FaceAlpha = Transparency;
Handle_Legend1 = legend(Handle_Axis2, [Handle_DataPoint, Handle_White, Handle_Bar, Handle_White], 'Measurement', '', 'Average', '');
    Handle_Legend1.Orientation = 'Horizontal';
    Handle_Legend1.EdgeColor = 'none';
    Handle_Legend1.Position = [0.065, 0.935, 0.45, 0.05];
    Handle_Legend1.Color = 'none';
Handle_Box = annotation('rectangle', [Handle_Legend1.Position(1) + Handle_Legend1.Position(3) + 0.01, Handle_Legend1.Position(2), Handle_Legend1.Position(3:4) + [0.02, 0]]);
Handle_Color1 = annotation('rectangle');
    Handle_Color1.Position = [Handle_Box.Position(1) + 0.05, Handle_Box.Position(2), 0.01*1.5, 2*0.015*1.5];
    Handle_Color1.EdgeColor = 'none';
    Handle_Color1.FaceColor = MyGreen;
Handle_Text1 = annotation('textbox');
    Handle_Text1.Position = Handle_Color1.Position + [0.015, 0, 0.2, 0];
    Handle_Text1.EdgeColor = 'none';
    Handle_Text1.String = 'Without Disturbance';
    Handle_Text1.FontSize = Handle_Legend1.FontSize;
    Handle_Text1.FontName = Handle_Legend1.FontName;
    Handle_Text1.VerticalAlignment = 'Middle';
Handle_Color3 = annotation('rectangle');
    Handle_Color3.Position = [Handle_Text1.Position(1) + Handle_Text1.Position(3), Handle_Color1.Position(2:4)];
    Handle_Color3.EdgeColor = 'none';
    Handle_Color3.FaceColor = DrugColor;
Handle_Text2 = annotation('textbox');
    Handle_Text2.Position = Handle_Color3.Position + [0.015, 0, 0.2, 0];
    Handle_Text2.EdgeColor = 'none';
    Handle_Text2.String = 'With Disturbance';
    Handle_Text2.FontSize = Handle_Legend1.FontSize;
    Handle_Text2.FontName = Handle_Legend1.FontName;
    Handle_Text2.VerticalAlignment = 'Middle';
% Handle_Text3 = annotation('textbox');
%     Handle_Text3.Position = Handle_Color3.Position + [0.2, 0, 0.2, 0];
%     Handle_Text3.EdgeColor = 'none';
%     Handle_Text3.Color = MyRed;
%     Handle_Text3.String = '% Steady-State Error';
%     Handle_Text3.FontSize = Handle_Legend1.FontSize/1.5;
%     Handle_Text3.FontName = Handle_Legend1.FontName;
%     Handle_Text3.VerticalAlignment = 'Middle';
Handle_Box.Position = [0.075, 0.92, 0.915, 0.08];

end

