%% Clear Workspace
close all;
clear;
clc;
Colors = lines(10);
MyBlue = Colors(1,:); MyRed = Colors(2,:); MyGreen = Colors(5,:);
SS = 4; % Screen Scale
Save_Flag = 0;

%% Inputs & Disturbances
G_1 = 1.3;                                                  % [pmol]
G_2 = 1.5;                                                  % [pmol]
D = 0.3;                                                 	% [nM]
Input = [G_1, G_2, D];
Type = 1;                                                   % 0 for Open Loop, 1 for Closed Loop

%% Biological Parameters
BioParameters = Class_BioParameters();
BioParameters.gamma = 0.14;                                 % [1/hr]
BioParameters.delta = 0.1;                                 % [1/hr]
BioParameters.eta = 10.355;                                 % [1/nM/hr]
BioParameters.k = 100.32;                                     % [1/hr]
BioParameters.c = 1;                                       % [1/hr]
BioParameters.a  = 8.3;                                     % [1/nM/hr]
BioParameters.d = 9.3;                                      % [1/hr]
BioParameters.k_0 = 0.124;                                  % [(nM/pmol)/hr]
BioParameters.k_1 = 30.532;                                  % [(nM/pmol)/hr]
BioParameters.k_2 = 5.34;                                   % [(nM/pmol)/hr]
BioParameters.k_3 = 1.34;                                   % [1/hr]
BioParameters.k_prime_3 = 1.22;                             % [1/hr]
BioParameters.kappa_2 = 5.23 / (Type + 1e-10);              % [nM]
BioParameters.kappa_3 = 3.253;                              % [nM]
BioParameters.kappa_prime_3 = 4.33;                         % [nM]
BioParameters.c_x = 94;                                     % [a.u./nM]

%% Time Horizon and Initial Conditions
tf = 100;                                                   % [hr]
N = 1000;
IC = [0; 0; 0; 0; 0];

%% Simulation
Solver = 'ODE23s';
NetworkParameters = struct(BioParameters);
NetworkParameters.G_1 = Input(1);
NetworkParameters.G_2 = Input(2);
NetworkParameters.D = Input(3);
[time_vector, Solution] = DSA(StoichiometryMatrix(), @PropensityFunction, NetworkParameters, IC, tf, N, Solver);
X_1 = Solution(1,:); X_2 = Solution(2,:); A = Solution(3,:); 
Z_1 = Solution(4,:); Z_2 = Solution(5,:);

%% Fixed Point
[X_bar_1, X_bar_2, A_bar, Z_bar_1, Z_bar_2] = BioFixedPoint_FiniteEta(Input, BioParameters, Type);
LumpedParameters = Bio2Lumped(BioParameters);
[X_tilde_1, X_tilde_2, A_tilde, Z_bar_1_tilde, Z_bar_2_tilde] = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);

%% Figure Settings
Figure_Width = 7 * SS;
Figure_Height = 8 * SS;
FontSize = 7 * SS;
FontSize_Small = 4 * SS;
LineWidth = 0.5 * SS;
LineWidth_Thick = 1 * SS;
MarkerSize = 5 * SS;
% Figure 1
Handle_Figure1 = figure();
    Handle_Figure1.Color = [1 1 1];
    Handle_Figure1.PaperUnits = 'centimeters';
    Handle_Figure1.Units = 'centimeters';
    Handle_Figure1.Position = [1, 20, Figure_Width, Figure_Height];
    Handle_Figure1.PaperPositionMode = 'auto';
    Handle_Figure1.PaperSize = [Handle_Figure1.PaperPosition(3), Handle_Figure1.PaperPosition(4)];
Handle_Axis1 = subplot(2,3,1);
    Handle_Axis1.Box = 'on';
    Handle_Axis1.FontSize = FontSize;
    hold(Handle_Axis1, 'on');
    grid(Handle_Axis1, 'on');
    Handle_Axis1.XMinorGrid = 'on';
    Handle_Axis1.YMinorGrid = 'on';
    Handle_Axis1.XLabel.String = '$t$';
    Handle_Axis1.YLabel.String = '$Z_1$';
    Handle_Axis1.XLabel.Interpreter = 'latex';
    Handle_Axis1.YLabel.Interpreter = 'latex';
Handle_Axis2 = subplot(2,3,2);
    Handle_Axis2.Box = 'on';
    Handle_Axis2.FontSize = FontSize;
    hold(Handle_Axis2, 'on');
    grid(Handle_Axis2, 'on');
    Handle_Axis2.XMinorGrid = 'on';
    Handle_Axis2.YMinorGrid = 'on';
    Handle_Axis2.XLabel.String = '$t$';
    Handle_Axis2.YLabel.String = '$Z_2$';
    Handle_Axis2.XLabel.Interpreter = 'latex';
    Handle_Axis2.YLabel.Interpreter = 'latex';
Handle_Axis3 = subplot(2,3,4);
    Handle_Axis3.Box = 'on';
    Handle_Axis3.FontSize = FontSize;
    hold(Handle_Axis3, 'on');
    grid(Handle_Axis3, 'on');
    Handle_Axis3.XMinorGrid = 'on';
    Handle_Axis3.YMinorGrid = 'on';
    Handle_Axis3.XLabel.String = '$t$';
    Handle_Axis3.YLabel.String = '$X_1$';
    Handle_Axis3.XLabel.Interpreter = 'latex';
    Handle_Axis3.YLabel.Interpreter = 'latex';
Handle_Axis4 = subplot(2,3,5);
    Handle_Axis4.Box = 'on';
    Handle_Axis4.FontSize = FontSize;
    hold(Handle_Axis4, 'on');
    grid(Handle_Axis4, 'on');
    Handle_Axis4.XMinorGrid = 'on';
    Handle_Axis4.YMinorGrid = 'on';
    Handle_Axis4.XLabel.String = '$t$';
    Handle_Axis4.YLabel.String = '$X_2$';
    Handle_Axis4.XLabel.Interpreter = 'latex';
    Handle_Axis4.YLabel.Interpreter = 'latex';
Handle_Axis5 = subplot(2,3,6);
    Handle_Axis5.Box = 'on';
    Handle_Axis5.FontSize = FontSize;
    hold(Handle_Axis5, 'on');
    grid(Handle_Axis5, 'on');
    Handle_Axis5.XMinorGrid = 'on';
    Handle_Axis5.YMinorGrid = 'on';
    Handle_Axis5.XLabel.String = '$t$';
    Handle_Axis5.YLabel.String = '$A$';
    Handle_Axis5.XLabel.Interpreter = 'latex';
    Handle_Axis5.YLabel.Interpreter = 'latex';

%% Response
plot(Handle_Axis1, time_vector, Z_1, 'Color', MyBlue, 'LineWidth', LineWidth);
plot(Handle_Axis2, time_vector, Z_2, 'Color', MyBlue, 'LineWidth', LineWidth);
plot(Handle_Axis3, time_vector, X_1, 'Color', MyGreen, 'LineWidth', LineWidth);
plot(Handle_Axis4, time_vector, X_2, 'Color', MyGreen, 'LineWidth', LineWidth);
plot(Handle_Axis5, time_vector, A, 'Color', MyGreen, 'LineWidth', LineWidth);


%% Steady State
plot(Handle_Axis1, time_vector(end), Z_bar_1, 'Color', MyBlue, 'Marker', 'o', 'MarkerSize', MarkerSize);
plot(Handle_Axis2, time_vector(end), Z_bar_2, 'Color', MyBlue, 'Marker', 'o', 'MarkerSize', MarkerSize);
plot(Handle_Axis3, time_vector(end), X_bar_1, 'Color', MyGreen, 'Marker', 'o', 'MarkerSize', MarkerSize);
plot(Handle_Axis4, time_vector(end), X_bar_2, 'Color', MyGreen, 'Marker', 'o', 'MarkerSize', MarkerSize);
plot(Handle_Axis5, time_vector(end), A_bar, 'Color', MyGreen, 'Marker', 'o', 'MarkerSize', MarkerSize);

plot(Handle_Axis1, time_vector(end), Z_bar_1_tilde, 'Color', MyBlue, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis2, time_vector(end), Z_bar_2_tilde, 'Color', MyBlue, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis3, time_vector(end), X_tilde_1 * BioParameters.gamma / BioParameters.c / BioParameters.c_x, 'Color', MyGreen, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis4, time_vector(end), X_tilde_2 / BioParameters.c_x, 'Color', MyGreen, 'Marker', 'x', 'MarkerSize', MarkerSize);
plot(Handle_Axis5, time_vector(end), A_tilde / BioParameters.c_x, 'Color', MyGreen, 'Marker', 'x', 'MarkerSize', MarkerSize);

%% Saving
if Save_Flag == 1
    print(Handle_Figure1, 'Response', '-dpdf');
end
