function S = StoichiometryMatrix()
% Stoichiometry Matrix for the I-Circuit
%   Species:        X = [X1; X2; A; Z1; Z2]
%   Reactions:      R1:     X1          -->     phi             [gamma*X_1 + D * (k_3*X_1/kappa_3 + k_prime_3*(X_1/kappa_prime_3)^2) / (1 + X_1/kappa_3 + (X_1/kappa_prime_3)^2)]
%                   R2:     X2          -->     phi             [gamma*X_2]
%                   R3:     A           -->     phi             [gamma*A]
%                   R4:     X1          -->     X2              [c*X_1]
%                   R5:     X2 + X2     -->     A               [a*X_2^2]
%                   R6:     A           -->     X2 + X2         [d*A]
%                   R7:     X2          -->     X2 + Z2         [(k_0 + (k_2 - k_0) * (A/kappa_2) / (1 + (A/kappa_2)))*G_2]  
%                   R8:     Z1 + Z2     -->     phi             [eta*Z_1*Z_2]
%                   R9:     phi         -->     Z1              [k_1*G_1]
%                   R10:    Z1       	-->     Z1 + X1         [k*Z_1]
%                   R11:    Z1       	-->     phi             [delta*Z_1]
%                   R12:    Z2        	-->     phi             [delta*Z_2]

S = [-1,     0,         0,      -1,     0,      0,      0,      0,      0,      1,   	0,      0; ...
     0,      -1,        0,     	1,      -2,     2,      0,      0,      0,      0,   	0,      0; ...
     0,      0,         -1,   	0,      1,      -1,  	0,      0,      0,      0,    	0,      0; ...
     0,      0,         0,     	0,      0,      0,      0,      -1,     1,      0,    	-1,     0; ...
     0,      0,         0,    	0,      0,      0,      1,      -1,     0,      0,    	0,      -1];
end

