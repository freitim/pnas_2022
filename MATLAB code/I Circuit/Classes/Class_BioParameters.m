classdef Class_BioParameters
    properties
        gamma               % [1/hr]
        delta               % [1/hr]
        eta                 % [1/nM/hr]
        k                   % [1/hr]
        c                   % [1/hr]
        a                   % [1/nM/hr]
        d                   % [1/hr]
        k_0                 % [(nM/pmol)/hr]
        k_1                 % [(nM/pmol)/hr]
        k_2                 % [(nM/pmol)/hr]
        k_3                 % [1/hr]
        k_prime_3           % [1/hr]
        kappa_2             % [nM]
        kappa_3             % [nM]
        kappa_prime_3       % [nM]
        c_x                 % [a.u./nM]
    end
    
    methods
        function obj = Class_BioParameters(Vector)
            if nargin > 0
                obj.gamma = Vector(1);
                obj.delta = Vector(2);
                obj.eta = Vector(3);
                obj.k = Vector(4);
                obj.c = Vector(5);
                obj.a = Vector(6);
                obj.d = Vector(7);
                obj.k_0 = Vector(8);
                obj.k_1 = Vector(9);
                obj.k_2 = Vector(10);
                obj.k_3 = Vector(11);
                obj.k_prime_3 = Vector(12);
                obj.kappa_2 = Vector(13);
                obj.kappa_3 = Vector(14);
                obj.kappa_prime_3 = Vector(15);
                obj.c_x = Vector(16);
            end
        end
        
        function Vector = Vector(obj)
            Vector = [obj.gamma; obj.delta; obj.eta; ...
                obj.k; obj.c; obj.a; obj.d; ...
                obj.k_0; obj.k_1; obj.k_2; obj.k_3; obj.k_prime_3; ...
                obj.kappa_2; obj.kappa_3; obj.kappa_prime_3; ...
                obj.c_x];
        end     
    end
end

