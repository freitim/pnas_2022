function [Input_Data, Measurement_Data, Measurement_SEM, ...
          LumpedParameters_IG,  LumpedParameters_LB,  LumpedParameters_UB,  ...
          A, B, Aeq, Beq, Options] = OptimizationSettings_CL_ND(DataFile)
      
%% Generate Data
Data = load(DataFile);
[Input_Data, Measurement_Data, Measurement_SEM] = GenerateData(Data.DataTable);
clear Data;

%% Load Open Loop Parameters and Set Initial Guess
Loaded = load('Results_OL_ND');
LumpedParameters_IG = Loaded.LumpedParameters;
  	  
LumpedParameters_IG.kappa_bar = 1e3;            % [a.u.]                    kappa_bar = c_x * kappa
LumpedParameters_IG.kappa_bar_2 = 1e3;          % [a.u.]                    kappa_bar_2 = c_x * sqrt(kappa * kappa_2)
LumpedParameters_IG.k_bar_2 = 1;                % [Dimensionless]           k_bar_2 = k_2 / k_1

Fraction_LB = 0.99;
Fraction_UB = 100;
[LumpedParameters_LB, LumpedParameters_UB] = Generate_UBLB_CL_ND(LumpedParameters_IG, Fraction_LB, Fraction_UB);

%% Linear Constraints
A = []; B = []; Aeq = []; Beq = [];

%% Solver Options
Options = optimoptions(@fmincon, 'Display','iter', 'Algorithm', 'sqp', 'TolFun', 1e-14, 'MaxFunEvals', 1e4, 'TolCon',1e-14, 'TolX',1e-14, 'MaxIter', 1e4);
% Options = optimoptions(@fmincon, 'Display','iter', 'Algorithm', 'sqp', 'MaxFunEvals', 1000);

end

