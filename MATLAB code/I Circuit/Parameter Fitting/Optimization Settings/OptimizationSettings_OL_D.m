function [Input_Data, Measurement_Data, Measurement_SEM, ...
          LumpedParameters_IG,  LumpedParameters_LB,  LumpedParameters_UB,  ...
          A, B, Aeq, Beq, Options] = OptimizationSettings_OL_D(DataFile)
      
%% Generate Data
Data = load(DataFile);
[Input_Data, Measurement_Data, Measurement_SEM] = GenerateData(Data.DataTable);
clear Data;

%% Load Open Loop Parameters and Set Initial Guess
Loaded = load('Results_CL_ND');
LumpedParameters_IG = Loaded.LumpedParameters;
  	  
LumpedParameters_IG.Delta_1 = 1e-10;                                    % [1/nM]                    Delta_1 = k_3 / (kappa_3*c_bar)
LumpedParameters_IG.Delta_2 = 2.5e-06;                                  % [1/a.u./nM]            	Delta_2 = (k_prime_3*gamma) / (kappa_prime_3^2*c_bar^2*c_G)
LumpedParameters_IG.Delta_prime_1 = 1e-10;                              % [1/a.u.]               	Delta_prime_1 = gamma / (kappa_3*c_bar*c_G)
LumpedParameters_IG.Delta_prime_2 = 1e-10;                              % [1/a.u.]                	Delta_prime_2 = gamma^2 / (kappa_prime_3^2*c_bar^2*c_G^2) 

Fraction_LB = 0.9999999;
Fraction_UB = 10000000;
[LumpedParameters_LB, LumpedParameters_UB] = Generate_UBLB_OL_D(LumpedParameters_IG, Fraction_LB, Fraction_UB);

%% Linear Constraints
A = []; B = []; Aeq = []; Beq = [];

%% Solver Options
Options = optimoptions(@fmincon, 'Display','iter', 'Algorithm', 'sqp', 'TolFun', 1e-14, 'MaxFunEvals', 1e4, 'TolCon',1e-14, 'TolX',1e-14, 'MaxIter', 1e4);
% Options = optimoptions(@fmincon, 'Display','iter', 'Algorithm', 'sqp', 'MaxFunEvals', 1000);

end

