function [Input_Data, Measurement_Data, Measurement_SEM, ...
          LumpedParameters_IG,  LumpedParameters_LB,  LumpedParameters_UB,  ...
          A, B, Aeq, Beq, Options] = OptimizationSettings_OL_ND(DataFile)
      
%% Generate Data
Data = load(DataFile);
[Input_Data, Measurement_Data, Measurement_SEM] = GenerateData(Data.DataTable);
clear Data;

%% Initial Guess for System & Measurement Parameters
LumpedParameters_IG = Class_LumpedParameters();
LumpedParameters_IG.gamma_bar = 1e-7;           % [1/a.u.]                  gamma_bar = (gamma/k) * ((c+gamma)/c) / c_x
LumpedParameters_IG.delta_bar = 1e-2;           % [dimensionless]           delta_bar = delta / k_1
LumpedParameters_IG.eta_bar = 1e2;              % [1/nM]                    eta_bar = eta / k_1
LumpedParameters_IG.kappa_bar = 1e3;            % [a.u.]                    kappa_bar = c_x * kappa
LumpedParameters_IG.kappa_bar_2 = 1e3;          % [a.u.]                    kappa_bar_2 = c_x * sqrt(kappa * kappa_2)
LumpedParameters_IG.k_bar_0 = 1e-8;                % [Dimensionless]           k_bar_0 = k_0 / k_1
LumpedParameters_IG.k_bar_2 = 1;                % [Dimensionless]           k_bar_2 = k_2 / k_1
LumpedParameters_IG.Delta_1 = 1e-3;           	% [1/nM]                    Delta_1 = k_3 / (kappa_3*c_bar)
LumpedParameters_IG.Delta_2 = 1e-3;           	% [1/a.u./nM]            	Delta_2 = (k_prime_3*gamma) / (kappa_prime_3^2*c_bar*c*c_x)
LumpedParameters_IG.Delta_prime_1 = 1e-3;     	% [1/a.u.]               	Delta_prime_1 = gamma / (kappa_3*c*c_x)
LumpedParameters_IG.Delta_prime_2 = 1e-2;      	% [1/a.u.^2]               	Delta_prime_2 = gamma^2 / (kappa_prime_3^2*c^2*c_x^2)

Fraction_LB = 0.99;
Fraction_UB = 100;
[LumpedParameters_LB, LumpedParameters_UB] = Generate_UBLB_OL_ND(LumpedParameters_IG, Fraction_LB, Fraction_UB);

%% Linear Constraints
A = []; B = []; Aeq = []; Beq = [];

%% Solver Options
Options = optimoptions(@fmincon, 'Display','iter', 'Algorithm', 'sqp', 'TolFun', 1e-14, 'MaxFunEvals', 1e4, 'TolCon',1e-14, 'TolX',1e-14, 'MaxIter', 1e4);
% Options = optimoptions(@fmincon, 'Display','iter', 'Algorithm', 'sqp', 'MaxFunEvals', 1000);

end

