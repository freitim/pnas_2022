function [] = Compare_FullData_Simulation_Prediction_Normalized(DataTable, LumpedParameters, N_Sim, XScale)
%% Extract Inputs and Measurements
N = size(DataTable,1) / 3;
Input_Data = zeros(N,4);
Measurement1_Data = zeros(N,1);
Measurement2_Data = zeros(N,1);
Measurement3_Data = zeros(N,1);
for i = 1 : N
    Input_Data(i,1) = DataTable{3*(i-1)+1,3};                           % G_1 [pmol]
    Input_Data(i,2) = DataTable{3*(i-1)+1,4};                           % G_2 [pmol]
    Input_Data(i,3) = DataTable{3*(i-1)+1,5} * 1e3;                     % D [nM]
    if (DataTable{3*(i-1)+1,1} == 'Open')
        Input_Data(i,4) = 0;
    elseif (DataTable{3*(i-1)+1,1} == 'Closed')
        Input_Data(i,4) = 1;
    else
        error('Only Open and Closed Loop are allowed.');
    end
    Measurement1_Data(i) = DataTable{3*(i-1)+1,7};                    % Green [a.u.]
    Measurement2_Data(i) = DataTable{3*(i-1)+2,7};                    % Green [a.u.]
    Measurement3_Data(i) = DataTable{3*(i-1)+3,7};                    % Green [a.u.]
end

%% Sort with and without Disturbance
[Input_Data, Indeces] = sortrows(Input_Data, [4, 3, 1]);
Measurement1_Data = Measurement1_Data(Indeces);
Measurement2_Data = Measurement2_Data(Indeces);
Measurement3_Data = Measurement3_Data(Indeces);

%% Compute the Means
Measurement_Data = mean([Measurement1_Data, Measurement2_Data, Measurement3_Data], 2);

%% Construct Inputs
G_1_Min = min(Input_Data(:,1));
G_1_Max = max(Input_Data(:,1)) * 1.2;
G_1 = linspace(G_1_Min, G_1_Max, N_Sim)';
G_2 = Input_Data(1,2) * ones(N_Sim,1);

%% Compute Fixed Points for the Open Loop
Type = zeros(N_Sim,1);
% Without Disturbance
D = zeros(N_Sim,1);
Input = [G_1, G_2, D];
[Measurement_OL_ND, ~, ~, ~, ~] = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);
% With Disturbance
D = Input_Data(end,3) * ones(N_Sim,1);
Input = [G_1, G_2, D];
[Measurement_OL_D, ~, ~, ~, ~] = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);

%% Compute Fixed Points for the Closed Loop
Type = ones(N_Sim,1);
% Without Disturbance
D = zeros(N_Sim,1);
Input = [G_1, G_2, D];
[Measurement_CL_ND, ~, ~, ~, ~] = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);
% With Disturbance
D = Input_Data(end,3) * ones(N_Sim,1);
Input = [G_1, G_2, D];
[Measurement_CL_D, ~, ~, ~, ~] = LumpedFixedPoint_FiniteEta(Input, LumpedParameters, Type);

%% General Figure Settings 
MyRed = [228,26,28]/255;
MyGreen = [77,175,74]/255;
DrugColor = MyRed;

SS = 4; % Screen Scale
Figure_Width = 12 * SS;
Figure_Height = 4 * SS;
FontSize = 7 * SS;
FontSize_Small = 4 * SS;
LineWidth_Thin = 0.01 * SS;
LineWidth = 1 * SS;
LineWidth_Thick = 1 * SS;
MarkerSize = 2 * SS;
BarWidth = 300;
Transparency = 0.5;
DotSpace = 0.001;

%% Figure 1 Settings
Handle_Figure1 = figure();
    Handle_Figure1.Color = [1 1 1];
    Handle_Figure1.PaperUnits = 'centimeters';
    Handle_Figure1.Units = 'centimeters';
    Handle_Figure1.Position = [1, 20, Figure_Width, Figure_Height];
    Handle_Figure1.PaperPositionMode = 'auto';
    Handle_Figure1.PaperSize = [Handle_Figure1.PaperPosition(3), Handle_Figure1.PaperPosition(4)];
figure(Handle_Figure1);

Handle_Axis1 = subplot(2,2,1);
    Handle_Axis1.Position = [0.09, 0.17, 0.4, 0.66];
    hold(Handle_Axis1, 'on');
    Handle_Axis1.Box = 'on';
    Handle_Axis1.FontSize = FontSize;
    hold(Handle_Axis1, 'on');
    grid(Handle_Axis1, 'on');
    Handle_Axis1.XMinorGrid = 'off';
    Handle_Axis1.YMinorGrid = 'off';
    Handle_Axis1.XLabel.String = '$G_1/G_2$';
    Handle_Axis1.XLabel.Interpreter = 'latex';
    Handle_Axis1.YLabel.String = {'Normalized'; 'Fluorescence'};
    Handle_Axis1.Title.String = 'Open Loop';
    Handle_Axis1.XLim = [0, 2.03];
    Handle_Axis1.YLim = [0, 1.2];
    Handle_Axis1.YTick = 0 : 0.2 : 1;
    Handle_Axis1.XScale = XScale; 
   
Handle_Axis2 = subplot(2,2,2);
    Handle_Axis2.Position = [0.53, 0.17, 0.4, 0.66];
    hold(Handle_Axis2, 'on');
    Handle_Axis2.Box = 'on';
    Handle_Axis2.FontSize = FontSize;
    hold(Handle_Axis2, 'on');
    grid(Handle_Axis2, 'on');
    Handle_Axis2.XMinorGrid = 'off';
    Handle_Axis2.YMinorGrid = 'off';
    Handle_Axis2.XLabel.String = '$G_1/G_2$';
    Handle_Axis2.XLabel.Interpreter = 'latex';
    Handle_Axis2.Title.String = 'Closed Loop';
    Handle_Axis2.XLim = [0, 2.03];
    Handle_Axis2.YLim = [0, 1.2];
    Handle_Axis2.YTickLabel = [];
  	Handle_Axis2.XScale = XScale;

%% Plotting Figure 1
N_OL = find(Input_Data(:,4) == 1, 1) - 1;
N_OL_ND = find(Input_Data(:,3) > 0, 1) - 1;
N_CL_ND = find(Input_Data(N_OL+1:end,3) > 0, 1) - 1;
Handle_Axis1.XTick = Input_Data(1:N_OL_ND,1) ./ Input_Data(1:N_OL_ND,2);
Handle_Axis1.XTickLabel = cellstr(strtrim(rats(Handle_Axis1.XTick')));
Handle_Axis2.XTick = Input_Data(N_OL+1:N_OL+N_CL_ND,1) ./ Input_Data(N_OL+1:N_OL+N_CL_ND,2);
Handle_Axis2.XTickLabel = cellstr(strtrim(rats(Handle_Axis2.XTick')));
if strcmp(XScale, 'linear')
    Handle_Axis1.XTickLabel{2} = ' ';
    Handle_Axis2.XTickLabel{2} = ' ';
end

Normalization_OL = Measurement_Data(N_OL_ND);
Normalization_CL = Measurement_Data(N_OL+N_CL_ND);
% ------------------------
% Open Loop No Disturbance
% ------------------------
% Green Simulation
plot(Handle_Axis1, G_1 ./ G_2, Measurement_OL_ND / Normalization_OL, 'LineWidth', LineWidth, 'Color', MyGreen);
% Green Diamonds (Means)
plot(Handle_Axis1, Input_Data(1:N_OL_ND,1) ./ Input_Data(1:N_OL_ND,2), Measurement_Data(1:N_OL_ND) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', 2*MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', MyGreen, 'Color', 'k');
% Green Data Points
plot(Handle_Axis1, Input_Data(1:N_OL_ND,1) ./ Input_Data(1:N_OL_ND,2) - DotSpace, Measurement1_Data(1:N_OL_ND) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis1, Input_Data(1:N_OL_ND,1) ./ Input_Data(1:N_OL_ND,2), Measurement2_Data(1:N_OL_ND) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis1, Input_Data(1:N_OL_ND,1) ./ Input_Data(1:N_OL_ND,2) + DotSpace, Measurement3_Data(1:N_OL_ND) / Normalization_OL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', MyGreen, 'Color', 'k');

% --------------------------
% Open Loop With Disturbance
% --------------------------
% Green Simulation
plot(Handle_Axis1, G_1 ./ G_2, Measurement_OL_D / Normalization_OL, 'LineWidth', LineWidth, 'Color', DrugColor, 'LineStyle', '-');
% Green Diamonds (Means)
plot(Handle_Axis1, Input_Data(N_OL_ND+1:N_OL,1) ./ Input_Data(N_OL_ND+1:N_OL,2), Measurement_Data(N_OL_ND+1:N_OL) / Normalization_OL, ... 
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', 2*MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', DrugColor, 'Color', 'k');
% Green Data Points
plot(Handle_Axis1, Input_Data(N_OL_ND+1:N_OL,1) ./ Input_Data(N_OL_ND+1:N_OL,2) - DotSpace, Measurement1_Data(N_OL_ND+1:N_OL) / Normalization_OL, ... 
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis1, Input_Data(N_OL_ND+1:N_OL,1) ./ Input_Data(N_OL_ND+1:N_OL,2), Measurement2_Data(N_OL_ND+1:N_OL) / Normalization_OL, ... 
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis1, Input_Data(N_OL_ND+1:N_OL,1) ./ Input_Data(N_OL_ND+1:N_OL,2) + DotSpace, Measurement3_Data(N_OL_ND+1:N_OL) / Normalization_OL, ... 
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', DrugColor, 'Color', 'k');

yyaxis(Handle_Axis1, 'right');
Handle_Axis1.YLim = [0, 1.2*Measurement_Data(N_OL_ND)];
Handle_Axis1.YAxis(2).Color = MyGreen;

% --------------------------
% Closed Loop No Disturbance
% --------------------------
% Green Simulation
plot(Handle_Axis2, G_1 ./ G_2, Measurement_CL_ND / Normalization_CL, 'LineWidth', LineWidth, 'Color', MyGreen);
% Green Diamonds (Means)
plot(Handle_Axis2, Input_Data(N_OL+1:N_OL+N_CL_ND,1) ./ Input_Data(N_OL+1:N_OL+N_CL_ND,2), Measurement_Data(N_OL+1:N_OL+N_CL_ND) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', 2.35*MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', MyGreen, 'Color', 'k');
% Green Data Points
plot(Handle_Axis2, Input_Data(N_OL+1:N_OL+N_CL_ND,1) ./ Input_Data(N_OL+1:N_OL+N_CL_ND,2) - DotSpace, Measurement1_Data(N_OL+1:N_OL+N_CL_ND) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis2, Input_Data(N_OL+1:N_OL+N_CL_ND,1) ./ Input_Data(N_OL+1:N_OL+N_CL_ND,2), Measurement2_Data(N_OL+1:N_OL+N_CL_ND) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', MyGreen, 'Color', 'k');
plot(Handle_Axis2, Input_Data(N_OL+1:N_OL+N_CL_ND,1) ./ Input_Data(N_OL+1:N_OL+N_CL_ND,2) + DotSpace, Measurement3_Data(N_OL+1:N_OL+N_CL_ND) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', MyGreen, 'Color', 'k');

% ----------------------------
% Closed Loop With Disturbance
% ----------------------------
% Green Diamonds (Means)
plot(Handle_Axis2, Input_Data(N_OL+N_CL_ND+1:end,1) ./ Input_Data(N_OL+N_CL_ND+1:end,2), Measurement_Data(N_OL+N_CL_ND+1:end,1) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', 1.65*MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', DrugColor, 'Color', 'k');
% Green Data Points
plot(Handle_Axis2, Input_Data(N_OL+N_CL_ND+1:end,1) ./ Input_Data(N_OL+N_CL_ND+1:end,2) - DotSpace, Measurement1_Data(N_OL+N_CL_ND+1:end) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis2, Input_Data(N_OL+N_CL_ND+1:end,1) ./ Input_Data(N_OL+N_CL_ND+1:end,2), Measurement2_Data(N_OL+N_CL_ND+1:end) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', DrugColor, 'Color', 'k');
plot(Handle_Axis2, Input_Data(N_OL+N_CL_ND+1:end,1) ./ Input_Data(N_OL+N_CL_ND+1:end,2) + DotSpace, Measurement3_Data(N_OL+N_CL_ND+1:end) / Normalization_CL, ...
     'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', DrugColor, 'Color', 'k');
% Green Simulation
plot(Handle_Axis2, G_1 ./ G_2, Measurement_CL_D / Normalization_CL, 'LineWidth', LineWidth/1.5, 'Color', DrugColor, 'LineStyle', '--');

yyaxis(Handle_Axis2, 'right');
Handle_Axis2.YLim = [0, 1.2*Measurement_Data(N_OL+N_CL_ND)];
Handle_Axis2.YLabel.String = 'Fluorescence [a.u.]';
Handle_Axis2.YAxis(2).Color = MyGreen;

% -------
% Legends
% -------
Handle_White = plot(Handle_Axis2, -1, -1, 'Color', [1, 1, 1]);
Handle_DataPoint = plot(Handle_Axis2, -1, -1, 'LineStyle', 'none', 'Marker', 'o', 'MarkerSize', MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', [0.5, 0.5, 0.5], 'Color', 'k'); 
Handle_Fit = plot(Handle_Axis2, -1, -1, 'LineWidth', LineWidth, 'Color', [0.5, 0.5, 0.5], 'LineStyle', '-');
Handle_Prediction = plot(Handle_Axis2, -1, -1, 'LineWidth', LineWidth/1.4, 'Color', [0.5, 0.5, 0.5], 'LineStyle', '--');
Handle_Diamond = plot(Handle_Axis2, -1,-1, 'LineStyle', 'none', 'Marker', 'diamond', 'MarkerSize', 2*MarkerSize, 'LineWidth', LineWidth_Thin, 'MarkerFaceColor', [0.5, 0.5, 0.5], 'Color', 'k');
Handle_Legend1 = legend(Handle_Axis2, [Handle_DataPoint, Handle_Diamond, Handle_Fit, Handle_Prediction], 'Measurement{      }', 'Average{      }',  'Model Calibration{      }', 'Model Prediction');
    Handle_Legend1.Orientation = 'Horizontal';
    Handle_Legend1.EdgeColor = 'none';
    Handle_Legend1.Position = [0.1475, 0.935, 0.4, 0.05];
    Handle_Legend1.Color = 'none';
Handle_Box = annotation('rectangle', [Handle_Legend1.Position(1) + Handle_Legend1.Position(3) + 0.01, Handle_Legend1.Position(2), Handle_Legend1.Position(3:4) + [0.02, 0]]);
Handle_Color1 = annotation('rectangle');
    Handle_Color1.Position = [Handle_Box.Position(1) + 0.2, Handle_Box.Position(2), 0.01*1.5, 2*0.015*1.5];
    Handle_Color1.EdgeColor = 'none';
    Handle_Color1.FaceColor = MyGreen;
Handle_Text1 = annotation('textbox');
    Handle_Text1.Position = Handle_Color1.Position + [0.015, 0, 0.3, 0];
    Handle_Text1.EdgeColor = 'none';
    Handle_Text1.String = 'With/Without Disturbance';
    Handle_Text1.FontSize = Handle_Legend1.FontSize;
    Handle_Text1.FontName = Handle_Legend1.FontName;
    Handle_Text1.VerticalAlignment = 'Middle';
Handle_Color3 = annotation('rectangle');
    Handle_Color3.Position = [Handle_Box.Position(1) + 0.175, Handle_Color1.Position(2:4)];
    Handle_Color3.EdgeColor = 'none';
    Handle_Color3.FaceColor = DrugColor;
Handle_Text2 = annotation('textbox');
    Handle_Text2.Position = Handle_Color3.Position + [0.013, 0, 0, 0];
    Handle_Text2.EdgeColor = 'none';
    Handle_Text2.String = '/';
    Handle_Text2.FontSize = Handle_Legend1.FontSize;
    Handle_Text2.FontName = Handle_Legend1.FontName;
    Handle_Text2.VerticalAlignment = 'Middle';
Handle_Box.Position = [0.006, 0.92, 0.985, 0.08];
end

