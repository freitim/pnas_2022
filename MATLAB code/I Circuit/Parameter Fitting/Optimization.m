function [LumpedParameters_Optimal, J] = Optimization(Input_Data, Measurement_Data, ...
                                                      CostFunction, ...
                                                      LumpedParameters_IG, LumpedParameters_LB, LumpedParameters_UB, ...
                                                  	  A, B, Aeq, Beq, Options)
%% Optimization Routine
THETA0 = Vector(LumpedParameters_IG);
LB = Vector(LumpedParameters_LB);
UB = Vector(LumpedParameters_UB);
[THETA_Opt, J] = fmincon(@(THETA) OptimizationFunction(THETA, Input_Data, Measurement_Data, CostFunction), THETA0, A, B, Aeq, Beq, LB, UB, [], Options);
                     
%% Extract the Optimal Parameters
LumpedParameters_Optimal = Class_LumpedParameters(THETA_Opt);

end

%% Optimization Function
function J = OptimizationFunction(THETA, Input_Data, Measurement_Data, CostFunction)
    LumpedParameters = Class_LumpedParameters(THETA);
    J = CostFunction(LumpedParameters, Input_Data, Measurement_Data);
end