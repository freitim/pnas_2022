function [Input_Data, Measurement_Data, Measurement_SEM] = GenerateData(DataTable)
% Input_Data = [G_1, G_2, D] in [pmol, pmol, nM]
% Measurement_Data = Green in [a.u.]
% The data generated are sorted as Open Loop, then without disturbance, then
% smaller G_1.

%% Extract Inputs and Measurements
N = size(DataTable,1) / 3;
Input_Data = zeros(N,4);
Measurement_Data = zeros(N,1);
Measurement_SEM = zeros(N,1);
for i = 1 : N
    Input_Data(i,1) = DataTable{3*(i-1)+1,3};                           % G_1 [pmol]
    Input_Data(i,2) = DataTable{3*(i-1)+1,4};                           % G_2 [pmol]
    Input_Data(i,3) = DataTable{3*(i-1)+1,5} * 1e3;                     % D [nM]
    if (DataTable{3*(i-1)+1,1} == 'Open')
        Input_Data(i,4) = 0;
    elseif (DataTable{3*(i-1)+1,1} == 'Closed')
        Input_Data(i,4) = 1;
    else
        error('Only Open and Closed Loop are allowed.');
    end
    Measurement_Data(i) = mean(DataTable{3*(i-1)+1:3*i,7});             % Green [a.u.]
    Measurement_SEM(i) = std(DataTable{3*(i-1)+1:3*i,7})/sqrt(3);    	% Green [a.u.]
end

%% Sort with and without Disturbance
[Input_Data, Indeces] = sortrows(Input_Data, [4, 3, 1]);
Measurement_Data = Measurement_Data(Indeces);
Measurement_SEM = Measurement_SEM(Indeces);

end