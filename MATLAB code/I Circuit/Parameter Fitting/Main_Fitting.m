%% Clear Workspace
close all;
clear;
clc;
Colors = lines(10);
MyBlue = Colors(1,:); MyRed = Colors(2,:); MyGreen = Colors(5,:);
SS = 4; % Screen Scale
Save_Flag = 0;

%% Select Optimization Settings and Cost Function and Specify FileName
% FileName = 'Results_OL_ND';
% CostFunction = @CostFunction_OL_ND;
% DataFile = 'FullData_I1.mat';
% [Input_Data, Measurement_Data, Measurement_SEM, ~,  LumpedParameters_LB,  LumpedParameters_UB, A, B, Aeq, Beq, Options] = OptimizationSettings_OL_ND(DataFile);

% FileName = 'Results_CL_ND';
% CostFunction = @CostFunction_CL_ND;
% DataFile = 'FullData_I1.mat';
% [Input_Data, Measurement_Data, Measurement_SEM, ~,  LumpedParameters_LB,  LumpedParameters_UB, A, B, Aeq, Beq, Options] = OptimizationSettings_CL_ND(DataFile);

FileName = 'Results_OL_D';
CostFunction = @CostFunction_OL_D;
DataFile = 'FullData_I1.mat';
[Input_Data, Measurement_Data, Measurement_SEM, ~,  LumpedParameters_LB,  LumpedParameters_UB, A, B, Aeq, Beq, Options] = OptimizationSettings_OL_D(DataFile);

% FileName = 'Results_Full';
% CostFunction = @CostFunction_Full;
% DataFile = 'FullData_I1.mat';
% [Input_Data, Measurement_Data, Measurement_SEM, ~,  LumpedParameters_LB,  LumpedParameters_UB, A, B, Aeq, Beq, Options] = OptimizationSettings_Full(DataFile);

%% Generate Random Initial Guesses
N_IG = 100;
LumpedParameters_IG = Generate_RandomIG(LumpedParameters_LB, LumpedParameters_UB, N_IG);

%% Optimizing 
tic;
J = zeros(N_IG,1);
LumpedParameters_Optimal(1:N_IG) = Class_LumpedParameters();
for i = 1 : N_IG
    i
    [LumpedParameters_Optimal(i), J(i)] = Optimization(Input_Data, Measurement_Data, ...
                                                       CostFunction, ...
                                                       LumpedParameters_IG(i), LumpedParameters_LB, LumpedParameters_UB, ...
                                                  	   A, B, Aeq, Beq, Options);
end
SimTime = toc;

%% Compare Data and Model
[J_Sorted, Indeces] = sort(J, 'descend');
figure();
plot(J_Sorted);
LumpedParameters = LumpedParameters_Optimal(Indeces(end));

%% Save Parameters
clearvars -except LumpedParameters_Optimal J ...
                  LumpedParameters_LB LumpedParameters_UB ...
                  LumpedParameters ...
                  FileName;
save(FileName);
