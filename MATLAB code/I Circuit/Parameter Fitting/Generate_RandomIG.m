function LumpedParameters_IG = Generate_RandomIG(LumpedParameters_LB, LumpedParameters_UB, N)
%% Vectorizing the Bounds
LumpedParameters_LB_Vector = Vector(LumpedParameters_LB);
LumpedParameters_UB_Vector = Vector(LumpedParameters_UB);
N_Parameters = length(LumpedParameters_LB_Vector);

%% Generate Random Initial Guesses for the System Parameters
LumpedParameters_IG_Vector = LumpedParameters_LB_Vector + (LumpedParameters_UB_Vector - LumpedParameters_LB_Vector) .* rand(N_Parameters,N);
LumpedParameters_IG(1:N) = Class_LumpedParameters();
for i = 1 : N
    LumpedParameters_IG(i) = Class_LumpedParameters(LumpedParameters_IG_Vector(:,i));
end

end

