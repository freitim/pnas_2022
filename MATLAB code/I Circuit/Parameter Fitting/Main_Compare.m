%% Clear Workspace
close all;
clear;
clc;
Colors = lines(10);
MyBlue = Colors(1,:); MyRed = Colors(2,:); MyGreen = Colors(5,:);
SS = 4; % Screen Scale
Save_Flag = 1;

%% Select Parameter Fit
% load Results_OL_ND;
% load Results_CL_ND;
load Results_OL_D;
% load Results_Full;


%% Generate Data
Data = load('FullData_I1');
[Input_Data, Measurement_Data] = GenerateData(Data.DataTable);

%% Override Manual Tuning
% LumpedParameters.gamma_bar = 1*LumpedParameters.gamma_bar;                    % [1/a.u.]                  gamma_bar = (gamma/k) * ((c+gamma)/c) / c_x
% LumpedParameters.delta_bar = 1*LumpedParameters.delta_bar;                    % [dimensionless]           delta_bar = delta / k_1
% LumpedParameters.eta_bar = 1*LumpedParameters.eta_bar;                        % [1/nM]                    eta_bar = eta / k_1
% LumpedParameters.kappa_bar = 1*LumpedParameters.kappa_bar;                    % [a.u.]                    kappa_bar = c_x * kappa
% LumpedParameters.kappa_bar_2 = 1*LumpedParameters.kappa_bar_2;                % [a.u.]                    kappa_bar_2 = c_x * sqrt(kappa * kappa_2)
% LumpedParameters.k_bar_0 = 1*LumpedParameters.k_bar_0;                        % [Dimensionless]           k_bar_0 = k_0 / k_1
% LumpedParameters.k_bar_2 = 1*LumpedParameters.k_bar_2;                        % [Dimensionless]           k_bar_2 = k_2 / k_1
% LumpedParameters.Delta_1 = 1*LumpedParameters.Delta_1;                        % [1/nM]                    Delta_1 = k_3 / (kappa_3*c_bar)
% LumpedParameters.Delta_2 = 1*LumpedParameters.Delta_2;                        % [1/a.u./nM]            	Delta_2 = (k_prime_3*gamma) / (kappa_prime_3^2*c_bar*c*c_x)
% LumpedParameters.Delta_prime_1 = 1*LumpedParameters.Delta_prime_1;            % [1/a.u.]               	Delta_prime_1 = gamma / (kappa_3*c*c_x)
% LumpedParameters.Delta_prime_2 = 1*LumpedParameters.Delta_prime_2;            % [1/a.u.^2]               	Delta_prime_2 = gamma^2 / (kappa_prime_3^2*c^2*c_x^2)

%% Select Compare Function
N = 100;
% Compare_FullData_Simulation_Prediction_Normalized_Mini(Data.DataTable, LumpedParameters, N, 'log');
Compare_FullData_Simulation_Prediction_Normalized(Data.DataTable, LumpedParameters, N, 'linear');

if Save_Flag == 1
    Handle_Figure = gcf;
    Handle_Figure.Color = 'none';
    set(Handle_Figure, 'InvertHardCopy', 'off');
%     print(Handle_Figure, 'Model_Fit_I_Normalized_Log_Mini','-dpdf');
    print(gcf, 'Model_Fit_I_Normalized','-dpdf');
    Handle_Figure.Color = [1, 1, 1];
end
