# A genetic mammalian proportional-integral feedback control circuit for robust and precise gene regulation

Data and code used in Timothy Frei, Ching-Hsiang Chang, Maurice Filo, Asterios Arampatzis, and Mustafa Khammash. “A genetic mammalian proportional-integral feedback control circuit for robust and precise gene regulation.”
